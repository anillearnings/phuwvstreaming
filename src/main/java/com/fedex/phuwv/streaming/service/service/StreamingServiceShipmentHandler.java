package com.fedex.phuwv.streaming.service.service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import com.fedex.sefs.core.shipment.v1.api.ShipmentDomainEvent;
import lombok.extern.slf4j.Slf4j;

@Service("StreamingServiceShipmentHandler")
@Slf4j
public class StreamingServiceShipmentHandler implements MessageHandler<ShipmentDomainEvent> {

	private RetryTemplate shipmentTriggerRetryTemplate;

	@Autowired
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Autowired
	private LocationTranslationInvoker locationTranslationInvoker;

	@Autowired
	private StreamingPublishService streamingPublishService;

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;

	private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

	private static final String COMPLETED_EXCEPTIONALLY = "Completed exceptionally:";

	private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

	@Autowired
	public StreamingServiceShipmentHandler(@Qualifier("jmsRetryTemplate") RetryTemplate shipmentTriggerRetryTemplate) {
		this.shipmentTriggerRetryTemplate = shipmentTriggerRetryTemplate;

	}

	@Override
	public void handle(GenericMessage<ShipmentDomainEvent> genericMessage) throws  Exception {
		// Synchronous method call :: START

		shipmentTriggerRetryTemplate.execute(retryContext -> {
			try {
				ShipmentDomainEvent message = genericMessage.getPayload();
				if (StringUtils.isNotEmpty(message.getShipmentUUID())) {
					log.info("shipmentUUID from shipment internal trigger : {}", message.getShipmentUUID());
					Audit.getInstance().put(AuditKey.IN, "shipmentUUID from shipment internal trigger : " + message.getShipmentUUID());
					if(streamingPublishService.validatePublishingPhuwvJms(message.getShipmentUUID())){
						return null;
					}

					// 1.Call ShipmentService (by shipment uuid from Msg Properties)
					//CompletableFuture<ShipmentDetails> shipmentServiceResponse = streamingPublishService.invokeGetFutureLatestShipmentByUUID(message.getShipmentUUID());
					ShipmentDetails shipmentServiceResponse = streamingPublishService.invokeGetLatestShipmentByUUID(message.getShipmentUUID());

					// Call HandlingUnitService (with HandlingUnitUUID from Msg Properties)
					//CompletableFuture<List<HandlingUnit>> listOfHUByshipmentUUID = invokeGetFutureListHandlingUnitByShipmentUUID(message.getShipmentUUID());
					List<HandlingUnit> listOfHUByshipmentUUID = invokeGetListHandlingUnitByShipmentUUID(message.getShipmentUUID());

					// Wait until they are all done
					//CompletableFuture.allOf(shipmentServiceResponse, listOfHUByshipmentUUID).join();

					//ShipmentDetails shipmentDetails = shipmentServiceResponse.get();
					ShipmentDetails shipmentDetails = shipmentServiceResponse;

					if(validateHandlingUnitList(listOfHUByshipmentUUID, shipmentDetails, message.getShipmentUUID())){
						return null;
					}
				}
				log.info("End of shipment trigger task with shipment uuid: " + message.getShipmentUUID());
			} catch (Exception e) {
				log.error("Exception:", e);
			}
			return null;
		}, retryContext -> {
			streamingPublishService.setRetryContext(retryContext);
			return null;
		});
	}

	private void buisnessLogicToPublishPhuwv(HandlingUnit huModel, ShipmentDetails shipmentDetails,
			ItineraryDTO itineraryResponse,List<Task> tasks) {
		log.info("***Merging response data and creating New HandlingUnit Model***");

		if (huModel != null) {
			try {
				streamingPublishService.publishPhuwvEvent(streamingPublishService.buildPhuwvAPI(huModel, shipmentDetails, streamingPublishService.fetchLSSIWaypoints(itineraryResponse), tasks), streamingServiceProperties.getPhuwvSefsCoreType());
			} catch (Exception e) {
				log.error("Exception when publishing phuwv event:" + e);
			}
		}

	}

	private CompletableFuture<List<HandlingUnit>> invokeGetFutureListHandlingUnitByShipmentUUID(String shipmentUUID) {
		return phuwvServiceInvoker
				.getFutureListHandlingUnitByShipmentUUID(shipmentUUID).exceptionally(ex -> {
					log.error(COMPLETED_EXCEPTIONALLY, ex);
					return null;
				});
	}

	private boolean validateHandlingUnitList(List<HandlingUnit> listOfHUByshipmentUUID, ShipmentDetails shipmentDetails, String shipmentUUID) throws ExecutionException,
			InterruptedException {

		// 3. Call WaypointService (by each handling unit id we got from step 2)
		/*if (listOfHUByshipmentUUID.isDone()) {
			List<HandlingUnit> listHU = listOfHUByshipmentUUID.get();

			if (Objects.isNull(listHU)) {
				log.warn("No response from handling unit service for shipment uuid:" + shipmentUUID
						+ "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
				return true;
			}
			computeAsyncTasks(listHU, shipmentDetails, shipmentUUID);
		}*/
		List<HandlingUnit> listHU = listOfHUByshipmentUUID;

		if (Objects.isNull(listHU)) {
			log.warn("No response from handling unit service for shipment uuid:" + shipmentUUID
					+ "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
			return true;
		}
		computeAsyncTasks(listHU, shipmentDetails, shipmentUUID);
		return false;
	}

	private void computeAsyncTasks(List<HandlingUnit> listHU, ShipmentDetails shipmentDetails, String shipmentUUID) {
		log.info("No: of handlingUnit by shipmentUUId  ->  {} " ,listHU.size() );

		IntStream.range(0, listHU.size()).forEach(i -> {

			if(validateShipmentDetailsFirstly(listHU.get(i), shipmentDetails, shipmentUUID)){
				return;
			}

			// call tripService(use HandlingUnit UUID from Msg Properties)
			//CompletableFuture<TripDTO> tripServiceResponse = streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(listHU.get(i).getHuUUID());
			TripDTO tripServiceResponse = streamingPublishService.invokeGetTripByHandlingUnitUUID(listHU.get(i).getHuUUID());

			//Call TaskService
			//CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(listHU.get(i).getHuUUID());
			List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(listHU.get(i).getHuUUID());

			// Wait until they are all done
			//CompletableFuture.allOf(tripServiceResponse, futureTasksByHandlingUnitUUID).join();

			// call ItinerrayService(By tripUUID from TripServiceResponse)
			ItineraryDTO itineraryServiceResponse = null;
			/*if (tripServiceResponse.isDone()) {
				try {
					itineraryServiceResponse = phuwvServiceInvoker
							.getLatestItineraryByTripUUID(tripServiceResponse.get().getTripUUID());
				} catch (Exception e) {
					log.error("No response from shipment service for uuid:"
							+ listHU.get(i).getHuUUID() + " due to:" + e);
					if(validateHUTypeForUUID(listHU.get(i))){
						return;
					}
				}
			}*/
			try {
				itineraryServiceResponse = phuwvServiceInvoker
						.getLatestItineraryByTripUUID(tripServiceResponse.getTripUUID());
			} catch (Exception e) {
				log.error("No response from shipment service for uuid:"
						+ listHU.get(i).getHuUUID() + " due to:" + e);
				if(validateHUTypeForUUID(listHU.get(i))){
					return;
				}
			}

			//List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, listHU.get(i).getHuUUID());
			List<Task> taskDetails = futureTasksByHandlingUnitUUID;

			// 4.Merge different Models into a new HandlingUnitModel class fully populated.
			buisnessLogicToPublishPhuwv(listHU.get(i), shipmentDetails,
					itineraryServiceResponse,taskDetails);
		});
	}

	private boolean validateShipmentDetailsFirstly(HandlingUnit handlingUnit, ShipmentDetails shipmentDetails, String shipmentUUID) {
		if (Objects.isNull(shipmentDetails)) {
			log.error("No response from shipment service for uuid:"
					+ shipmentUUID);
			return validateHUTypeForUUID(handlingUnit);
		}
		return false;
	}

	private boolean validateHUTypeForUUID(HandlingUnit handlingUnit) {
		switch (handlingUnit.getType()){
			case HANDLINGUNIT_TYPE_CONSOLIDATION:
				return false;
			default:
				return true;
		}
	}

	private List<HandlingUnit> invokeGetListHandlingUnitByShipmentUUID(String shipmentUUID) {
		return phuwvServiceInvoker.getListHandlingUnitByShipmentUUID(shipmentUUID);
	}
}
