package com.fedex.phuwv.streaming.service.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fedex.phuwv.common.dto.Waypoint;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateRequest;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateResponses;
import com.fedex.phuwv.common.dto.lssi.dto.KeyValue;
import com.fedex.phuwv.common.dto.lssi.dto.TranslateRequest;
import com.fedex.phuwv.common.dto.lssi.dto.TranslateRequestFrom;
import com.fedex.phuwv.common.dto.lssi.dto.TranslateRequestTo;
import com.fedex.phuwv.common.dto.lssi.dto.TranslateRequestToTargetType;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LocationTranslationInvoker {

	@Autowired
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;

	public List<Waypoint> convertOSCWaypointsToLSSI(List<Waypoint> waypointOSCList) {
		BulkTranslateRequest bulkTranslateRequest = new BulkTranslateRequest();
		try{
			if (Objects.nonNull(waypointOSCList)) {
				waypointOSCList.stream().filter(wayPointOSC->wayPointOSC.getFacilityOpCo()!=null && wayPointOSC.getFacilityOpCo().equalsIgnoreCase(streamingServiceProperties.getWaypointOscFacilityopcoTnt())).collect(Collectors.toList())
						.forEach(criteriaList -> bulkTranslateRequest.add(setupOfTranslateRequest(criteriaList.getFacilityCode(), criteriaList.getType().getValue())));
				return setupOfLSSIWaypoints(bulkTranslateRequest, waypointOSCList);
			}
		}catch (Exception e){
			log.error("Error occurred when invoking location translation service because:", e);
		}
		return waypointOSCList;
	}

	private TranslateRequestTo setupOfTranslateRequestTo() {
		TranslateRequestTo to = new TranslateRequestTo();
		TranslateRequestToTargetType translateRequestToTargetType = new TranslateRequestToTargetType();
		translateRequestToTargetType.setKey(streamingServiceProperties.getTranslateRequestToTargetTypeKey());
		translateRequestToTargetType.setValue(streamingServiceProperties.getTranslateRequestToTargetTypeValue());
		to.setTargetType(translateRequestToTargetType);
		to.setTargetValueKey(streamingServiceProperties.getTranslateRequestToTargetValueKey());
		return to;
	}

	private TranslateRequest setupOfTranslateRequest(String facilityCode, String waypointType) {
		TranslateRequest translateReq = new TranslateRequest();
		TranslateRequestFrom from = new TranslateRequestFrom();
		List<KeyValue> criteria = new ArrayList<>();

		KeyValue keyValue = new KeyValue();
		KeyValue keyValue1 = new KeyValue();
		KeyValue keyValue2 = new KeyValue();

		keyValue.setKey(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropParty());
		keyValue.setValue(streamingServiceProperties.getTranslateRequestFromCriteriaValueOsc());
		criteria.add(keyValue);

		keyValue1.setKey(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropOperationId());
		keyValue1.setValue(facilityCode);

		keyValue2.setKey(streamingServiceProperties.getTranslateRequestFromCriteriaKeyBusinessContext());
		keyValue2.setValue(waypointType);

		criteria.add(keyValue1);
		criteria.add(keyValue2);

		from.setCriteria(criteria);
		translateReq.setFrom(from);
		translateReq.setTo(setupOfTranslateRequestTo());
		return translateReq;
	}

	private List<Waypoint> setupOfLSSIWaypoints(BulkTranslateRequest bulkTranslateRequest, List<Waypoint> waypointOSCList) {
		if (!bulkTranslateRequest.isEmpty()) {
			BulkTranslateResponses bulkTranslateResponses = phuwvServiceInvoker
					.getFutureTranslatedLocationCode(bulkTranslateRequest);
			if (bulkTranslateResponses != null) {
				return buildNewWaypointList(bulkTranslateResponses, waypointOSCList);
			}
		}
		return waypointOSCList;
	}

	private List<Waypoint> buildNewWaypointList(BulkTranslateResponses bulkTranslateResponses, List<Waypoint> waypointOSCList) {
		List<Waypoint> newWayPointList = new ArrayList<>();
		int j = 0;
		for (Waypoint wList : waypointOSCList) {
			if (wList.getFacilityOpCo() != null && wList.getFacilityOpCo().equals(streamingServiceProperties.getWaypointOscFacilityopcoTnt()))
				wList.setFacilityCode(bulkTranslateResponses.get(j++).getTargetValue());
			newWayPointList.add(wList);
		}
		return newWayPointList;
	}
}