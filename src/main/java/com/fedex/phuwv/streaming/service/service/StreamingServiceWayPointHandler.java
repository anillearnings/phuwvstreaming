package com.fedex.phuwv.streaming.service.service;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.waypoint.v3.api.Association;
import com.fedex.sefs.core.waypoint.v3.api.WaypointDomainEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service("StreamingServiceWayPointHandler")
@Slf4j
public class StreamingServiceWayPointHandler implements MessageHandler<WaypointDomainEvent> {

    @Autowired
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Autowired
    private StreamingServiceProperties streamingServiceProperties;

    @Autowired
    private StreamingPublishService streamingPublishService;

    @Autowired
    private LocationTranslationInvoker locationTranslationInvoker;

    private RetryTemplate waypointTriggerRetryTemplate;
    
    private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

    private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

    @Autowired
    public StreamingServiceWayPointHandler(@Qualifier("jmsRetryTemplate") RetryTemplate waypointTriggerRetryTemplate) {
        this.waypointTriggerRetryTemplate = waypointTriggerRetryTemplate;

    }

    @Override
    public void handle(GenericMessage<WaypointDomainEvent> genericMessage) throws Exception {

        waypointTriggerRetryTemplate.execute(retryContext -> {
            String handlingUnitUUID = null;
            String tripUUID = null;

            try {
                WaypointDomainEvent message = genericMessage.getPayload();
                String waypointUUID = String.valueOf(message.getWaypoint().getSystemIdentificationProfile().getUuid());
                log.info("waypointUUID from waypoint trigger event: {}", waypointUUID);
                Audit.getInstance().put(AuditKey.IN, "waypointUUID from waypoint trigger event: " + waypointUUID);

                Map<String,String> huAndTripUUIDsMap = settingHuAndTripUUIDs(message.getAssociations());
                handlingUnitUUID = huAndTripUUIDsMap.get(streamingServiceProperties.getWaypointAssociationLevelHandlingunit());
                tripUUID = huAndTripUUIDsMap.get(streamingServiceProperties.getWaypointAssociationLevelTrip());

                if(streamingPublishService.validatePublishingPhuwvJms(waypointUUID)){
                    return null;
                }

                //	Call TripService by lookup on HandlingUnitUUID.because it is not present in message
                //We can return just a Data Transfer Object(DTO) that has TRIP_UUID,HANDLING_UNIT_UUID for now.
                handlingUnitUUID = settinghuUUID(handlingUnitUUID, tripUUID);

                if(validateHandlingUnitModel(handlingUnitUUID, tripUUID, waypointUUID)){
                    return null;
                }
            } catch (Exception e) {
                log.error("exception caught in waypoint handler: {}, {}", e, WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);

            }
            return null;
        }, retryContext -> {
            streamingPublishService.setRetryContext(retryContext);
            return null;
        });
    }

    private Map<String,String> settingHuAndTripUUIDs(List<Association> associations) {
        Map<String,String> huAndTripUUIDsMap = new HashMap<>();
        if (associations != null && !associations.isEmpty()) {
            for (Association association : associations) {
                String associationLevel = association.getAssociationLevel();
                if (associationLevel.equalsIgnoreCase(streamingServiceProperties.getWaypointAssociationLevelHandlingunit())) {
                    huAndTripUUIDsMap.put(streamingServiceProperties.getWaypointAssociationLevelHandlingunit(), association.getAssociationUUID());
                    huAndTripUUIDsMap.put(streamingServiceProperties.getWaypointAssociationLevelTrip(), null);

                } else if (association.getAssociationLevel().equalsIgnoreCase(streamingServiceProperties.getWaypointAssociationLevelTrip())) {
                    huAndTripUUIDsMap.put(streamingServiceProperties.getWaypointAssociationLevelTrip(), association.getAssociationUUID());
                    huAndTripUUIDsMap.put(streamingServiceProperties.getWaypointAssociationLevelHandlingunit(), null);
                }
            }
        }
        return huAndTripUUIDsMap;
    }

    private String settinghuUUID(String handlingUnitUUID, String tripUUID) {
        if (StringUtils.isEmpty(handlingUnitUUID)) {
            TripDTO tripDTO = phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(tripUUID);
            return (null != tripDTO)?tripDTO.getHandlingUnitUUID():null;
        }
        return handlingUnitUUID;
    }

    private boolean validateHandlingUnitModel(String handlingUnitUUID, String tripUUID, String waypointUUID) throws ExecutionException, InterruptedException {
        if (StringUtils.isNotEmpty(handlingUnitUUID)) {

            log.info("handlingUnitUUID from waypoint internal trigger : {}" + handlingUnitUUID);
            //	Call HandlingUnitService (use HandlingUnit UUID )
            HandlingUnit handlingUnitModel = phuwvServiceInvoker.getLatestHandlingUnitByUUID(handlingUnitUUID);
            if (Objects.isNull(handlingUnitModel)) {
                log.warn("No response from Handling unit service for hu uuid:" + handlingUnitUUID + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
                return true;
            }

            return computeAsyncTasks(handlingUnitModel, handlingUnitUUID, tripUUID, waypointUUID);
        }else{
            log.info("No handling unit uuid present for the trip uuid: {}, {}", tripUUID, WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
        }
        return false;
    }

    private boolean computeAsyncTasks(HandlingUnit handlingUnitModel, String handlingUnitUUID, String tripUUID, String waypointUUID) throws ExecutionException, InterruptedException {

        ShipmentDetails shipmentModel = null;

        // Call ShipmentService (with shipment uuid from Handling Unit service)
        //CompletableFuture<ShipmentDetails> shipmentServiceResponse = streamingPublishService.invokeGetFutureLatestShipmentByUUID(handlingUnitModel.getShipment().getShipmentUUID());
        ShipmentDetails shipmentServiceResponse = streamingPublishService.invokeGetLatestShipmentByUUID(handlingUnitModel.getShipment().getShipmentUUID());

        //Call TaskService
        //CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(handlingUnitUUID);
        List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(handlingUnitUUID);

        // Wait until they are all done
        //CompletableFuture.allOf(shipmentServiceResponse, futureTasksByHandlingUnitUUID).join();

        // Call Waypoint Service for every WAYPOINT_UUID. We can then pull together all
        // associated waypoint information associated with the ITINERARY to populate our
        // PHUWV API
        List<Waypoint> waypointModelList = streamingPublishService.fetchItineraryAndGetWaypoints(tripUUID);

        /*if (shipmentServiceResponse.isDone()) {
            shipmentModel = shipmentServiceResponse.get();
            if(validateShipmentDetailsViaHUType(handlingUnitModel, shipmentModel, handlingUnitUUID)){
                return true;
            }
        }*/
        shipmentModel = shipmentServiceResponse;
        if(validateShipmentDetailsViaHUType(handlingUnitModel, shipmentModel, handlingUnitUUID)){
            return true;
        }

        //List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, handlingUnitUUID);
        List<Task> taskDetails = futureTasksByHandlingUnitUUID;

        // Merge different Models from the calls above into a new/pristine PHUWV HandlingUnit class fully populated.
        streamingPublishService.mergeToPublish(handlingUnitModel, shipmentModel, locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointModelList), taskDetails,streamingServiceProperties.getPhuwvSefsCoreType());
        log.info("End of waypoint  trigger task with waypoint uuid: " + waypointUUID);
        return false;
    }

    private boolean validateShipmentDetailsViaHUType(HandlingUnit handlingUnitModel, ShipmentDetails shipmentModel, String handlingUnitUUID) {
        if (shipmentModel == null) {
            switch (handlingUnitModel.getType()){
                case HANDLINGUNIT_TYPE_CONSOLIDATION:
                    return false;
                default:
                    log.warn("No response from Shipment service for hu uuid:" + handlingUnitUUID
                            + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
                    return true;
            }
        }
        return false;
    }
}







