package com.fedex.phuwv.streaming.service.configuration;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
@EnableConfigurationProperties(StreamingServiceProperties.class)
@EnableAsync
@Slf4j
@EnableRetry
public class StreamingServiceConfiguration {

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;

	@Bean(name = "asyncTaskExecutor", destroyMethod = "shutdown")
	@Qualifier("asyncTaskExecutor")
	public Executor asyncTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(streamingServiceProperties.getCorePoolSize());
		executor.setMaxPoolSize(streamingServiceProperties.getMaxPoolSize());
		executor.setQueueCapacity(streamingServiceProperties.getQueueCapacity());
		executor.setKeepAliveSeconds(streamingServiceProperties.getKeepAliveSeconds());
		executor.setThreadNamePrefix("AsyncTaskExecutorThread-");
		executor.initialize();
		executor.setRejectedExecutionHandler((r, executor1) -> {
			int remainingCapacity = executor1.getQueue().remainingCapacity();
			int activeCount = executor1.getActiveCount();
			int poolSize = executor1.getPoolSize();
			long taskCount = executor1.getTaskCount();
			long completedTaskCount = executor1.getCompletedTaskCount();
			log.info("ActiveCount: {}, RemainingCapacity: {}, PoolSize: {}, TaskCount: {}, CompletedTaskCount: {}",
					activeCount, remainingCapacity, poolSize, taskCount, completedTaskCount);
		});

		return executor;
	}
}
