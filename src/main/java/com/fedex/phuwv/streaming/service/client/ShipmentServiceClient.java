package com.fedex.phuwv.streaming.service.client;

import com.fedex.phuwv.common.dto.ShipmentDetails;
import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PHUWV-SHIPMENT-SERVICE", url = "${app.rest-client.end-points.SHIPMENT.base-uri}",
		configuration = FeignClientConfig.class)
public interface ShipmentServiceClient {

	@GetMapping(value = "${app.rest-client.end-points.GET_SHIPMENT_BY_SHIPMENT_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ShipmentDetails> getLatestShipmentByUUID(@PathVariable("shipmentUUID") String shipmentUUID);

	@DeleteMapping(value = "${app.rest-client.end-points.DELETE_SHIPMENT_BY_PURGE_DATE.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Integer> purgeUntilDate(@PathVariable(value = "purgeDt") String purgeDt);

	@PatchMapping(value = "${app.rest-client.end-points.PATCH_SHIPMENT_BY_SHIPMENT_UUID_PURGE_DATE.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Integer> updateShipmentWithPurgeDate(@PathVariable(value = "shipmentUUID") String shipmentUUID, @PathVariable(value = "purgeDt") String purgeDt);
}
