package com.fedex.phuwv.streaming.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude= {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients
@ComponentScan(basePackages = { "com.fedex.phuwv.common", "com.fedex.phuwv.streaming" })
@EnableCircuitBreaker
public class StreamingServiceApplication {
	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(StreamingServiceApplication.class);
		springApplication.run(args);

	}
	

}

