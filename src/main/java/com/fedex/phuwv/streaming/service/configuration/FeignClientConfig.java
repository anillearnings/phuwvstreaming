package com.fedex.phuwv.streaming.service.configuration;

import feign.RetryableException;
import feign.Retryer;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@NoArgsConstructor
public class FeignClientConfig implements Retryer {

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;

	private int retryMaxAttempt;

	private long retryInterval;

	private int attempt = 1;

	@Bean
	public FeignErrorDecoder errorDecoder(){
		return new FeignErrorDecoder();
	}

	public FeignClientConfig (int retryMaxAttempt, Long retryInterval) {
	        this.retryMaxAttempt = retryMaxAttempt;
	        this.retryInterval = retryInterval;
	    }

	@Override
	public void continueOrPropagate(RetryableException e) {
		log.info("Feign retry attempt {} due to {} ", attempt, e.getMessage());

		if (attempt++ == retryMaxAttempt) {
			throw e;
		}
		try {
			Thread.sleep(retryInterval);
		} catch (InterruptedException ignored) {
			Thread.currentThread().interrupt();
		}

	}

	@Override
	public Retryer clone() {
		return new FeignClientConfig(streamingServiceProperties.getFeignClientRetryMaxAttempt(),
				Long.valueOf(streamingServiceProperties.getFeignClientRetryInterval()));
	}
}
