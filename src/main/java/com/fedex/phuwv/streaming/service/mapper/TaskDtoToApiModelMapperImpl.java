package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.api.models.TaskStatus;
import com.fedex.phuwv.common.dto.Task;
import com.fedex.phuwv.common.dto.TaskFacilityDetail;
import com.fedex.phuwv.common.dto.WorkAttribute;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-10-12T17:08:48+0530",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-6.8.2.jar, environment: Java 1.8.0_281 (Oracle Corporation)"
)
@Component
public class TaskDtoToApiModelMapperImpl implements TaskDtoToApiModelMapper {

    @Override
    public List<com.fedex.phuwv.common.api.models.Task> taskDtoToApiModel(List<Task> taskDto) {
        if ( taskDto == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.Task> list = new ArrayList<com.fedex.phuwv.common.api.models.Task>( taskDto.size() );
        for ( Task task : taskDto ) {
            list.add( taskToTask( task ) );
        }

        return list;
    }

    @Override
    public List<TaskStatus> taskStatusDtoToApiModel(List<com.fedex.phuwv.common.dto.TaskStatus> taskStatus) {
        if ( taskStatus == null ) {
            return null;
        }

        List<TaskStatus> list = new ArrayList<TaskStatus>( taskStatus.size() );
        for ( com.fedex.phuwv.common.dto.TaskStatus taskStatus1 : taskStatus ) {
            list.add( taskStatusToTaskStatus( taskStatus1 ) );
        }

        return list;
    }

    @Override
    public com.fedex.phuwv.common.api.models.WorkAttribute workAttributeDtoToApiModel(WorkAttribute workAttribute) {
        if ( workAttribute == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.WorkAttribute workAttribute1 = new com.fedex.phuwv.common.api.models.WorkAttribute();

        workAttribute1.setKey( workAttribute.getKey() );
        List<String> list = workAttribute.getValues();
        if ( list != null ) {
            workAttribute1.setValues( new ArrayList<String>( list ) );
        }

        return workAttribute1;
    }

    @Override
    public com.fedex.phuwv.common.api.models.TaskFacilityDetail taskFacilityDetailDtoToApiModel(TaskFacilityDetail taskFacilityDetail) {
        if ( taskFacilityDetail == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.TaskFacilityDetail taskFacilityDetail1 = new com.fedex.phuwv.common.api.models.TaskFacilityDetail();

        taskFacilityDetail1.setLegacyLocationCode( taskFacilityDetail.getLegacyLocationCode() );
        taskFacilityDetail1.setOwner( taskFacilityDetail.getOwner() );
        taskFacilityDetail1.setCode( taskFacilityDetail.getCode() );

        return taskFacilityDetail1;
    }

    protected com.fedex.phuwv.common.api.models.Task taskToTask(Task task) {
        if ( task == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Task task1 = new com.fedex.phuwv.common.api.models.Task();

        task1.setStatuses( taskStatusDtoToApiModel( task.getStatuses() ) );
        task1.setWork( workAttributeDtoToApiModel( task.getWork() ) );
        task1.setTaskFacilitys( taskFacilityDetailDtoToApiModel( task.getTaskFacilitys() ) );

        return task1;
    }

    protected TaskStatus taskStatusToTaskStatus(com.fedex.phuwv.common.dto.TaskStatus taskStatus) {
        if ( taskStatus == null ) {
            return null;
        }

        TaskStatus taskStatus1 = new TaskStatus();

        taskStatus1.setType( taskStatus.getType() );
        taskStatus1.setValue( taskStatus.getValue() );
        taskStatus1.setEffectiveDateTime( taskStatus.getEffectiveDateTime() );

        return taskStatus1;
    }
}
