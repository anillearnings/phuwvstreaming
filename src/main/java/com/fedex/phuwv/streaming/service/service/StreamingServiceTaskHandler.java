package com.fedex.phuwv.streaming.service.service;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.task.v1.api.TaskDomainEvent;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service("StreamingServiceTaskHandler")
@Slf4j
public class StreamingServiceTaskHandler implements MessageHandler<TaskDomainEvent> {

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;

	@Autowired
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Autowired
	private StreamingPublishService streamingPublishService;

	@Autowired
	private LocationTranslationInvoker locationTranslationInvoker;

	private RetryTemplate taskTriggerRetryTemplate;

	private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

	private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

	@Autowired
	public StreamingServiceTaskHandler(@Qualifier("jmsRetryTemplate") RetryTemplate taskTriggerRetryTemplate) {
		this.taskTriggerRetryTemplate = taskTriggerRetryTemplate;
	}

	@Override
	public void handle(GenericMessage<TaskDomainEvent> genericMessage) throws Exception {

		taskTriggerRetryTemplate.execute(retryContext -> {
			try {
				String huUUID = (String) genericMessage.getHeaders().get("HandlingUnitUUID");
				Audit.getInstance().put(AuditKey.IN, "handlingUnitUUID from task internal trigger : " + huUUID);

				if (StringUtils.isNotEmpty(huUUID)) {
					log.info("handlingUnitUUID from hu internal trigger : {}", huUUID);
					if(streamingPublishService.validatePublishingPhuwvJms(huUUID)){
						return null;
					}

					// Call HandlingUnitService (with HandlingUnitUUID from message header)
					HandlingUnit handlingUnit = phuwvServiceInvoker.getLatestHandlingUnitByUUID(huUUID);
					if (Objects.isNull(handlingUnit)) {
						log.warn("No response from Handling unit service for hu uuid:" + huUUID
								+ "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
						return null;
					}

					// Call ShipmentService (use ShipmentUUID from the HandlingUnit PHUWV class
					// returned from step 1)
					//CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID = streamingPublishService.invokeGetFutureLatestShipmentByUUID(handlingUnit.getShipment().getShipmentUUID());
					ShipmentDetails futureLatestShipmentByUUID = streamingPublishService.invokeGetLatestShipmentByUUID(handlingUnit.getShipment().getShipmentUUID());

					// Call TripService by lookup on HandlingUnitUUID.
					//CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(huUUID);
					TripDTO futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetTripByHandlingUnitUUID(huUUID);

					// Call TaskService by lookup on HandlingUnitUUID.
					//CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(huUUID);
					List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(huUUID);

					// Wait until they are all done
					/*CompletableFuture.allOf(futureLatestShipmentByUUID, futureLatestTripByHandlingUnitUUID,
							futureTasksByHandlingUnitUUID).join();*/

					Map<String,Object> tripItineraryWaypointShipmentTaskMap = computeTripItineraryShipmentTaskEthics(handlingUnit, futureLatestTripByHandlingUnitUUID, futureLatestShipmentByUUID, futureTasksByHandlingUnitUUID, huUUID);
					if((boolean) tripItineraryWaypointShipmentTaskMap.get(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull())){
						return null;
					}
					ShipmentDetails shipmentDetails = (ShipmentDetails) tripItineraryWaypointShipmentTaskMap.get(streamingServiceProperties.getPhuwvDomainEventTypeShipment());
					List<Waypoint> waypointModelList = (List<Waypoint>) tripItineraryWaypointShipmentTaskMap.get(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint());
					List<Task> tasks = (List<Task>) tripItineraryWaypointShipmentTaskMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTask());

					// Merge different Models from the calls above into a new/pristine PHUWV
					// HandlingUnit class fully populated.
					streamingPublishService.mergeToPublish(handlingUnit, shipmentDetails, locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointModelList), tasks, streamingServiceProperties.getPhuwvSefsCoreType());
					log.info("End of task trigger task with hu uuid: " + huUUID);
				}
			} catch (Exception e) {
				log.error("Exception in task trigger message:", e);
			}
			return null;
		}, retryContext -> {
			streamingPublishService.setRetryContext(retryContext);
			return null;
		});
	}

	private Map<String,Object> computeTripItineraryShipmentTaskEthics(HandlingUnit handlingUnit, TripDTO futureLatestTripByHandlingUnitUUID, ShipmentDetails futureLatestShipmentByUUID,
																	  List<Task> futureTasksByHandlingUnitUUID, String huUUID) throws ExecutionException, InterruptedException {
		//Map<String,Object> tripAndItineraryMap = streamingPublishService.settingTripItineraryWaypointInfo(futureLatestTripByHandlingUnitUUID, huUUID);
		Map<String,Object> tripAndItineraryMap = streamingPublishService.settingTripItineraryWaypointInfoSync(futureLatestTripByHandlingUnitUUID, huUUID);
		List<Waypoint> waypointModelList = new ArrayList<>();
		ShipmentDetails shipmentDetails;
		List<Task> tasks;
		Map<String,Object> tripItineraryWaypointShipmentTaskMap = new HashMap<>();
		switch (handlingUnit.getType()){
			case HANDLINGUNIT_TYPE_CONSOLIDATION:
				if(!Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTrip())) && !Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()))){
					waypointModelList = streamingPublishService.fetchAndSortWaypoints((ItineraryDTO) tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()));
				}
				//shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(futureLatestShipmentByUUID);
				shipmentDetails = futureLatestShipmentByUUID;
				//tasks = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, huUUID);
				tasks = futureTasksByHandlingUnitUUID;
				break;
			default:
				if(Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTrip())) || Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()))){
					tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
					return tripItineraryWaypointShipmentTaskMap;
				}
				waypointModelList = streamingPublishService.fetchAndSortWaypoints((ItineraryDTO) tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()));

				//shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(futureLatestShipmentByUUID);
				shipmentDetails = futureLatestShipmentByUUID;
				if(streamingPublishService.validateObjectForClosure(shipmentDetails, huUUID)){
					tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
					return tripItineraryWaypointShipmentTaskMap;
				}

				//tasks = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, huUUID);
				tasks = futureTasksByHandlingUnitUUID;
				if(streamingPublishService.validateTaskDetailsForClosure(tasks, huUUID)){
					tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
					return tripItineraryWaypointShipmentTaskMap;
				}
				break;
		}
		tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.FALSE);
		tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint(), waypointModelList);
		tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvDomainEventTypeShipment(), shipmentDetails);
		tripItineraryWaypointShipmentTaskMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTask(), tasks);
		return tripItineraryWaypointShipmentTaskMap;
	}
}
