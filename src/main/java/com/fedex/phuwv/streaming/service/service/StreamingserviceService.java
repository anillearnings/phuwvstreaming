package com.fedex.phuwv.streaming.service.service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.streaming.service.mapper.*;
import org.apache.commons.lang.StringUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class StreamingserviceService {

	@Autowired
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Autowired
	private LocationTranslationInvoker locationTranslationInvoker;

	private HandlingUnitDtoToApiModelMapper handlingUnitDtoToApiModelMapper = Mappers.getMapper(HandlingUnitDtoToApiModelMapper.class);

	private SepDtoToApiModelMapper sepDtoToApiModelMapper = Mappers.getMapper(SepDtoToApiModelMapper.class);

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;

	@Autowired
	private StreamingPublishService streamingPublishService;

	public com.fedex.phuwv.common.api.models.HandlingUnit getLatestHandlingUnit(String id)
	{
		try {

			if (StringUtils.isNotEmpty(id)) {

				Map<String,Object> booleanHandlingUnitMap = settingHandlingUnitBasedOnId(id);
				HandlingUnit handlingUnitFinal = (HandlingUnit) booleanHandlingUnitMap.get(streamingServiceProperties.getPhuwvDomainEventType());
				boolean isUUID= (boolean) booleanHandlingUnitMap.get(streamingServiceProperties.getPhuwvCheckIfUuid());

				Map<String,Object> validateHandlingUnitMap = validateHandlingUnit(handlingUnitFinal, isUUID, id);
				if((boolean) validateHandlingUnitMap.get(streamingServiceProperties.getPhuwvCheckIfHandlingunitNull())){
					return (com.fedex.phuwv.common.api.models.HandlingUnit) validateHandlingUnitMap.get(streamingServiceProperties.getPhuwvDomainEventType());
				}

				handlingUnitFinal = (HandlingUnit) validateHandlingUnitMap.get(streamingServiceProperties.getPhuwvDomainEventType());

				if(null == handlingUnitFinal.getShipment()){
					com.fedex.phuwv.common.api.models.HandlingUnit handlingUnitApiModelsFinal =  handlingUnitDtoToApiModelMapper.huDtoToApiModel(handlingUnitFinal);
					handlingUnitApiModelsFinal.setPhuwvReleaseVersion(streamingServiceProperties.getPhuwvReleaseVersion());
					return handlingUnitApiModelsFinal;
				}

				// Call ShipmentService (with shipment uuid from Handling Unit service)
				CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID = streamingPublishService.invokeGetFutureLatestShipmentByUUID(handlingUnitFinal.getShipment().getShipmentUUID());

				// Call TripService by lookup on HandlingUnitUUID. We can return just a Data
				// Transfer Object(DTO) that has TRIP_UUID for now.
				CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(handlingUnitFinal.getHuUUID());

				//Call TaskService
                CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(handlingUnitFinal.getHuUUID());

				// Wait until they are all done
				CompletableFuture.allOf(futureLatestShipmentByUUID, futureLatestTripByHandlingUnitUUID, futureTasksByHandlingUnitUUID).join();

				List<Waypoint> waypointModelList = settingWaypoints(futureLatestTripByHandlingUnitUUID, isUUID, id);

				ShipmentDetails shipmentDetails = settingShipmentDetails(futureLatestShipmentByUUID, isUUID, id);

				List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, handlingUnitFinal.getHuUUID());

				return streamingPublishService.buildPhuwvAPI(handlingUnitFinal, shipmentDetails, locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointModelList), taskDetails);
			} else {
				log.warn("Incoming handling unit uuid/tracking number is invalid.");
				throw new NullPointerException("null handling unit UUID/TrackNbr");
			}
		} catch (Exception e) {
			log.error("Exception:", e);
		}
		return null;
	}

	private List<Waypoint> settingWaypoints(CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID, boolean isUUID, String id) throws ExecutionException, InterruptedException {
		List<Waypoint> waypointModelList = new ArrayList<>();
		if (futureLatestTripByHandlingUnitUUID.isDone()) {
			// Use the TRIP_UUID to call ItineraryService lookup by TRIP_UUID. We can just
			// return our own ItineraryDTO object with a list containing Sequence Number and
			// corresponding WaypointUUID.
			TripDTO tripDTO = futureLatestTripByHandlingUnitUUID.get();

			if (Objects.isNull(tripDTO)) {
				validateUUID(isUUID, id);
			}else{
				// Call Waypoint Service for every WAYPOINT_UUID. We can then pull together all
				// associated waypoint information associated with the ITINERARY to populate our
				// PHUWV API.
				waypointModelList = streamingPublishService.fetchItineraryAndGetWaypoints(tripDTO.getTripUUID());
			}
		}
		return waypointModelList;
	}

	private ShipmentDetails settingShipmentDetails(CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID, boolean isUUID, String id) throws ExecutionException, InterruptedException {
		ShipmentDetails shipmentDetails = new ShipmentDetails();
		if (futureLatestShipmentByUUID.isDone()) {
			shipmentDetails = futureLatestShipmentByUUID.get();
			if (Objects.isNull(shipmentDetails)) {
				validateUUID(isUUID, id);
			}
		}
		return shipmentDetails;
	}

	private Map<String,Object> settingHandlingUnitBasedOnId(String id) {
		Map<String,Object> booleanHandlingUnitMap = new HashMap<>();
		if(id.contains("-")) {
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvCheckIfUuid(),Boolean.TRUE);
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvDomainEventType(),phuwvServiceInvoker.getLatestHandlingUnitByUUID(id));
		}
		else {
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvCheckIfUuid(),Boolean.FALSE);
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvDomainEventType(),phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(id));
		}
		return booleanHandlingUnitMap;
	}

	private Map<String,Object> validateHandlingUnit(HandlingUnit handlingUnitFinal, boolean isUUID, String id) {
		com.fedex.phuwv.common.api.models.HandlingUnit handlingUnitApiModelsFinal = new com.fedex.phuwv.common.api.models.HandlingUnit();
		handlingUnitApiModelsFinal.setPhuwvReleaseVersion(streamingServiceProperties.getPhuwvReleaseVersion());
		Map<String,Object> booleanHandlingUnitMap = new HashMap<>();
		if (Objects.isNull(handlingUnitFinal)) {
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvCheckIfHandlingunitNull(),Boolean.TRUE);
			if(isUUID){
				log.warn("No response from Handling unit service for hu uuid:" + id);
			} else{
				log.warn("No response from Handling unit service for hu TrackNbr:" + id
						+ " after retries, Calling SEP Service");
				handlingUnitApiModelsFinal = sepDtoToApiModelMapper.sepDtoToApiModel(phuwvServiceInvoker.getFutureHandlingUnitWithSepData(id));
			}
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvDomainEventType(), handlingUnitApiModelsFinal);
		}else{
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvCheckIfHandlingunitNull(),Boolean.FALSE);
			booleanHandlingUnitMap.put(streamingServiceProperties.getPhuwvDomainEventType(), handlingUnitFinal);
		}
		return booleanHandlingUnitMap;
	}

	private void validateUUID(boolean isUUID, String id) {
		if(isUUID)
			log.warn("No response from this service for hu uuid:" + id);
		else
			log.warn("No response from this service for hu TrackNbr:" + id);
	}
}
