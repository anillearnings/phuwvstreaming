package com.fedex.phuwv.streaming.service.configuration;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@ConfigurationProperties(prefix = "phuwv.streaming.service")
@Slf4j
@Getter
@Setter
@NoArgsConstructor
public class StreamingServiceProperties {
	private int corePoolSize = 15;
	private int maxPoolSize = 15;
	private int queueCapacity = 100;
	private int keepAliveSeconds = 60;
	private boolean allowCoreThreadTimeout;
	private int futureTimeoutSecs = 30;
	private int dupTrkNbrThreshold = 10;
	private int maxHandlingUnitTrackNbrs;
	private int feignClientRetryMaxAttempt;
	private int feignClientRetryInterval;
	private String phuwvReleaseVersion;
	private String phuwvSourceType;
	private String phuwvDomainEventType;
	private String waypointOscFacilityopcoTnt;
	private String translateRequestToTargetTypeKey;
	private String translateRequestToTargetTypeValue;
	private String translateRequestToTargetValueKey;
	private String translateRequestFromCriteriaKeyInteropParty;
	private String translateRequestFromCriteriaValueOsc;
	private String translateRequestFromCriteriaKeyInteropOperationId;
	private String translateRequestFromCriteriaKeyBusinessContext;
	private String waypointAssociationLevelHandlingunit;
	private String waypointAssociationLevelTrip;
	private String handlingunitAssociationLevelShipment;
	private String phuwvDomainEventTypeTrip;
	private String phuwvDomainEventTypeItinerary;
	private String phuwvCheckIfUuid;
	private String phuwvCheckIfHandlingunitNull;
	private String phuwvCheckIfTripItineraryShipmentNull;
	private String phuwvDomainEventTypeWaypoint;
	private String phuwvDomainEventTypeShipment;
	private String phuwvDomainEventTypeTask;
	private String phuwvSepCoreType;
	private String phuwvSefsCoreType;
	private boolean phuwvPublishJms;

	@PostConstruct
	public void initialize() {
		log.info("Properties initialized");
	}
}
