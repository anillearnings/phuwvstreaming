package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.dto.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WaypointDtoToApiModelMapper {

    com.fedex.phuwv.common.api.models.Waypoint waypointDtoToApiModel(Waypoint waypointDto);

    List<com.fedex.phuwv.common.api.models.DateTimeFrame> dateTimeFramesDtoToApiModel(List<DateTimeFrame> dateTimeFrames);

    com.fedex.phuwv.common.api.models.Address addressDtoToApiModel(Address address);
}
