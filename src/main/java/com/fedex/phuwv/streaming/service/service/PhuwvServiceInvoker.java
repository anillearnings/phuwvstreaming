package com.fedex.phuwv.streaming.service.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateRequest;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateResponses;
import com.fedex.phuwv.streaming.service.client.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fedex.phuwv.common.exception.NoDataFoundException;

@Service
@Slf4j
public class PhuwvServiceInvoker {

	@Autowired
	private ShipmentServiceClient shipmentServiceClient;

	@Autowired
	private HandlingUnitServiceClient handlingUnitServiceClient;

	@Autowired
	private WaypointServiceClient waypointServiceClient;

	@Autowired
	private TripServiceClient tripServiceClient;

	@Autowired
	private ItineraryServiceClient itineraryServiceClient;
	
	@Autowired
	private SepServiceClient  sepServiceClient;
	
	@Autowired
	private TaskServiceClient  taskServiceClient;

	@Autowired
	private LocationTranslationServiceClient locationTranslationServiceClient;

	private static final String NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITUUID = "No handling unit data found by handlingUnitUUID:";

	private static final String NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR = "No handling unit data found by handlingUnitTrackNbr:";

	private static final String NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID = "No shipment details found by shipmentUUID:";

	private static final String NO_WAYPOINT_DETAILS_FOUND_BY_HUUUID = "No waypoint details found by huUUID:";

	private static final String NO_TRIP_DETAILS_FOUND_BY_HUUUID = "No trip details found by huUUID:";

	private static final String NO_WAYPOINT_DATA_FOUND_BY_WAYPOINTUUID = "No waypoint data found by waypointUUID:";

	private static final String NO_TASK_DETAILS_FOUND_BY_HUUUID = "No task details found by huUUID:";

	@Async("asyncTaskExecutor")
	public CompletableFuture<HandlingUnit> getFutureLatestHandlingUnitByUUID(String handlingUnitUUID) {
		ResponseEntity<HandlingUnit> handlingUnitResponseEntity = handlingUnitServiceClient
				.getLatestHandlingUnitByUUID(handlingUnitUUID);
		if (handlingUnitResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITUUID + handlingUnitUUID);
			throw new NoDataFoundException(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITUUID + handlingUnitUUID);
		}
		return CompletableFuture.completedFuture(handlingUnitResponseEntity.getBody());
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<HandlingUnit> getFutureLatestHandlingUnitByTrackNbr(String handlingUnitTrackNbr) {
		ResponseEntity<HandlingUnit> handlingUnitResponseEntity = handlingUnitServiceClient
				.getLatestHandlingUnitByTrackNbr(handlingUnitTrackNbr);
		if (handlingUnitResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + handlingUnitTrackNbr);
			throw new NoDataFoundException(
					NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + handlingUnitTrackNbr);
		}
		return CompletableFuture.completedFuture(handlingUnitResponseEntity.getBody());
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<ShipmentDetails> getFutureLatestShipmentByUUID(String shipmentUUID) {
		ShipmentDetails shipmentModel = null;
		log.info("Using ShipmentServiceClient to get ShipmentModel using shipmentUUID: " + shipmentUUID);
		try {
			ResponseEntity<ShipmentDetails> serviceResponse = shipmentServiceClient
					.getLatestShipmentByUUID(shipmentUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID);
				CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
				shipmentDetailsCompletableFuture.completeExceptionally(new NoDataFoundException(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID));
				return shipmentDetailsCompletableFuture;
			}
			if (Objects.nonNull(serviceResponse.getBody() ) ) {
				shipmentModel = serviceResponse.getBody();
				log.info(" shipment details using Handling unit Handler for shipment uuid: {}", shipmentUUID);
			} else {
				log.warn(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID);
				CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
				shipmentDetailsCompletableFuture.completeExceptionally(new NoDataFoundException(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID));
				return shipmentDetailsCompletableFuture;
			}
		} catch (Exception e) {
			log.error("Exception when fetching shipment details for shipment uuid: {} occurred: {}", shipmentUUID, e);
			CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
			shipmentDetailsCompletableFuture.completeExceptionally(e);
			return shipmentDetailsCompletableFuture;
		}
		return CompletableFuture.completedFuture(shipmentModel);
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<Waypoint> getFutureLatestWaypointByUUID(String waypointUUID) {
		ResponseEntity<Waypoint> waypointResponseEntity = waypointServiceClient
				.getLatestWaypointByUUID(waypointUUID);
		if (waypointResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No waypoint details found by waypointUUID : " + waypointUUID);
			throw new NoDataFoundException("No waypoint details found by waypointUUID : " + waypointUUID);
		}
		return CompletableFuture.completedFuture(waypointResponseEntity.getBody());
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<Waypoint> getFutureLatestWaypointByTrackNbr(String waypointTrkNbr) {
		ResponseEntity<Waypoint> waypointResponseEntity = waypointServiceClient
				.getLatestWaypointByTrackNbr(waypointTrkNbr);
		if (waypointResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No waypoint details found by waypointTrkNbr : " + waypointTrkNbr);
			throw new NoDataFoundException("No waypoint details found by waypointTrkNbr : " + waypointTrkNbr);
		}
		return CompletableFuture.completedFuture(waypointResponseEntity.getBody());
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<Waypoint> getFutureWaypointByEnterpriseTrackNbr(String waypointTrackNbr) {
		ResponseEntity<Waypoint> waypointResponseEntity = waypointServiceClient
				.getWaypointByEnterpriseTrackNbr(waypointTrackNbr);
		if (waypointResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No waypoint details found by waypointTrackNbr : " + waypointTrackNbr);
			throw new NoDataFoundException("No waypoint details found by waypointTrackNbr : " + waypointTrackNbr);
		}
		return CompletableFuture.completedFuture(waypointResponseEntity.getBody());
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<List<Waypoint>> getFutureLatestWaypointByHandlingUnitUUID(String huUUID) {
		List<Waypoint> waypointModel = null;
		log.info("Using WaypointServiceClient to get WaypointModel using handling unit UUID: " + huUUID);
		try {
			ResponseEntity<List<Waypoint>> serviceResponse = waypointServiceClient
					.getLatestWaypointByHandlingUnitUUID(huUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_WAYPOINT_DETAILS_FOUND_BY_HUUUID + huUUID);
				CompletableFuture<List<Waypoint>> waypointListCompletableFuture = new CompletableFuture<>();
				waypointListCompletableFuture.completeExceptionally(new NoDataFoundException(NO_WAYPOINT_DETAILS_FOUND_BY_HUUUID + huUUID));
				return waypointListCompletableFuture;
			}
			if (serviceResponse.getBody() != null) {
				waypointModel = serviceResponse.getBody();
				log.info(" waypoint model details: {}", waypointModel);
			} else {
				log.warn(NO_WAYPOINT_DETAILS_FOUND_BY_HUUUID + huUUID);
				CompletableFuture<List<Waypoint>> waypointListCompletableFuture = new CompletableFuture<>();
				waypointListCompletableFuture.completeExceptionally(new NoDataFoundException(NO_WAYPOINT_DETAILS_FOUND_BY_HUUUID + huUUID));
				return waypointListCompletableFuture;
			}
		} catch (Exception e) {
			log.error("Exception when fetching waypoint details for hu uuid: {} occurred: {}",huUUID,e);
			CompletableFuture<List<Waypoint>> waypointListCompletableFuture = new CompletableFuture<>();
			waypointListCompletableFuture.completeExceptionally(e);
			return waypointListCompletableFuture;
		}
		return CompletableFuture.completedFuture(waypointModel);
	}

	public ShipmentDetails getLatestShipmentByUUID(String shipmentUUID) {
		ResponseEntity<ShipmentDetails> shipmentDetailsResponseEntity = shipmentServiceClient
				.getLatestShipmentByUUID(shipmentUUID);
		if (shipmentDetailsResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID);
			throw new NoDataFoundException(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID);
		}
		return shipmentDetailsResponseEntity.getBody();
	}

	public HandlingUnit getLatestHandlingUnitByUUID(String huUUID) {
		HandlingUnit handlingUnitModel = null;
		log.info("Using HandlingUnitServiceClient to get HandlingUnitModel using handling unit UUID: " + huUUID);
		try {
			ResponseEntity<HandlingUnit> serviceResponse = handlingUnitServiceClient
					.getLatestHandlingUnitByUUID(huUUID);
			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				Exception e = new Exception(Integer.toString(status.value()));
				log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITUUID + huUUID);
				throw e;
			}
			if (Objects.nonNull(serviceResponse.getBody())) {
				handlingUnitModel = serviceResponse.getBody();
				log.info(" uuid from handling unit model: {}", handlingUnitModel);
			} else {
				log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITUUID + huUUID);
				throw new NoDataFoundException(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITUUID + huUUID);
			}
		} catch (Exception e) {
			log.error("Exception when fetching handling unit for hu uuid: {} occurred: {}",huUUID,e);
		}
		return handlingUnitModel;
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<TripDTO> getFutureLatestTripByHandlingUnitUUID(String huUUID) {
		TripDTO tripDTO = null;
		log.info("Using TripServiceClient to get TripDTO using handling unit UUID: " + huUUID);
		try {
			ResponseEntity<TripDTO> serviceResponse = tripServiceClient
					.getLatestTripByHandlingUnitUUID(huUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				Exception e = new Exception(Integer.toString(status.value()));
				log.warn(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID);
				CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
				tripDTOCompletableFuture.completeExceptionally(e);
				return tripDTOCompletableFuture;
			}
			if (Objects.nonNull(serviceResponse.getBody())) {
				tripDTO = serviceResponse.getBody();
				log.info(" trip dto details: {}", tripDTO);
			} else {
				log.warn(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID);
				CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
				tripDTOCompletableFuture.completeExceptionally(new NoDataFoundException(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID));
				return tripDTOCompletableFuture;
			}
		} catch (Exception e) {
			log.error("Exception when fetching trip details for hu uuid: {} occurred: {}",huUUID,e);
			CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
			tripDTOCompletableFuture.completeExceptionally(e);
			return tripDTOCompletableFuture;
		}
		return CompletableFuture.completedFuture(tripDTO);
	}
	
	public TripDTO getLatestHandlingUnitUUIDByTripUUID(String tripUUID) {
		TripDTO tripDTO = null;
		log.info("Using TripServiceClient to get TripDTO using trip UUID: " + tripUUID);
		try {
			ResponseEntity<TripDTO> serviceResponse = tripServiceClient
					.getLatestHandlingUnitUUIDByTripUUID(tripUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				Exception e = new Exception(Integer.toString(status.value()));
				log.warn("No trip data found by tripUUID : " + tripUUID);
				throw e;
			}
			if (Objects.nonNull(serviceResponse.getBody())) {
				tripDTO= serviceResponse.getBody();
				log.info(" uuid from trip dto: {}", tripDTO);
			} else {
				log.warn("No data found by tripUUID : " + tripUUID);
			}
		} catch (Exception e) {
			log.error("Exception when fetching handling unit for trip uuid: {} occurred: {}",tripUUID,e);
		}
		return tripDTO;
	}


	public ItineraryDTO getLatestItineraryByTripUUID(String tripUUID) {
		ItineraryDTO itineraryDTO = null;
		log.info("Using ItineraryServiceClient to get ItineraryDTO using trip UUID: " + tripUUID);
		try {
			ResponseEntity<ItineraryDTO> serviceResponse = itineraryServiceClient
					.getLatestItineraryByTripUUID(tripUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn("No itinerary data found by tripUUID : " + tripUUID);
			}
			if (Objects.nonNull(serviceResponse.getBody())) {
				itineraryDTO = serviceResponse.getBody();
				log.info(" uuid from itinerary dto: {}", itineraryDTO);
			} else {
				log.warn("No itinerary data found by tripUUID : " + tripUUID);
			}
		} catch (Exception e) {
			log.error("Exception when fetching itinerary data for trip uuid: {} occurred: {},",tripUUID,e);
			return itineraryDTO;
		}
		return itineraryDTO;
	}

	public Waypoint getLatestWaypointByUUID(String waypointUUID) {
		Waypoint waypoint = null;
		log.info("Using WaypointServiceClient to get Waypoint using waypoint UUID: " + waypointUUID);
		try {
			ResponseEntity<Waypoint> serviceResponse = waypointServiceClient
					.getLatestWaypointByUUID(waypointUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				Exception e = new Exception(Integer.toString(status.value()));
				log.warn(NO_WAYPOINT_DATA_FOUND_BY_WAYPOINTUUID + waypointUUID);
				throw e;
			}
			if (Objects.nonNull (serviceResponse.getBody() )) {
				waypoint = serviceResponse.getBody();
				log.info(" uuid from waypoint model: {}", waypoint);
			} else {
				log.warn(NO_WAYPOINT_DATA_FOUND_BY_WAYPOINTUUID + waypointUUID);
				throw new NoDataFoundException(NO_WAYPOINT_DATA_FOUND_BY_WAYPOINTUUID + waypointUUID);
			}
		} catch (Exception e) {
			log.error("Exception when fetching waypoint data for waypoint uuid: {} occurred: {}",waypointUUID,e);
		}
		return waypoint;
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<List<HandlingUnit>> getFutureListHandlingUnitByShipmentUUID(String shipmentUUID) {
		ResponseEntity<List<HandlingUnit>> handlingUnitResponseEntity = handlingUnitServiceClient
				.getListOfLatestHandlingUnitByShipmentUUID(shipmentUUID);
		if (handlingUnitResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No handling unit data found by shipmentUUID : " + shipmentUUID);
			throw new NoDataFoundException("No handling unit data found by shipmentUUID : " + shipmentUUID);
		}
		return CompletableFuture.completedFuture(handlingUnitResponseEntity.getBody());
	}
	
	
	public  HandlingUnit getLatestHandlingUnitBySepTrackingNbr(String sepTrackingNumber) {
		ResponseEntity<HandlingUnit> handlingUnitResponseEntity = handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(sepTrackingNumber);
		if (handlingUnitResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + sepTrackingNumber);
			throw new NoDataFoundException(
					NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + sepTrackingNumber);
		}
		return handlingUnitResponseEntity.getBody();
	}
	
	
	public  HandlingUnit getFutureHandlingUnitWithSepData(String sepTrackingNumber) {
		ResponseEntity<HandlingUnit> handlingUnitResponseEntity = sepServiceClient.getLatestHUFromSEPByTrackingNbr(sepTrackingNumber);
		if (handlingUnitResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No handling unit data found by SepTrackNbr From SEP Service : " + sepTrackingNumber);
			throw new NoDataFoundException(
					"No handling unit data found by SepTrackNbr From SEP Service : " + sepTrackingNumber);
		}
		return handlingUnitResponseEntity.getBody();
	}

	 
	 
	
	public HandlingUnit getLatestHandlingUnitByTrackNbr(String trackNbr) {
		HandlingUnit handlingUnitModel = null;
		log.info("Using HandlingUnitServiceClient to get HandlingUnitModel using handling unit TrackNbr: " + trackNbr);
		try {
			ResponseEntity<HandlingUnit> serviceResponse = handlingUnitServiceClient
					.getLatestHandlingUnitByTrackNbr(trackNbr);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				Exception e = new Exception(Integer.toString(status.value()));
				log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + trackNbr);
				throw e;
			}
			if (serviceResponse.getBody() != null) {
				handlingUnitModel = serviceResponse.getBody();
				log.info(" trackNbr from handling unit model: {}", handlingUnitModel);
			} else {
				log.warn(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + trackNbr);
				throw new NoDataFoundException(NO_HANDLING_UNIT_DATA_FOUND_BY_HANDLINGUNITTRACKNBR + trackNbr);
			}
		} catch (Exception e) {
			log.error("Exception when fetching handling unit data for tracking number: {} occurred: {}",trackNbr,e);
		}
		return handlingUnitModel;
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<HandlingUnit> getFutureHandlingUnitByUUID(String handlingUnitUUID) {
		return CompletableFuture.completedFuture(getLatestHandlingUnitByUUID(handlingUnitUUID));
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<ShipmentDetails> getFutureShipmentByUUID(String shipmentUUID) {
		ShipmentDetails shipmentModel = null;
		try {
			ResponseEntity<ShipmentDetails> serviceResponse = shipmentServiceClient
					.getLatestShipmentByUUID(shipmentUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID);
				CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
				shipmentDetailsCompletableFuture.completeExceptionally(new NoDataFoundException(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID));
				return shipmentDetailsCompletableFuture;
			}
			if (serviceResponse.getBody() != null) {
				shipmentModel = serviceResponse.getBody();
				log.info(" shipment details using Handling unit Handler: {}", shipmentModel.toString());
			} else {
				CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
				shipmentDetailsCompletableFuture.completeExceptionally(new NoDataFoundException(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID));
				return shipmentDetailsCompletableFuture;
			}
		} catch (Exception e) {
			CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
			shipmentDetailsCompletableFuture.completeExceptionally(e);
			return shipmentDetailsCompletableFuture;
		}
		return CompletableFuture.completedFuture(shipmentModel);
	}

	@Async("asyncTaskExecutor")
	public CompletableFuture<TripDTO> getFutureTripByHandlingUnitUUID(String huUUID) {
		TripDTO tripDTO = null;
		try {
			ResponseEntity<TripDTO> serviceResponse = tripServiceClient
					.getLatestTripByHandlingUnitUUID(huUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID);
				CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
				tripDTOCompletableFuture.completeExceptionally(new NoDataFoundException(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID));
				return tripDTOCompletableFuture;
			}
			if (serviceResponse.getBody() != null) {
				tripDTO = serviceResponse.getBody();
			} else {
				CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
				tripDTOCompletableFuture.completeExceptionally(new NoDataFoundException(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID));
				return tripDTOCompletableFuture;
			}
		} catch (Exception e) {
			CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
			tripDTOCompletableFuture.completeExceptionally(e);
			return tripDTOCompletableFuture;
		}
		return CompletableFuture.completedFuture(tripDTO);
	}
	
	@Async("asyncTaskExecutor")
	public CompletableFuture<List<Task>> getFutureTaskByHandlingUnitUUID(String huUUID) {
		List<Task> tasks = null;
		try {
			ResponseEntity<List<Task>> serviceResponse = taskServiceClient
					.getTasksByHandlingUnitUUID(huUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_TASK_DETAILS_FOUND_BY_HUUUID + huUUID);
				CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
				taskCompletableFuture.completeExceptionally(new NoDataFoundException(NO_TASK_DETAILS_FOUND_BY_HUUUID + huUUID));
				return taskCompletableFuture;
			}
			if (serviceResponse.getBody() != null) {
				tasks = serviceResponse.getBody();
			} else {
				CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
				taskCompletableFuture.completeExceptionally(new NoDataFoundException(NO_TASK_DETAILS_FOUND_BY_HUUUID + huUUID));
				return taskCompletableFuture;
			}
		} catch (Exception e) {
			CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
			taskCompletableFuture.completeExceptionally(e);
			return taskCompletableFuture;
		}
		return CompletableFuture.completedFuture(tasks);
	}

	
	public BulkTranslateResponses getFutureTranslatedLocationCode(BulkTranslateRequest request) {
		ResponseEntity<BulkTranslateResponses> translatedLocationResponse = locationTranslationServiceClient
				.getLocationTranslation(request);
		if (translatedLocationResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No translatedLocationResponse found for: " + request);
			throw new NoDataFoundException("No translatedLocationResponse found for : " + request);
		}
		return translatedLocationResponse.getBody();
	}

	public List<Waypoint> getLatestWaypointsByUUIDs(List<String> waypointUUIDs) {
		List<Waypoint> waypoints;
		log.info("Using WaypointServiceClient to get Waypoints using waypoint UUIDs: " + waypointUUIDs);
		try {
			ResponseEntity<List<Waypoint>> serviceResponse = waypointServiceClient
					.getLatestWaypointsByUUIDs(waypointUUIDs);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_WAYPOINT_DATA_FOUND_BY_WAYPOINTUUID + waypointUUIDs);
				return new ArrayList<>();
			}
			if (Objects.nonNull (serviceResponse.getBody() )) {
				waypoints = serviceResponse.getBody();
				log.info(" For waypointUUIDs: {}, waypoint data found: {}", waypointUUIDs, waypoints);
			} else {
				log.warn(NO_WAYPOINT_DATA_FOUND_BY_WAYPOINTUUID + waypointUUIDs);
				return new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Exception when fetching waypoint data for waypoint uuids: {} occurred: {}",waypointUUIDs,e);
			return new ArrayList<>();
		}
		return waypoints;
	}

	public ShipmentDetails getShipmentByUUID(String shipmentUUID) {
		ShipmentDetails shipmentModel = null;
		try {
			ResponseEntity<ShipmentDetails> serviceResponse = shipmentServiceClient
					.getLatestShipmentByUUID(shipmentUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID + shipmentUUID);
				return null;
			}
			if (serviceResponse.getBody() != null) {
				shipmentModel = serviceResponse.getBody();
				log.info(" shipment details using Handling unit Handler: {}", shipmentModel.toString());
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
		return shipmentModel;
	}

	public TripDTO getTripByHandlingUnitUUID(String huUUID) {
		TripDTO tripDTO = null;
		try {
			ResponseEntity<TripDTO> serviceResponse = tripServiceClient
					.getLatestTripByHandlingUnitUUID(huUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_TRIP_DETAILS_FOUND_BY_HUUUID + huUUID);
				return null;
			}
			if (serviceResponse.getBody() != null) {
				tripDTO = serviceResponse.getBody();
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
		return tripDTO;
	}

	public List<Task> getTaskByHandlingUnitUUID(String huUUID) {
		List<Task> tasks = null;
		try {
			ResponseEntity<List<Task>> serviceResponse = taskServiceClient
					.getTasksByHandlingUnitUUID(huUUID);

			HttpStatus status = serviceResponse.getStatusCode();
			if (!(status.is2xxSuccessful())) {
				log.warn(NO_TASK_DETAILS_FOUND_BY_HUUUID + huUUID);
				return null;
			}
			if (serviceResponse.getBody() != null) {
				tasks = serviceResponse.getBody();
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
		return tasks;
	}

	public List<HandlingUnit> getListHandlingUnitByShipmentUUID(String shipmentUUID) {
		ResponseEntity<List<HandlingUnit>> handlingUnitResponseEntity = handlingUnitServiceClient
				.getListOfLatestHandlingUnitByShipmentUUID(shipmentUUID);
		if (handlingUnitResponseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			log.warn("No handling unit data found by shipmentUUID : " + shipmentUUID);
			throw new NoDataFoundException("No handling unit data found by shipmentUUID : " + shipmentUUID);
		}
		return handlingUnitResponseEntity.getBody();
	}
}
