package com.fedex.phuwv.streaming.service.service;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.itinerary.v1.api.ItineraryDomainEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service("StreamingServiceItineraryHandler")
@Slf4j
public class StreamingServiceItineraryHandler implements MessageHandler<ItineraryDomainEvent> {

    private RetryTemplate itineraryTriggerRetryTemplate;

    @Autowired
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Autowired
    private LocationTranslationInvoker locationTranslationInvoker;

    @Autowired
    private StreamingPublishService streamingPublishService;
   
    @Autowired
    private StreamingServiceProperties streamingServiceProperties;

    private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

    private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

    @Autowired
    public StreamingServiceItineraryHandler(@Qualifier("jmsRetryTemplate") RetryTemplate itineraryTriggerRetryTemplate) {
        this.itineraryTriggerRetryTemplate = itineraryTriggerRetryTemplate;

    }

    @Override
    public void handle(GenericMessage<ItineraryDomainEvent> genericMessage) throws  Exception {
        // Synchronous method call :: START

        itineraryTriggerRetryTemplate.execute(retryContext -> {
            try {
                ItineraryDomainEvent message = genericMessage.getPayload();
                String tripUUID = message.getItineraryDescribesTripAssociation().getTripUUID();
                Audit.getInstance().put(AuditKey.IN, "Trip uuid used for itinerary trigger event:" + tripUUID);
                log.info("Trip uuid used by this itinerary trigger event: {}", tripUUID);

                if (StringUtils.isNotEmpty(tripUUID)) {
                    if(streamingPublishService.validatePublishingPhuwvJms(tripUUID)){
                        return null;
                    }

                    //Call TripService(use TripUUID from the message property to get the HandlingUnitUUID)
                    TripDTO futureLatestTripByHandlingUnitUUID = phuwvServiceInvoker
                            .getLatestHandlingUnitUUIDByTripUUID(tripUUID);

                    //Call HandlingUnitService (use HandlingUnitUUID from the data returned from step 3)
                    HandlingUnit handlingUnit = (null != futureLatestTripByHandlingUnitUUID)?phuwvServiceInvoker.getLatestHandlingUnitByUUID(
                            futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID()):null;
                    if (Objects.isNull(handlingUnit)) {
                        log.warn("No response from Handling unit service for trip uuid:" + tripUUID
                                + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
                        return null;
                    }

                    //Call ShipmentService (use ShipmentUUID from the HandlingUnit PHUWV class returned from step 4)
                    //CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID = streamingPublishService.invokeGetFutureLatestShipmentByUUID(handlingUnit.getShipment().getShipmentUUID());
                    ShipmentDetails futureLatestShipmentByUUID = streamingPublishService.invokeGetLatestShipmentByUUID(handlingUnit.getShipment().getShipmentUUID());

                    //Call TaskService
                    //CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID());
                    List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID());

                    // Wait until they are all done
                    //CompletableFuture.allOf(futureLatestShipmentByUUID, futureTasksByHandlingUnitUUID).join();

                    //ShipmentDetails shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(futureLatestShipmentByUUID);
                    ShipmentDetails shipmentDetails = futureLatestShipmentByUUID;
                    if(validateShipmentDetails(handlingUnit, shipmentDetails)){
                        return null;
                    }

                    // Call ItineraryService lookup by ITINERARY_UUID..We can just return our own
                    // ItineraryDTO object with a list containing Sequence Number and corresponding
                    // WaypointUUID.

                    // Call Waypoint Service for every WAYPOINT_UUID from the list in step 4.2. We
                    // can then pull together all associated waypoint information associated with
                    // the ITINERARY to populate our PHUWV API.

                    List<Waypoint> waypointModelList = streamingPublishService.fetchItineraryAndGetWaypoints(tripUUID);

                    //List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID());
                    List<Task> taskDetails = futureTasksByHandlingUnitUUID;

                    // Merge different Models from the calls above into a new/pristine PHUWV
                    // HandlingUnit class fully populated.
                    streamingPublishService.mergeToPublish(handlingUnit, shipmentDetails, locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointModelList), taskDetails, streamingServiceProperties.getPhuwvSefsCoreType());
                    log.info("End of itinerary trigger event implemented using trip uuid: " + tripUUID);
                }
            } catch (Exception e) {
                log.error("Exception:", e);
            }
            return null;
        }, retryContext -> {
            streamingPublishService.setRetryContext(retryContext);
            return null;
        });
        // Synchronous method call :: END
    }

    private boolean validateShipmentDetails(HandlingUnit handlingUnit, ShipmentDetails shipmentDetails) {
        if(streamingPublishService.validateObjectForClosure(shipmentDetails, handlingUnit.getShipment().getShipmentUUID())){
            switch (handlingUnit.getType()){
                case HANDLINGUNIT_TYPE_CONSOLIDATION:
                    return false;
                default:
                    return true;
            }
        }
        return false;
    }
}
