package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.api.models.DateTimeFrame;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.dto.Address;
import com.fedex.phuwv.common.dto.DomainStatus;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-10-12T17:08:48+0530",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-6.8.2.jar, environment: Java 1.8.0_281 (Oracle Corporation)"
)
@Component
public class WaypointDtoToApiModelMapperImpl implements WaypointDtoToApiModelMapper {

    @Override
    public Waypoint waypointDtoToApiModel(com.fedex.phuwv.common.dto.Waypoint waypointDto) {
        if ( waypointDto == null ) {
            return null;
        }

        Waypoint waypoint = new Waypoint();

        if ( waypointDto.getType() != null ) {
            waypoint.setType( waypointDto.getType().name() );
        }
        waypoint.setEngageWindows( dateTimeFramesDtoToApiModel( waypointDto.getEngageWindows() ) );
        waypoint.setSequenceNumber( waypointDto.getSequenceNumber() );
        waypoint.setAddress( addressDtoToApiModel( waypointDto.getAddress() ) );
        waypoint.setFacilityOpCo( waypointDto.getFacilityOpCo() );
        waypoint.setFacilityCode( waypointDto.getFacilityCode() );
        waypoint.setFacilityCountryCode( waypointDto.getFacilityCountryCode() );
        waypoint.setStatuses( domainStatusListToDomainStatusList( waypointDto.getStatuses() ) );

        return waypoint;
    }

    @Override
    public List<DateTimeFrame> dateTimeFramesDtoToApiModel(List<com.fedex.phuwv.common.dto.DateTimeFrame> dateTimeFrames) {
        if ( dateTimeFrames == null ) {
            return null;
        }

        List<DateTimeFrame> list = new ArrayList<DateTimeFrame>( dateTimeFrames.size() );
        for ( com.fedex.phuwv.common.dto.DateTimeFrame dateTimeFrame : dateTimeFrames ) {
            list.add( dateTimeFrameToDateTimeFrame( dateTimeFrame ) );
        }

        return list;
    }

    @Override
    public com.fedex.phuwv.common.api.models.Address addressDtoToApiModel(Address address) {
        if ( address == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Address address1 = new com.fedex.phuwv.common.api.models.Address();

        address1.setCountryCode( address.getCountryCode() );
        address1.setCity( address.getCity() );
        address1.setStateCode( address.getStateCode() );
        address1.setPostal( address.getPostal() );
        address1.setAddressLine1( address.getAddressLine1() );
        address1.setAddressLine2( address.getAddressLine2() );
        address1.setAddressLine3( address.getAddressLine3() );

        return address1;
    }

    protected com.fedex.phuwv.common.api.models.DomainStatus domainStatusToDomainStatus(DomainStatus domainStatus) {
        if ( domainStatus == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.DomainStatus domainStatus1 = new com.fedex.phuwv.common.api.models.DomainStatus();

        domainStatus1.setStatusType( domainStatus.getStatusType() );
        domainStatus1.setStatusValue( domainStatus.getStatusValue() );

        return domainStatus1;
    }

    protected List<com.fedex.phuwv.common.api.models.DomainStatus> domainStatusListToDomainStatusList(List<DomainStatus> list) {
        if ( list == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.DomainStatus> list1 = new ArrayList<com.fedex.phuwv.common.api.models.DomainStatus>( list.size() );
        for ( DomainStatus domainStatus : list ) {
            list1.add( domainStatusToDomainStatus( domainStatus ) );
        }

        return list1;
    }

    protected DateTimeFrame dateTimeFrameToDateTimeFrame(com.fedex.phuwv.common.dto.DateTimeFrame dateTimeFrame) {
        if ( dateTimeFrame == null ) {
            return null;
        }

        DateTimeFrame dateTimeFrame1 = new DateTimeFrame();

        dateTimeFrame1.setType( dateTimeFrame.getType() );
        dateTimeFrame1.setBeginDateTime( dateTimeFrame.getBeginDateTime() );
        dateTimeFrame1.setEndDateTime( dateTimeFrame.getEndDateTime() );

        return dateTimeFrame1;
    }
}
