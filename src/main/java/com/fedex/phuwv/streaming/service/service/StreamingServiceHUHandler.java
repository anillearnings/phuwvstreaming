package com.fedex.phuwv.streaming.service.service;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service("StreamingServiceHUHandler")
@Slf4j
public class StreamingServiceHUHandler implements MessageHandler<HandlingUnitDomainEvent> {

    private RetryTemplate huTriggerRetryTemplate;

    @Autowired
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Autowired
    private StreamingPublishService streamingPublishService;
    
    @Autowired
    private LocationTranslationInvoker locationTranslationInvoker;

    @Autowired
    private StreamingServiceProperties streamingServiceProperties;

    private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

    private static final String COMPLETED_EXCEPTIONALLY = "Completed exceptionally:";

    private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

    @Autowired
    public StreamingServiceHUHandler(@Qualifier("jmsRetryTemplate") RetryTemplate huTriggerRetryTemplate) {
        this.huTriggerRetryTemplate = huTriggerRetryTemplate;
    }

    @Override
    public void handle(GenericMessage<HandlingUnitDomainEvent> genericMessage) throws  Exception {

        huTriggerRetryTemplate.execute(retryContext -> {
            try {
                HandlingUnitDomainEvent message = genericMessage.getPayload();
                String huUUID = String.valueOf(message.getHandlingUnit().getSystemIdentificationProfile().getUuid());
                Audit.getInstance().put(AuditKey.IN, "handlingUnitUUID from hu internal trigger : " + huUUID);

                if (StringUtils.isNotEmpty(huUUID)) {
                    log.info("handlingUnitUUID from hu internal trigger : {}", huUUID);
                    if(streamingPublishService.validatePublishingPhuwvJms(huUUID)){
                        return null;
                    }

                    // Call HandlingUnitService (with HandlingUnitUUID from Msg Properties)
                    //CompletableFuture<HandlingUnit> futureLatestHandlingUnitByUUID = invokeGetFutureHandlingUnitByUUID(huUUID);
                    HandlingUnit handlingUnitFinal = invokeGetHandlingUnitByUUID(huUUID);

                    // Call ShipmentService (with shipment uuid from Handling Unit service)
                    //CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID = invokeGetFutureShipmentByUUID(message.getAssociations().stream().filter(association -> association.getAssociationLevel().equalsIgnoreCase(streamingServiceProperties.getHandlingunitAssociationLevelShipment())).findAny().get().getAssociationUUID());
                    ShipmentDetails futureLatestShipmentByUUID = invokeGetShipmentByUUID(message.getAssociations().stream().filter(association -> association.getAssociationLevel().equalsIgnoreCase(streamingServiceProperties.getHandlingunitAssociationLevelShipment())).findAny().get().getAssociationUUID());

                    // Call TripService by lookup on HandlingUnitUUID. We can return just a Data
                    // Transfer Object(DTO) that has TRIP_UUID for now.
                    //CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(huUUID);
                    TripDTO futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetTripByHandlingUnitUUID(huUUID);

                    //Call TaskService
                    //CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(huUUID);
                    List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(huUUID);

                    // Wait until they are all done
                    //CompletableFuture.allOf(futureLatestHandlingUnitByUUID, futureLatestShipmentByUUID, futureLatestTripByHandlingUnitUUID, futureTasksByHandlingUnitUUID).join();

                    //HandlingUnit handlingUnitFinal = streamingPublishService.settingHandlingUnit(futureLatestHandlingUnitByUUID);
                    if(streamingPublishService.validateHandlingUnitForClosure(handlingUnitFinal, huUUID)){
                        return null;
                    }

                    Map<String,Object> tripItineraryWaypointShipmentMap = computeTripItineraryShipmentDetails(handlingUnitFinal, futureLatestTripByHandlingUnitUUID, futureLatestShipmentByUUID, huUUID);
                    if((boolean) tripItineraryWaypointShipmentMap.get(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull())){
                        return null;
                    }
                    ShipmentDetails shipmentDetails = (ShipmentDetails) tripItineraryWaypointShipmentMap.get(streamingServiceProperties.getPhuwvDomainEventTypeShipment());
                    List<Waypoint> waypointModelList = (List<Waypoint>) tripItineraryWaypointShipmentMap.get(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint());

                    //List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, huUUID);
                    List<Task> taskDetails = futureTasksByHandlingUnitUUID;

                    // Merge different Models from the calls above into a new/pristine PHUWV HandlingUnit class fully populated.
                    streamingPublishService.mergeToPublish(handlingUnitFinal, shipmentDetails, locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointModelList), taskDetails, streamingServiceProperties.getPhuwvSefsCoreType());
                    log.info("End of handling unit trigger task with hu uuid: " + huUUID);
                }
            } catch (Exception e) {
                log.error("Exception in handling unit trigger message:", e);
            }
            return null;
        }, retryContext -> {
            streamingPublishService.setRetryContext(retryContext);
            return null;
        });
    }

    private CompletableFuture<HandlingUnit> invokeGetFutureHandlingUnitByUUID(String huUUID) {
        return phuwvServiceInvoker
                .getFutureHandlingUnitByUUID(huUUID)
                .exceptionally(ex -> {
                    log.warn(WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT + " as invoking Handling unit service for hu uuid:" + huUUID
                            + " caused exception: " + ex);
                    return null;
                });
    }

    private CompletableFuture<ShipmentDetails> invokeGetFutureShipmentByUUID(String associationUUID) {
        return phuwvServiceInvoker
                .getFutureShipmentByUUID(associationUUID)
                .exceptionally(ex -> {
                    log.warn(COMPLETED_EXCEPTIONALLY, ex);
                    return null;
                });
    }

    private Map<String,Object> computeTripItineraryShipmentDetails(HandlingUnit handlingUnitFinal, TripDTO futureLatestTripByHandlingUnitUUID, ShipmentDetails futureLatestShipmentByUUID,
                                                        String huUUID) throws ExecutionException, InterruptedException {
        //Map<String,Object> tripAndItineraryMap = streamingPublishService.settingTripItineraryWaypointInfo(futureLatestTripByHandlingUnitUUID, huUUID);
        Map<String,Object> tripAndItineraryMap = streamingPublishService.settingTripItineraryWaypointInfoSync(futureLatestTripByHandlingUnitUUID, huUUID);
        List<Waypoint> waypointModelList = new ArrayList<>();
        ShipmentDetails shipmentDetails;
        Map<String,Object> tripItineraryWaypointShipmentMap = new HashMap<>();
        switch (handlingUnitFinal.getType()){
            case HANDLINGUNIT_TYPE_CONSOLIDATION:
                if(!Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTrip())) && !Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()))){
                    waypointModelList = streamingPublishService.fetchAndSortWaypoints((ItineraryDTO) tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()));
                }
                //shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(futureLatestShipmentByUUID);
                shipmentDetails = futureLatestShipmentByUUID;
                break;
            default:
                if(Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTrip())) || Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()))){
                    tripItineraryWaypointShipmentMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
                    return tripItineraryWaypointShipmentMap;
                }
                waypointModelList = streamingPublishService.fetchAndSortWaypoints((ItineraryDTO) tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()));

                //shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(futureLatestShipmentByUUID);
                shipmentDetails = futureLatestShipmentByUUID;
                if(streamingPublishService.validateObjectForClosure(shipmentDetails, huUUID)){
                    tripItineraryWaypointShipmentMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
                    return tripItineraryWaypointShipmentMap;
                }
                break;
        }
        tripItineraryWaypointShipmentMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.FALSE);
        tripItineraryWaypointShipmentMap.put(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint(), waypointModelList);
        tripItineraryWaypointShipmentMap.put(streamingServiceProperties.getPhuwvDomainEventTypeShipment(), shipmentDetails);
        return tripItineraryWaypointShipmentMap;
    }

    private HandlingUnit invokeGetHandlingUnitByUUID(String huUUID) {
        return phuwvServiceInvoker.getLatestHandlingUnitByUUID(huUUID);
    }

    private ShipmentDetails invokeGetShipmentByUUID(String associationUUID) {
        return phuwvServiceInvoker.getShipmentByUUID(associationUUID);
    }
}