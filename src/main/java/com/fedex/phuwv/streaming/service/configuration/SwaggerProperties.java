package com.fedex.phuwv.streaming.service.configuration;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@ConfigurationProperties(prefix = "app.swagger")
@Slf4j
@Data
public class SwaggerProperties {
	
	private String baseApiPackage;
    private boolean useDefaultResponseMessages;

    @PostConstruct
    public void initialize() {
        if (log.isInfoEnabled()) {
            log.info("Loaded properties : " + toString());
        }
    }


}
