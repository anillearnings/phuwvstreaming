package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ShipmentDetailsDtoToApiModelMapper {

    @Mappings({@Mapping(
            target = "owningOpCo",
            ignore = true
    )})
    com.fedex.phuwv.common.api.models.ShipmentDetails shipmentDetailsDtoToApiModel(ShipmentDetails shipmentDetailsDto);

    com.fedex.phuwv.common.api.models.CarrierDetail carrierDetailDtoToApiModel(CarrierDetail carrierDetail);

    List<com.fedex.phuwv.common.api.models.Customer> customerAssociationsDtoToApiModel(List<Customer> customerAssociations);

    com.fedex.phuwv.common.api.models.Address customerAddressDtoToApiModel(Address customerAddress);
}
