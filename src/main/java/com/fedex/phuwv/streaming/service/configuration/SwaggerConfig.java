package com.fedex.phuwv.streaming.service.configuration;

import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@EnableConfigurationProperties(SwaggerProperties.class)
@ConditionalOnProperty(prefix = "app.swagger", name = "enabled", havingValue = "true")
public class SwaggerConfig {
	
	private SwaggerProperties swaggerProperties;
	
	@Autowired
	public SwaggerConfig(SwaggerProperties swaggerproperties) {
		
		this.swaggerProperties=swaggerproperties;
	}
	
	@Bean
	@ConditionalOnMissingBean
	public Docket productAPI()
	{
		if(StringUtils.isNotBlank(swaggerProperties.getBaseApiPackage())) {
			return new Docket(DocumentationType.SWAGGER_2).forCodeGeneration(true)
					.useDefaultResponseMessages(swaggerProperties.isUseDefaultResponseMessages())
					.host("https://geois-swaggerhub.prod.fedex.com/apis/Express_Ship_Sys/PHUWV_getHU/2.0.1")
					.select()
					.apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBaseApiPackage()))
					.paths(PathSelectors.ant("/api/v1/phuwv/*")).build();
		}
		else {
			return new Docket(DocumentationType.SWAGGER_2).forCodeGeneration(true)
					.useDefaultResponseMessages(swaggerProperties.isUseDefaultResponseMessages())
					.select()
					.apis(RequestHandlerSelectors.any())
					.paths(PathSelectors.any()).build();
		}
	}

}
