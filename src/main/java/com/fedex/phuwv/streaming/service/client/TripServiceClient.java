package com.fedex.phuwv.streaming.service.client;

import com.fedex.phuwv.common.dto.TripDTO;
import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PHUWV-TRIP-SERVICE" , url = "${app.rest-client.end-points.TRIP.base-uri}",configuration = FeignClientConfig.class)
public interface TripServiceClient {

	@GetMapping(value = "${app.rest-client.end-points.GET_TRIP_BY_HANDLING_UNIT_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TripDTO> getLatestTripByHandlingUnitUUID(@PathVariable("handlingUnitUUID") String handlingUnitUUID);

	@GetMapping(value = "${app.rest-client.end-points.GET_TRIP_BY_TRIP_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<TripDTO>getLatestHandlingUnitUUIDByTripUUID(@PathVariable("tripUUID") String tripUUID);



}
