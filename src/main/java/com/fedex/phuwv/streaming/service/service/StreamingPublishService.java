package com.fedex.phuwv.streaming.service.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.exception.ApplicationException;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.common.exception.RetryableException;
import com.fedex.phuwv.common.exception.classifier.ExceptionClassifier;
import com.fedex.phuwv.common.jms.JMSPublisher;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.streaming.service.mapper.HandlingUnitDtoToApiModelMapper;
import com.fedex.phuwv.streaming.service.mapper.ShipmentDetailsDtoToApiModelMapper;
import com.fedex.phuwv.streaming.service.mapper.TaskDtoToApiModelMapper;
import com.fedex.phuwv.streaming.service.mapper.WaypointDtoToApiModelMapper;
import com.fedex.phuwv.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.RetryContext;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service("StreamingPublishService")
@Slf4j
public class StreamingPublishService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JMSPublisher domainEventPublisher;

    @Autowired
    private StreamingServiceProperties streamingServiceProperties;

    @Autowired
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Autowired
    private LocationTranslationInvoker locationTranslationInvoker;

    private static final HandlingUnitDtoToApiModelMapper handlingUnitDtoToApiModelMapper = Mappers.getMapper(HandlingUnitDtoToApiModelMapper.class);

    private static final ShipmentDetailsDtoToApiModelMapper shipmentDetailsDtoToApiModelMapper = Mappers.getMapper(ShipmentDetailsDtoToApiModelMapper.class);

    private static final WaypointDtoToApiModelMapper waypointDtoToApiModelMapper = Mappers.getMapper(WaypointDtoToApiModelMapper.class);

    private static final TaskDtoToApiModelMapper taskDtoToApiModelMapper = Mappers.getMapper(TaskDtoToApiModelMapper.class);

    private ExceptionClassifier<RetryableException> exClassifier;

    private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

    private static final String COMPLETED_EXCEPTIONALLY = "Completed exceptionally:";
    

    public void publishPhuwvEvent(com.fedex.phuwv.common.api.models.HandlingUnit handlingUnit, String coreType) throws IOException {
        log.info("Publishing the phuwv api json msg with hu uuid:" + handlingUnit.getHuUUID());
        Audit.getInstance().put(AuditKey.IN, "Publishing the phuwv api json msg with hu uuid:" + handlingUnit.getHuUUID());
        String pubDomEvent = JsonUtil.asCompactJson(objectMapper, handlingUnit);
        domainEventPublisher.publish(pubDomEvent, message -> postMessageProcess(handlingUnit, message, coreType));
        log.info("Published the phuwv api json msg:" + pubDomEvent);
        Audit.getInstance().put(AuditKey.OUT, "Published the phuwv api json msg:" + pubDomEvent);
    }

    public void mergeToPublish(HandlingUnit handlingUnitModel, ShipmentDetails shipmentDetails, List<com.fedex.phuwv.common.dto.Waypoint> waypointModelList,List<Task> tasks, String coreType) {
        // Publish PHUWV event with the contents being the pristine PHUWV HandlingUnit object.
        if (handlingUnitModel != null) {
            try {
                publishPhuwvEvent(buildPhuwvAPI(handlingUnitModel, shipmentDetails, waypointModelList, tasks), coreType);
            } catch (Exception e) {
                log.error("Exception occurred when populating PHUWV API:", e);
            }
        }
    }

    public com.fedex.phuwv.common.api.models.HandlingUnit buildPhuwvAPI(HandlingUnit handlingUnitModel, ShipmentDetails shipmentDetails, List<com.fedex.phuwv.common.dto.Waypoint> waypointModelList,List<Task> tasks) {
        com.fedex.phuwv.common.api.models.HandlingUnit handlingUnitApiModelsFinal = handlingUnitDtoToApiModelMapper.huDtoToApiModel(handlingUnitModel);
        handlingUnitApiModelsFinal.setPhuwvReleaseVersion(streamingServiceProperties.getPhuwvReleaseVersion());
        settingShipmentDetails(handlingUnitModel, shipmentDetails, handlingUnitApiModelsFinal);
        settingWaypoints(waypointModelList, handlingUnitApiModelsFinal);
        if(Objects.nonNull(tasks)){
            handlingUnitApiModelsFinal.setTasks(taskDtoToApiModelMapper.taskDtoToApiModel(tasks));
        }
        return handlingUnitApiModelsFinal;
    }

    public Message postMessageProcess(com.fedex.phuwv.common.api.models.HandlingUnit handlingUnit, Message message, String coreType) throws JMSException{
        message.setJMSCorrelationID(UUID.randomUUID().toString());
        message.setStringProperty("SOURCE_TYPE",streamingServiceProperties.getPhuwvSourceType());
        message.setStringProperty("FORM_CD", (null != handlingUnit.getFormID())?handlingUnit.getFormID():null);
        message.setStringProperty("ORIGIN_COUNTRY_CODE", handlingUnit.getOriginCountryCode());
        message.setStringProperty("DEST_COUNTRY_CODE",handlingUnit.getDestinationCountryCode());

        List<Waypoint> waypoints=handlingUnit.getWaypoints().stream().filter(wayPoints->wayPoints.getSequenceNumber()!=null && wayPoints.getFacilityCode()!=null).collect(Collectors.toList());
        waypoints.sort(Comparator.comparing(Waypoint::getSequenceNumber));
        List<String> facilityCodes=waypoints.stream().map(Waypoint::getFacilityCode).collect(Collectors.toList());
        message.setStringProperty("WAYPOINTS",facilityCodes.stream().map(Object::toString).collect(Collectors.joining(",")));

        List<Waypoint> waypoints_countryCodes=handlingUnit.getWaypoints()!=null?handlingUnit.getWaypoints().stream().filter(waypoint->waypoint.getFacilityCountryCode()!=null).collect(Collectors.toList()):null;
        List<String> countryCodes=waypoints_countryCodes!=null?waypoints_countryCodes.stream().map(Waypoint::getFacilityCountryCode).collect(Collectors.toList()):null;
        message.setStringProperty("WAYPOINT_COUNTRY_CD", countryCodes!=null?String.join(",", countryCodes.stream().distinct().collect(Collectors.toList())):null);

        message.setStringProperty("MESSAGE_VERSION", streamingServiceProperties.getPhuwvReleaseVersion());
        message.setStringProperty("DOMAIN_EVENT_TYPE", streamingServiceProperties.getPhuwvDomainEventType());
        message.setStringProperty("MessageType", com.fedex.phuwv.common.api.models.HandlingUnit.class.getName());
        message.setStringProperty("HANDLINGUNITUUID", handlingUnit.getHuUUID());
        message.setStringProperty("SHIPMENTUUID", (null != handlingUnit.getShipment())?handlingUnit.getShipment().getShipmentUUID():null);
        message.setStringProperty("DATA_DOMAIN_SOURCE", coreType);
        return message;
    }

    private void settingShipmentDetails(HandlingUnit handlingUnitModel, ShipmentDetails shipmentDetails, com.fedex.phuwv.common.api.models.HandlingUnit handlingUnitApiModelsFinal) {
        if(Objects.nonNull(shipmentDetails)){
            com.fedex.phuwv.common.api.models.ShipmentDetails shipmentDetailsApiModel = shipmentDetailsDtoToApiModelMapper.shipmentDetailsDtoToApiModel(shipmentDetails);
            shipmentDetailsApiModel.setOwningOpCo(handlingUnitModel.getOwningOpCo());
            handlingUnitApiModelsFinal.setShipment(shipmentDetailsApiModel);
            handlingUnitApiModelsFinal.setLegacyServiceCode(shipmentDetails.getLegacyServiceCode());
            handlingUnitApiModelsFinal.setZipCode(shipmentDetails.getZipCode());
        }
    }

    private void settingWaypoints(List<com.fedex.phuwv.common.dto.Waypoint> waypointModelList, com.fedex.phuwv.common.api.models.HandlingUnit handlingUnitApiModelsFinal) {
        List<com.fedex.phuwv.common.api.models.Waypoint> waypointApiModelList = new ArrayList<>();
        AtomicReference<String> waypointModelDestinationCountryCode = new AtomicReference<>();
        AtomicReference<String> waypointModelUrsaSuffix = new AtomicReference<>();
        AtomicReference<String> waypointModelOriginCountryCode = new AtomicReference<>();
        if(Objects.nonNull(waypointModelList)){
            waypointModelList.forEach(waypointModel -> {
                waypointApiModelList.add(waypointDtoToApiModelMapper.waypointDtoToApiModel(waypointModel));
                if(null != waypointModel.getType()){
                    switch (waypointModel.getType()){
                        case DESTINATION:
                            waypointModelDestinationCountryCode.set(waypointModel.getDestinationCountryCode());
                            break;
                        case DESTINATION_FACILITY:
                            waypointModelUrsaSuffix.set(waypointModel.getUrsaSuffix());
                            break;
                        case ORIGIN:
                            waypointModelOriginCountryCode.set(waypointModel.getOriginCountryCode());
                            break;
                        default:
                            break;
                    }
                }
            });
        }
        handlingUnitApiModelsFinal.setWaypoints(waypointApiModelList);
        handlingUnitApiModelsFinal.setDestinationCountryCode(waypointModelDestinationCountryCode.get());
        handlingUnitApiModelsFinal.setUrsaSuffix(waypointModelUrsaSuffix.get());
        handlingUnitApiModelsFinal.setOriginCountryCode(waypointModelOriginCountryCode.get());
    }

    public void setRetryContext(RetryContext retryContext) {
        RetryableException retryableException;
        try {
            retryableException = exClassifier.classify(retryContext.getLastThrowable());
        } catch (Throwable e) {
            log.error("Failed to recover", e);
            throw new ApplicationException("Failed to recover", e);
        }
        if (retryableException != null) {
            log.error("Failed to process trigger event: " + retryableException);
            throw new ResourceException("Failed to process trigger event: " + retryableException);
        }
    }

    public CompletableFuture<TripDTO> invokeGetFutureTripByHandlingUnitUUID(String huUUID) {
        return phuwvServiceInvoker
                .getFutureTripByHandlingUnitUUID(huUUID).exceptionally(ex -> {
                    log.warn(COMPLETED_EXCEPTIONALLY, ex);
                    return null;
                });
    }

    public CompletableFuture<List<Task>> invokeGetFutureTaskByHandlingUnitUUID(String huUUID) {
        return phuwvServiceInvoker
                .getFutureTaskByHandlingUnitUUID(huUUID).exceptionally(ex -> {
                    log.warn(COMPLETED_EXCEPTIONALLY, ex);
                    return null;
                });
    }

    public CompletableFuture<ShipmentDetails> invokeGetFutureLatestShipmentByUUID(String shipmentUUID) {
        return phuwvServiceInvoker
                .getFutureLatestShipmentByUUID(shipmentUUID)
                .exceptionally(ex -> {
                    log.error("Completed exceptionally: ", ex);
                    return null;
                });
    }

    public CompletableFuture<TripDTO> invokeGetFutureLatestTripByHandlingUnitUUID(String huUUID) {
        return phuwvServiceInvoker
                .getFutureLatestTripByHandlingUnitUUID(huUUID).exceptionally(ex -> {
                    log.error(COMPLETED_EXCEPTIONALLY, ex);
                    return null;
                });
    }

    public List<com.fedex.phuwv.common.dto.Waypoint> fetchAndSortWaypoints(ItineraryDTO itineraryDTO) {
        List<com.fedex.phuwv.common.dto.Waypoint> waypointModelList = new ArrayList<>();
        if (itineraryDTO != null) {
            waypointModelList = phuwvServiceInvoker.getLatestWaypointsByUUIDs(itineraryDTO.getItems().stream().map(ItineraryItems::getWaypointUUID).collect(Collectors.toList()));
            waypointModelList.sort(Comparator.comparing(com.fedex.phuwv.common.dto.Waypoint::getSequenceNumber));
        }
        return waypointModelList;
    }

    public List<com.fedex.phuwv.common.dto.Waypoint> fetchItineraryAndGetWaypoints(String tripUUID) {
        return fetchAndSortWaypoints(phuwvServiceInvoker.getLatestItineraryByTripUUID(tripUUID));
    }

    public List<com.fedex.phuwv.common.dto.Waypoint> fetchLSSIWaypoints(ItineraryDTO itineraryResponse) {
        return locationTranslationInvoker.convertOSCWaypointsToLSSI(fetchAndSortWaypoints(itineraryResponse));
    }

    public HandlingUnit settingHandlingUnit(CompletableFuture<HandlingUnit> futureLatestHandlingUnitByUUID) {
        HandlingUnit handlingUnit=null;
        try{
            if (futureLatestHandlingUnitByUUID.isDone()) {
                handlingUnit=futureLatestHandlingUnitByUUID.get();
            }
        }catch (Exception e){
            log.error("Exception when extracting handling unit out of future response:",e);
        }
        return handlingUnit;
    }

    public ShipmentDetails settingShipmentDetailsForHandler(CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID) {
        ShipmentDetails shipmentDetails=null;
        try{
            if (futureLatestShipmentByUUID.isDone()) {
                shipmentDetails=futureLatestShipmentByUUID.get();
            }
        }catch (Exception e){
            log.error("Exception when extracting shipment details out of future response",e);
        }
        return shipmentDetails;
    }

    public Map<String,Object> settingTripItineraryWaypointInfo(CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID, String huUUID) throws ExecutionException, InterruptedException {
        Map<String,Object> tripAndItineraryMap = new HashMap<>();
        TripDTO tripDTO = new TripDTO();
        ItineraryDTO itineraryDTO = new ItineraryDTO();
        if (futureLatestTripByHandlingUnitUUID.isDone()) {
            // Use the TRIP_UUID to call ItineraryService lookup by TRIP_UUID. We can just
            // return our own ItineraryDTO object with a list containing Sequence Number and
            // corresponding WaypointUUID.
            tripDTO = futureLatestTripByHandlingUnitUUID.get();
            if (null == tripDTO) {
                log.warn("No response from trip service for hu uuid:" + huUUID
                        + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
                tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), null);
                tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryDTO);
                return tripAndItineraryMap;
            }

            // Call Waypoint Service for every WAYPOINT_UUID. We can then pull together all
            // associated waypoint information associated with the ITINERARY to populate our
            // PHUWV API.
            itineraryDTO = phuwvServiceInvoker.getLatestItineraryByTripUUID(tripDTO.getTripUUID());
            if(Objects.isNull(itineraryDTO)) {
                log.warn("No response from itinerary service for hu uuid:" + huUUID
                        + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
                tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), tripDTO);
                tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), null);
                return tripAndItineraryMap;
            }
        }
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), tripDTO);
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryDTO);
        return tripAndItineraryMap;
    }

    public Map<String,Object> settingItineraryForSepTrigger(CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID, String huUUID) throws ExecutionException, InterruptedException {
        Map<String,Object> tripAndItineraryMap = new HashMap<>();
        TripDTO tripDTO = new TripDTO();
        ItineraryDTO itineraryServiceResponse = new ItineraryDTO();
        if (futureLatestTripByHandlingUnitUUID.isDone()) {
            // Use the TRIP_UUID to call ItineraryService lookup by TRIP_UUID. We can just
            // return our own ItineraryDTO object with a list containing Sequence Number and
            // corresponding WaypointUUID.
            if (futureLatestTripByHandlingUnitUUID.get() == null) {
                log.warn("No response from trip service for hu uuid:" + huUUID
                        + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
                tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), null);
                tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryServiceResponse);
                return tripAndItineraryMap;
            }

            itineraryServiceResponse = phuwvServiceInvoker
                    .getLatestItineraryByTripUUID(futureLatestTripByHandlingUnitUUID.get().getTripUUID());
        }
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), tripDTO);
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryServiceResponse);
        return tripAndItineraryMap;
    }

    public List<Task> settingTaskDetails(CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID, String huUUID) {
        List<Task> taskDetails=null;
        try{
            if (futureTasksByHandlingUnitUUID.isDone()) {
                taskDetails=futureTasksByHandlingUnitUUID.get();
            }
        }catch (Exception e){
            log.error("No response from Task service for hu uuid:" + huUUID
                    + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT + " due to:" + e);
        }
        return taskDetails;
    }

    public boolean validateObjectForClosure(Object object, String uuid) {
        if (Objects.isNull(object)) {
            log.warn(WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT + " after invoking this trigger for uuid:" + uuid);
            return true;
        }
        return false;
    }

    public boolean validateHandlingUnitForClosure(HandlingUnit handlingUnit, String uuid) {
        return validateObjectForClosure(handlingUnit, uuid);
    }

    public boolean validateTaskDetailsForClosure(List<Task> tasks, String uuid) {
        return validateObjectForClosure(tasks, uuid);
    }

    public boolean validatePublishingPhuwvJms(String uuidTrackingNumber) {
        if(streamingServiceProperties.isPhuwvPublishJms()){
            return false;
        }
        log.info(WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT + " as publishing is disabled for uuid/tracking number:" + uuidTrackingNumber + " from this trigger event");
        return true;
    }

    public TripDTO invokeGetTripByHandlingUnitUUID(String huUUID) {
        return phuwvServiceInvoker.getTripByHandlingUnitUUID(huUUID);
    }

    public Map<String,Object> settingTripItineraryWaypointInfoSync(TripDTO futureLatestTripByHandlingUnitUUID, String huUUID) {
        Map<String,Object> tripAndItineraryMap = new HashMap<>();
        TripDTO tripDTO = new TripDTO();
        ItineraryDTO itineraryDTO = new ItineraryDTO();

        // Use the TRIP_UUID to call ItineraryService lookup by TRIP_UUID. We can just
        // return our own ItineraryDTO object with a list containing Sequence Number and
        // corresponding WaypointUUID.
        tripDTO = futureLatestTripByHandlingUnitUUID;
        if (null == tripDTO) {
            log.warn("No response from trip service for hu uuid:" + huUUID
                    + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
            tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), null);
            tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryDTO);
            return tripAndItineraryMap;
        }

        // Call Waypoint Service for every WAYPOINT_UUID. We can then pull together all
        // associated waypoint information associated with the ITINERARY to populate our
        // PHUWV API.
        itineraryDTO = phuwvServiceInvoker.getLatestItineraryByTripUUID(tripDTO.getTripUUID());
        if(Objects.isNull(itineraryDTO)) {
            log.warn("No response from itinerary service for hu uuid:" + huUUID
                    + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
            tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), tripDTO);
            tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), null);
            return tripAndItineraryMap;
        }
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), tripDTO);
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryDTO);
        return tripAndItineraryMap;
    }

    public List<Task> invokeGetTaskByHandlingUnitUUID(String huUUID) {
        return phuwvServiceInvoker.getTaskByHandlingUnitUUID(huUUID);
    }

    public ShipmentDetails invokeGetLatestShipmentByUUID(String shipmentUUID) {
        return phuwvServiceInvoker.getShipmentByUUID(shipmentUUID);
    }

    public Map<String,Object> settingItineraryForSepTriggerSync(TripDTO futureLatestTripByHandlingUnitUUID, String huUUID) throws ExecutionException, InterruptedException {
        Map<String,Object> tripAndItineraryMap = new HashMap<>();
        TripDTO tripDTO = new TripDTO();
        ItineraryDTO itineraryServiceResponse = new ItineraryDTO();

        // Use the TRIP_UUID to call ItineraryService lookup by TRIP_UUID. We can just
        // return our own ItineraryDTO object with a list containing Sequence Number and
        // corresponding WaypointUUID.
        if (futureLatestTripByHandlingUnitUUID == null) {
            log.warn("No response from trip service for hu uuid:" + huUUID
                    + "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
            tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), null);
            tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryServiceResponse);
            return tripAndItineraryMap;
        }

        itineraryServiceResponse = phuwvServiceInvoker
                .getLatestItineraryByTripUUID(futureLatestTripByHandlingUnitUUID.getTripUUID());

        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeTrip(), tripDTO);
        tripAndItineraryMap.put(streamingServiceProperties.getPhuwvDomainEventTypeItinerary(), itineraryServiceResponse);
        return tripAndItineraryMap;
    }
}
