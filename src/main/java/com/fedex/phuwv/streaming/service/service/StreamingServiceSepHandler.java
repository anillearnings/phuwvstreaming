package com.fedex.phuwv.streaming.service.service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.streaming.service.mapper.*;
import org.apache.commons.lang.StringUtils;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import com.fedex.phuwv.common.dto.sep.xmlparser.dto.EnhancedEvent;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.common.jms.MessageHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("StreamingServiceSepHandler")
public class StreamingServiceSepHandler implements MessageHandler<EnhancedEvent> {

	private RetryTemplate sepTriggerRetryTemplate;

	@Autowired
	private PhuwvServiceInvoker phuwvServiceInvoker;
	
	@Autowired
    private LocationTranslationInvoker locationTranslationInvoker;

	@Autowired
	private StreamingPublishService streamingPublishService;

	private SepDtoToApiModelMapper sepDtoToApiModelMapper = Mappers.getMapper(SepDtoToApiModelMapper.class);

	@Autowired
	private StreamingServiceProperties streamingServiceProperties;
	

	private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

	private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

	@Autowired
	public StreamingServiceSepHandler(@Qualifier("jmsRetryTemplate") RetryTemplate sepTriggerRetryTemplate) {
		this.sepTriggerRetryTemplate = sepTriggerRetryTemplate;
	}

	
	@SuppressWarnings("unused")
	@Override
	public void handle(GenericMessage<EnhancedEvent> genericMessage) throws ResourceException, Exception {

		sepTriggerRetryTemplate.execute(retryContext -> {
			HandlingUnit handlingUnitModel=null;
			ShipmentDetails shipmentDetails = null;
			ItineraryDTO itineraryServiceResponse = null;
			try {
				EnhancedEvent message = genericMessage.getPayload();

				String trackingNumber = message.getMasterList().getTrackitemnumber();
				log.info("SEP Domain Event with tracking number: " + trackingNumber);
				Audit.getInstance().put(AuditKey.IN, "SEP Domain Event with tracking number: " + trackingNumber);

				if (StringUtils.isNotEmpty(trackingNumber)) {
					log.info("TrackingNbr from sep internal trigger : {}", trackingNumber);
					if(streamingPublishService.validatePublishingPhuwvJms(trackingNumber)){
						return null;
					}
					handlingUnitModel = invokeSepByTrackingNumber(trackingNumber);

					if (Objects.isNull(handlingUnitModel)) {
						log.warn("No response from Handling unit service for the Tracking Number:" + trackingNumber + ",Calling SEP Service");

						// Publish PHUWV with SEP Data
						streamingPublishService.publishPhuwvEvent(sepDtoToApiModelMapper.sepDtoToApiModel(phuwvServiceInvoker.getFutureHandlingUnitWithSepData(trackingNumber)), streamingServiceProperties.getPhuwvSepCoreType());
						return null;
					}
					String huUUID = handlingUnitModel.getHuUUID();

					//CompletableFuture<ShipmentDetails> shipmentServiceResponse = streamingPublishService.invokeGetFutureLatestShipmentByUUID(handlingUnitModel.getShipment().getShipmentUUID());
					ShipmentDetails shipmentServiceResponse = streamingPublishService.invokeGetLatestShipmentByUUID(handlingUnitModel.getShipment().getShipmentUUID());

					// Call TripService by lookup on HandlingUnitUUID. We can return just a Data
					// Transfer Object(DTO) that has TRIP_UUID for now.
					//CompletableFuture<TripDTO> futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(huUUID);
					TripDTO futureLatestTripByHandlingUnitUUID = streamingPublishService.invokeGetTripByHandlingUnitUUID(huUUID);

					//Call TaskService
                    //CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(huUUID);
					List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(huUUID);

					// Wait until they are all done
					//CompletableFuture.allOf(shipmentServiceResponse, futureLatestTripByHandlingUnitUUID, futureTasksByHandlingUnitUUID).join();

					Map<String,Object> tripItineraryShipmentMap = computeTripItineraryShipmentDetailsForHUType(handlingUnitModel, futureLatestTripByHandlingUnitUUID, shipmentServiceResponse, huUUID);
					if((boolean) tripItineraryShipmentMap.get(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull())){
						return null;
					}
					shipmentDetails = (ShipmentDetails) tripItineraryShipmentMap.get(streamingServiceProperties.getPhuwvDomainEventTypeShipment());
					itineraryServiceResponse = (ItineraryDTO) tripItineraryShipmentMap.get(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint());

					//List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, huUUID);
					List<Task> taskDetails = futureTasksByHandlingUnitUUID;

					// Calling buisnessLogic Method for Merging response Data
					buisnessLogicToPublishPhuwv(handlingUnitModel, shipmentDetails, itineraryServiceResponse,
							trackingNumber,taskDetails);
					log.info("End of Sep trigger for tracking Number: " + trackingNumber);
				}
			} catch (Exception e) {
				log.error("Exception:", e);
			}
			return null;
		}, retryContext -> {
			streamingPublishService.setRetryContext(retryContext);
			return null;
		});
	}

	@SuppressWarnings("unused")
	private void buisnessLogicToPublishPhuwv(HandlingUnit huModel, ShipmentDetails shipmentDetails,
			ItineraryDTO itineraryResponse, String trackingNbr,List<Task> tasks) throws IOException {
		log.info("***Merging response data and creating New HandlingUnit Model***");
		if (huModel != null) {
			log.info("All Responses Received from Core,Publishing Handling Unit for tracking Nbr: "+trackingNbr);
			//Publish HandlingUnitMOdel with SEFS Data
			streamingPublishService.publishPhuwvEvent(streamingPublishService.buildPhuwvAPI(huModel, shipmentDetails, streamingPublishService.fetchLSSIWaypoints(itineraryResponse), tasks), streamingServiceProperties.getPhuwvSefsCoreType());
		}
	}

	private HandlingUnit invokeSepByTrackingNumber(String trackingNumber) {
		try{
			return phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(trackingNumber);
		}catch (Exception e){
			log.error("Error occurred in Handling unit service for the Tracking Number:{} which is: {}", trackingNumber, e);
		}
		return null;
	}

	private Map<String,Object> computeTripItineraryShipmentDetailsForHUType(HandlingUnit handlingUnitModel, TripDTO futureLatestTripByHandlingUnitUUID,
																			ShipmentDetails shipmentServiceResponse, String huUUID) throws ExecutionException, InterruptedException {
		//Map<String,Object> tripAndItineraryMap = streamingPublishService.settingItineraryForSepTrigger(futureLatestTripByHandlingUnitUUID, huUUID);
		Map<String,Object> tripAndItineraryMap = streamingPublishService.settingItineraryForSepTriggerSync(futureLatestTripByHandlingUnitUUID, huUUID);
		ItineraryDTO itineraryServiceResponse = new ItineraryDTO();
		ShipmentDetails shipmentDetails;
		Map<String,Object> tripItineraryShipmentMap = new HashMap<>();
		switch (handlingUnitModel.getType()){
			case HANDLINGUNIT_TYPE_CONSOLIDATION:
				if(!Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTrip())) && !Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()))){
					itineraryServiceResponse = (ItineraryDTO) tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary());
				}
				//shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(shipmentServiceResponse);
				shipmentDetails = shipmentServiceResponse;
				break;
			default:
				if(Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeTrip())) || Objects.isNull(tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()))){
					tripItineraryShipmentMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
					return tripItineraryShipmentMap;
				}
				itineraryServiceResponse = (ItineraryDTO) tripAndItineraryMap.get(streamingServiceProperties.getPhuwvDomainEventTypeItinerary());

				//shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(shipmentServiceResponse);
				shipmentDetails = shipmentServiceResponse;
				if(streamingPublishService.validateObjectForClosure(shipmentDetails, huUUID)){
					tripItineraryShipmentMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.TRUE);
					return tripItineraryShipmentMap;
				}
				break;
		}
		tripItineraryShipmentMap.put(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull(), Boolean.FALSE);
		tripItineraryShipmentMap.put(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint(), itineraryServiceResponse);
		tripItineraryShipmentMap.put(streamingServiceProperties.getPhuwvDomainEventTypeShipment(), shipmentDetails);
		return tripItineraryShipmentMap;
	}
}
