package com.fedex.phuwv.streaming.service.client;

import com.fedex.phuwv.common.dto.HandlingUnit;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;


@FeignClient(name = "PHUWV-SEP-SERVICE", url = "${app.rest-client.end-points.SEP.base-uri}",
configuration = FeignClientConfig.class)
public interface SepServiceClient {
	
	@GetMapping(value = "${app.rest-client.end-points.GET_HU_BY_TRACKING_NBR.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })	
	ResponseEntity<HandlingUnit> getLatestHUFromSEPByTrackingNbr(@PathVariable("trackingNBR")String trackingNbr);
	
	@DeleteMapping(value = "${app.rest-client.end-points.DELET_SEP_BY_PURGE_DATE.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Integer> purgeUntilDate(@PathVariable(value = "purgeDt") String purgeDt);

}
