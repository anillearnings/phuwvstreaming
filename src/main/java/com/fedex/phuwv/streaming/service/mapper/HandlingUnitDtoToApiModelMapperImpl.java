package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.api.models.AlternateAccountId;
import com.fedex.phuwv.common.api.models.CarrierDetail;
import com.fedex.phuwv.common.api.models.CompositeNaturalId;
import com.fedex.phuwv.common.api.models.DateTimeFrame;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.api.models.HandlingUnitId;
import com.fedex.phuwv.common.api.models.HandlingUnitPhysicalCharacteristic;
import com.fedex.phuwv.common.api.models.LinearUnits;
import com.fedex.phuwv.common.api.models.Money;
import com.fedex.phuwv.common.api.models.ShipmentCommodityDetail;
import com.fedex.phuwv.common.api.models.ShipmentDetails;
import com.fedex.phuwv.common.api.models.TaskStatus;
import com.fedex.phuwv.common.api.models.Volume;
import com.fedex.phuwv.common.api.models.VolumeUnits;
import com.fedex.phuwv.common.api.models.Waypoint;
import com.fedex.phuwv.common.api.models.Weight;
import com.fedex.phuwv.common.api.models.WeightUnits;
import com.fedex.phuwv.common.api.models.WorkRequirementAssociation;
import com.fedex.phuwv.common.dto.Address;
import com.fedex.phuwv.common.dto.Customer;
import com.fedex.phuwv.common.dto.Dimensions;
import com.fedex.phuwv.common.dto.DomainStatus;
import com.fedex.phuwv.common.dto.Parameter;
import com.fedex.phuwv.common.dto.Task;
import com.fedex.phuwv.common.dto.TaskFacilityDetail;
import com.fedex.phuwv.common.dto.WorkAttribute;
import com.fedex.phuwv.common.dto.WorkRequirement;
import com.fedex.phuwv.common.dto.WorkRequirementStatus;
import com.fedex.phuwv.common.dto.WorkSource;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-10-12T17:08:48+0530",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-6.8.2.jar, environment: Java 1.8.0_281 (Oracle Corporation)"
)
@Component
public class HandlingUnitDtoToApiModelMapperImpl implements HandlingUnitDtoToApiModelMapper {

    @Override
    public HandlingUnit huDtoToApiModel(com.fedex.phuwv.common.dto.HandlingUnit huDto) {
        if ( huDto == null ) {
            return null;
        }

        HandlingUnit handlingUnit = new HandlingUnit();

        beforeMapping( handlingUnit, huDto );

        handlingUnit.setPhuwvReleaseVersion( huDto.getPhuwvReleaseVersion() );
        handlingUnit.setHuUUID( huDto.getHuUUID() );
        handlingUnit.setCreateDateTime( huDto.getCreateDateTime() );
        handlingUnit.setIntendedShipDate( huDto.getIntendedShipDate() );
        handlingUnit.setCommitDate( huDto.getCommitDate() );
        handlingUnit.setZipCode( huDto.getZipCode() );
        handlingUnit.setType( huDto.getType() );
        handlingUnit.setShipmentUnitType( huDto.getShipmentUnitType() );
        handlingUnit.setConsolidationCategory( huDto.getConsolidationCategory() );
        handlingUnit.setUrsaSuffix( huDto.getUrsaSuffix() );
        handlingUnit.setFormID( huDto.getFormID() );
        handlingUnit.setOriginCountryCode( huDto.getOriginCountryCode() );
        handlingUnit.setDestinationCountryCode( huDto.getDestinationCountryCode() );
        handlingUnit.setLegacyServiceCode( huDto.getLegacyServiceCode() );
        handlingUnit.setLegacyPackageCode( huDto.getLegacyPackageCode() );
        List<String> list = huDto.getSpecialHandlingCodes();
        if ( list != null ) {
            handlingUnit.setSpecialHandlingCodes( new ArrayList<String>( list ) );
        }
        handlingUnit.setDgManifestId( huDto.getDgManifestId() );
        handlingUnit.setTypeOfHandlingUnit( huDto.getTypeOfHandlingUnit() );
        handlingUnit.setHandlingUnitIds( handlingUnitIdsDtoToApiModel( huDto.getHandlingUnitIds() ) );
        handlingUnit.setNaturalIds( naturalIdsDtoToApiModel( huDto.getNaturalIds() ) );
        handlingUnit.setShipment( shipmentDetailsToShipmentDetails( huDto.getShipment() ) );
        handlingUnit.setPhysicalCharacteristic( handlingUnitPhysicalCharacteristicDtoToApiModel( huDto.getPhysicalCharacteristic() ) );
        handlingUnit.setWaypoints( waypointListToWaypointList( huDto.getWaypoints() ) );
        handlingUnit.setWorkRequirements( workRequirementsDtoToApiModel( huDto.getWorkRequirements() ) );
        handlingUnit.setTasks( tasksDtoToApiModel( huDto.getTasks() ) );
        handlingUnit.setCommodity( shipmentCommodityDetailsDtoToApiModel( huDto.getCommodity() ) );

        return handlingUnit;
    }

    @Override
    public List<HandlingUnitId> handlingUnitIdsDtoToApiModel(List<com.fedex.phuwv.common.dto.HandlingUnitId> handlingUnitIds) {
        if ( handlingUnitIds == null ) {
            return null;
        }

        List<HandlingUnitId> list = new ArrayList<HandlingUnitId>( handlingUnitIds.size() );
        for ( com.fedex.phuwv.common.dto.HandlingUnitId handlingUnitId : handlingUnitIds ) {
            list.add( handlingUnitIdToHandlingUnitId( handlingUnitId ) );
        }

        return list;
    }

    @Override
    public List<CompositeNaturalId> naturalIdsDtoToApiModel(List<com.fedex.phuwv.common.dto.CompositeNaturalId> naturalIds) {
        if ( naturalIds == null ) {
            return null;
        }

        List<CompositeNaturalId> list = new ArrayList<CompositeNaturalId>( naturalIds.size() );
        for ( com.fedex.phuwv.common.dto.CompositeNaturalId compositeNaturalId : naturalIds ) {
            list.add( compositeNaturalIdToCompositeNaturalId( compositeNaturalId ) );
        }

        return list;
    }

    @Override
    public List<com.fedex.phuwv.common.api.models.Parameter> parameterListDtoToApiModel(List<Parameter> parameterList) {
        if ( parameterList == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.Parameter> list = new ArrayList<com.fedex.phuwv.common.api.models.Parameter>( parameterList.size() );
        for ( Parameter parameter : parameterList ) {
            list.add( parameterToParameter( parameter ) );
        }

        return list;
    }

    @Override
    public HandlingUnitPhysicalCharacteristic handlingUnitPhysicalCharacteristicDtoToApiModel(com.fedex.phuwv.common.dto.HandlingUnitPhysicalCharacteristic handlingUnitPhysicalCharacteristic) {
        if ( handlingUnitPhysicalCharacteristic == null ) {
            return null;
        }

        HandlingUnitPhysicalCharacteristic handlingUnitPhysicalCharacteristic1 = new HandlingUnitPhysicalCharacteristic();

        handlingUnitPhysicalCharacteristic1.setWeight( weightDtoToApiModel( handlingUnitPhysicalCharacteristic.getWeight() ) );
        handlingUnitPhysicalCharacteristic1.setDimensions( dimensionsDtoToApiModel( handlingUnitPhysicalCharacteristic.getDimensions() ) );
        handlingUnitPhysicalCharacteristic1.setVolume( volumeDtoToApiModel( handlingUnitPhysicalCharacteristic.getVolume() ) );

        return handlingUnitPhysicalCharacteristic1;
    }

    @Override
    public Weight weightDtoToApiModel(com.fedex.phuwv.common.dto.Weight weight) {
        if ( weight == null ) {
            return null;
        }

        Weight weight1 = new Weight();

        weight1.setUnits( weightUnitsDtoToApiModel( weight.getUnits() ) );
        weight1.setValue( weight.getValue() );

        return weight1;
    }

    @Override
    public WeightUnits weightUnitsDtoToApiModel(com.fedex.phuwv.common.dto.WeightUnits weightUnits) {
        if ( weightUnits == null ) {
            return null;
        }

        WeightUnits weightUnits1;

        switch ( weightUnits ) {
            case KG: weightUnits1 = WeightUnits.KG;
            break;
            case LB: weightUnits1 = WeightUnits.LB;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + weightUnits );
        }

        return weightUnits1;
    }

    @Override
    public com.fedex.phuwv.common.api.models.Dimensions dimensionsDtoToApiModel(Dimensions dimensions) {
        if ( dimensions == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Dimensions dimensions1 = new com.fedex.phuwv.common.api.models.Dimensions();

        dimensions1.setLength( dimensions.getLength() );
        dimensions1.setWidth( dimensions.getWidth() );
        dimensions1.setHeight( dimensions.getHeight() );
        dimensions1.setUnits( linearUnitsDtoToApiModel( dimensions.getUnits() ) );

        return dimensions1;
    }

    @Override
    public LinearUnits linearUnitsDtoToApiModel(com.fedex.phuwv.common.dto.LinearUnits linearUnits) {
        if ( linearUnits == null ) {
            return null;
        }

        LinearUnits linearUnits1;

        switch ( linearUnits ) {
            case CM: linearUnits1 = LinearUnits.CM;
            break;
            case FT: linearUnits1 = LinearUnits.FT;
            break;
            case IN: linearUnits1 = LinearUnits.IN;
            break;
            case M: linearUnits1 = LinearUnits.M;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + linearUnits );
        }

        return linearUnits1;
    }

    @Override
    public Volume volumeDtoToApiModel(com.fedex.phuwv.common.dto.Volume volume) {
        if ( volume == null ) {
            return null;
        }

        Volume volume1 = new Volume();

        volume1.setUnits( volumeUnitsDtoToApiModel( volume.getUnits() ) );
        volume1.setValue( volume.getValue() );

        return volume1;
    }

    @Override
    public VolumeUnits volumeUnitsDtoToApiModel(com.fedex.phuwv.common.dto.VolumeUnits volumeUnits) {
        if ( volumeUnits == null ) {
            return null;
        }

        VolumeUnits volumeUnits1;

        switch ( volumeUnits ) {
            case CM: volumeUnits1 = VolumeUnits.CM;
            break;
            case FT: volumeUnits1 = VolumeUnits.FT;
            break;
            case IN: volumeUnits1 = VolumeUnits.IN;
            break;
            case M: volumeUnits1 = VolumeUnits.M;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + volumeUnits );
        }

        return volumeUnits1;
    }

    @Override
    public List<com.fedex.phuwv.common.api.models.WorkRequirement> workRequirementsDtoToApiModel(List<WorkRequirement> workRequirements) {
        if ( workRequirements == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.WorkRequirement> list = new ArrayList<com.fedex.phuwv.common.api.models.WorkRequirement>( workRequirements.size() );
        for ( WorkRequirement workRequirement : workRequirements ) {
            list.add( workRequirementToWorkRequirement( workRequirement ) );
        }

        return list;
    }

    @Override
    public List<com.fedex.phuwv.common.api.models.WorkAttribute> attributesDtoToApiModel(List<WorkAttribute> attributes) {
        if ( attributes == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.WorkAttribute> list = new ArrayList<com.fedex.phuwv.common.api.models.WorkAttribute>( attributes.size() );
        for ( WorkAttribute workAttribute : attributes ) {
            list.add( workAttributeToWorkAttribute( workAttribute ) );
        }

        return list;
    }

    @Override
    public com.fedex.phuwv.common.api.models.WorkSource workSourceDtoToApiModel(WorkSource workSource) {
        if ( workSource == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.WorkSource workSource1 = new com.fedex.phuwv.common.api.models.WorkSource();

        workSource1.setAppId( workSource.getAppId() );

        return workSource1;
    }

    @Override
    public List<com.fedex.phuwv.common.api.models.WorkRequirementStatus> workRequirementStatusesDtoToApiModel(List<WorkRequirementStatus> workRequirementStatuses) {
        if ( workRequirementStatuses == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.WorkRequirementStatus> list = new ArrayList<com.fedex.phuwv.common.api.models.WorkRequirementStatus>( workRequirementStatuses.size() );
        for ( WorkRequirementStatus workRequirementStatus : workRequirementStatuses ) {
            list.add( workRequirementStatusToWorkRequirementStatus( workRequirementStatus ) );
        }

        return list;
    }

    @Override
    public List<WorkRequirementAssociation> workRequirementAssociationsDtoToApiModel(List<com.fedex.phuwv.common.dto.WorkRequirementAssociation> workRequirementAssociations) {
        if ( workRequirementAssociations == null ) {
            return null;
        }

        List<WorkRequirementAssociation> list = new ArrayList<WorkRequirementAssociation>( workRequirementAssociations.size() );
        for ( com.fedex.phuwv.common.dto.WorkRequirementAssociation workRequirementAssociation : workRequirementAssociations ) {
            list.add( workRequirementAssociationToWorkRequirementAssociation( workRequirementAssociation ) );
        }

        return list;
    }

    @Override
    public List<com.fedex.phuwv.common.api.models.Task> tasksDtoToApiModel(List<Task> tasks) {
        if ( tasks == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.Task> list = new ArrayList<com.fedex.phuwv.common.api.models.Task>( tasks.size() );
        for ( Task task : tasks ) {
            list.add( taskToTask( task ) );
        }

        return list;
    }

    @Override
    public List<TaskStatus> taskStatusesDtoToApiModel(List<com.fedex.phuwv.common.dto.TaskStatus> taskStatuses) {
        if ( taskStatuses == null ) {
            return null;
        }

        List<TaskStatus> list = new ArrayList<TaskStatus>( taskStatuses.size() );
        for ( com.fedex.phuwv.common.dto.TaskStatus taskStatus : taskStatuses ) {
            list.add( taskStatusToTaskStatus( taskStatus ) );
        }

        return list;
    }

    @Override
    public com.fedex.phuwv.common.api.models.TaskFacilityDetail taskFacilitysDtoToApiModel(TaskFacilityDetail taskFacilitys) {
        if ( taskFacilitys == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.TaskFacilityDetail taskFacilityDetail = new com.fedex.phuwv.common.api.models.TaskFacilityDetail();

        taskFacilityDetail.setLegacyLocationCode( taskFacilitys.getLegacyLocationCode() );
        taskFacilityDetail.setOwner( taskFacilitys.getOwner() );
        taskFacilityDetail.setCode( taskFacilitys.getCode() );

        return taskFacilityDetail;
    }

    @Override
    public List<ShipmentCommodityDetail> shipmentCommodityDetailsDtoToApiModel(List<com.fedex.phuwv.common.dto.ShipmentCommodityDetail> shipmentCommodityDetails) {
        if ( shipmentCommodityDetails == null ) {
            return null;
        }

        List<ShipmentCommodityDetail> list = new ArrayList<ShipmentCommodityDetail>( shipmentCommodityDetails.size() );
        for ( com.fedex.phuwv.common.dto.ShipmentCommodityDetail shipmentCommodityDetail : shipmentCommodityDetails ) {
            list.add( shipmentCommodityDetailToShipmentCommodityDetail( shipmentCommodityDetail ) );
        }

        return list;
    }

    @Override
    public Money customsValueDtoToApiModel(com.fedex.phuwv.common.dto.Money customsValue) {
        if ( customsValue == null ) {
            return null;
        }

        Money money = new Money();

        money.setCurrencyCode( customsValue.getCurrencyCode() );
        money.setCurrencyType( customsValue.getCurrencyType() );
        money.setAmount( customsValue.getAmount() );

        return money;
    }

    protected CarrierDetail carrierDetailToCarrierDetail(com.fedex.phuwv.common.dto.CarrierDetail carrierDetail) {
        if ( carrierDetail == null ) {
            return null;
        }

        CarrierDetail carrierDetail1 = new CarrierDetail();

        carrierDetail1.setType( carrierDetail.getType() );
        carrierDetail1.setCode( carrierDetail.getCode() );

        return carrierDetail1;
    }

    protected AlternateAccountId alternateAccountIdToAlternateAccountId(com.fedex.phuwv.common.dto.AlternateAccountId alternateAccountId) {
        if ( alternateAccountId == null ) {
            return null;
        }

        AlternateAccountId alternateAccountId1 = new AlternateAccountId();

        alternateAccountId1.setOwner( alternateAccountId.getOwner() );
        alternateAccountId1.setId( alternateAccountId.getId() );
        alternateAccountId1.setCountryCode( alternateAccountId.getCountryCode() );

        return alternateAccountId1;
    }

    protected com.fedex.phuwv.common.api.models.Address addressToAddress(Address address) {
        if ( address == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Address address1 = new com.fedex.phuwv.common.api.models.Address();

        address1.setCountryCode( address.getCountryCode() );
        address1.setCity( address.getCity() );
        address1.setStateCode( address.getStateCode() );
        address1.setPostal( address.getPostal() );
        address1.setAddressLine1( address.getAddressLine1() );
        address1.setAddressLine2( address.getAddressLine2() );
        address1.setAddressLine3( address.getAddressLine3() );

        return address1;
    }

    protected com.fedex.phuwv.common.api.models.Customer customerToCustomer(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Customer customer1 = new com.fedex.phuwv.common.api.models.Customer();

        customer1.setCustomerType( customer.getCustomerType() );
        customer1.setCompanyName( customer.getCompanyName() );
        customer1.setContactName( customer.getContactName() );
        customer1.setPhoneNumber( customer.getPhoneNumber() );
        customer1.setCustomerAccountnumber( customer.getCustomerAccountnumber() );
        customer1.setAlternateAccount( alternateAccountIdToAlternateAccountId( customer.getAlternateAccount() ) );
        customer1.setCustomerAddress( addressToAddress( customer.getCustomerAddress() ) );

        return customer1;
    }

    protected List<com.fedex.phuwv.common.api.models.Customer> customerListToCustomerList(List<Customer> list) {
        if ( list == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.Customer> list1 = new ArrayList<com.fedex.phuwv.common.api.models.Customer>( list.size() );
        for ( Customer customer : list ) {
            list1.add( customerToCustomer( customer ) );
        }

        return list1;
    }

    protected ShipmentDetails shipmentDetailsToShipmentDetails(com.fedex.phuwv.common.dto.ShipmentDetails shipmentDetails) {
        if ( shipmentDetails == null ) {
            return null;
        }

        ShipmentDetails shipmentDetails1 = new ShipmentDetails();

        shipmentDetails1.setShipmentUUID( shipmentDetails.getShipmentUUID() );
        shipmentDetails1.setTotalHU( shipmentDetails.getTotalHU() );
        shipmentDetails1.setMasterTrackingNumber( shipmentDetails.getMasterTrackingNumber() );
        shipmentDetails1.setCarrierDetail( carrierDetailToCarrierDetail( shipmentDetails.getCarrierDetail() ) );
        shipmentDetails1.setCustomerAssociations( customerListToCustomerList( shipmentDetails.getCustomerAssociations() ) );

        return shipmentDetails1;
    }

    protected DateTimeFrame dateTimeFrameToDateTimeFrame(com.fedex.phuwv.common.dto.DateTimeFrame dateTimeFrame) {
        if ( dateTimeFrame == null ) {
            return null;
        }

        DateTimeFrame dateTimeFrame1 = new DateTimeFrame();

        dateTimeFrame1.setType( dateTimeFrame.getType() );
        dateTimeFrame1.setBeginDateTime( dateTimeFrame.getBeginDateTime() );
        dateTimeFrame1.setEndDateTime( dateTimeFrame.getEndDateTime() );

        return dateTimeFrame1;
    }

    protected List<DateTimeFrame> dateTimeFrameListToDateTimeFrameList(List<com.fedex.phuwv.common.dto.DateTimeFrame> list) {
        if ( list == null ) {
            return null;
        }

        List<DateTimeFrame> list1 = new ArrayList<DateTimeFrame>( list.size() );
        for ( com.fedex.phuwv.common.dto.DateTimeFrame dateTimeFrame : list ) {
            list1.add( dateTimeFrameToDateTimeFrame( dateTimeFrame ) );
        }

        return list1;
    }

    protected com.fedex.phuwv.common.api.models.DomainStatus domainStatusToDomainStatus(DomainStatus domainStatus) {
        if ( domainStatus == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.DomainStatus domainStatus1 = new com.fedex.phuwv.common.api.models.DomainStatus();

        domainStatus1.setStatusType( domainStatus.getStatusType() );
        domainStatus1.setStatusValue( domainStatus.getStatusValue() );

        return domainStatus1;
    }

    protected List<com.fedex.phuwv.common.api.models.DomainStatus> domainStatusListToDomainStatusList(List<DomainStatus> list) {
        if ( list == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.DomainStatus> list1 = new ArrayList<com.fedex.phuwv.common.api.models.DomainStatus>( list.size() );
        for ( DomainStatus domainStatus : list ) {
            list1.add( domainStatusToDomainStatus( domainStatus ) );
        }

        return list1;
    }

    protected Waypoint waypointToWaypoint(com.fedex.phuwv.common.dto.Waypoint waypoint) {
        if ( waypoint == null ) {
            return null;
        }

        Waypoint waypoint1 = new Waypoint();

        if ( waypoint.getType() != null ) {
            waypoint1.setType( waypoint.getType().name() );
        }
        waypoint1.setEngageWindows( dateTimeFrameListToDateTimeFrameList( waypoint.getEngageWindows() ) );
        waypoint1.setSequenceNumber( waypoint.getSequenceNumber() );
        waypoint1.setAddress( addressToAddress( waypoint.getAddress() ) );
        waypoint1.setFacilityOpCo( waypoint.getFacilityOpCo() );
        waypoint1.setFacilityCode( waypoint.getFacilityCode() );
        waypoint1.setFacilityCountryCode( waypoint.getFacilityCountryCode() );
        waypoint1.setStatuses( domainStatusListToDomainStatusList( waypoint.getStatuses() ) );

        return waypoint1;
    }

    protected List<Waypoint> waypointListToWaypointList(List<com.fedex.phuwv.common.dto.Waypoint> list) {
        if ( list == null ) {
            return null;
        }

        List<Waypoint> list1 = new ArrayList<Waypoint>( list.size() );
        for ( com.fedex.phuwv.common.dto.Waypoint waypoint : list ) {
            list1.add( waypointToWaypoint( waypoint ) );
        }

        return list1;
    }

    protected HandlingUnitId handlingUnitIdToHandlingUnitId(com.fedex.phuwv.common.dto.HandlingUnitId handlingUnitId) {
        if ( handlingUnitId == null ) {
            return null;
        }

        HandlingUnitId handlingUnitId1 = new HandlingUnitId();

        handlingUnitId1.setIdType( handlingUnitId.getIdType() );
        handlingUnitId1.setId( handlingUnitId.getId() );
        handlingUnitId1.setEntryType( handlingUnitId.getEntryType() );
        handlingUnitId1.setUniqueIdentifier( handlingUnitId.getUniqueIdentifier() );
        handlingUnitId1.setDefiningAuthorityName( handlingUnitId.getDefiningAuthorityName() );

        return handlingUnitId1;
    }

    protected CompositeNaturalId compositeNaturalIdToCompositeNaturalId(com.fedex.phuwv.common.dto.CompositeNaturalId compositeNaturalId) {
        if ( compositeNaturalId == null ) {
            return null;
        }

        CompositeNaturalId compositeNaturalId1 = new CompositeNaturalId();

        compositeNaturalId1.setIds( parameterListDtoToApiModel( compositeNaturalId.getIds() ) );

        return compositeNaturalId1;
    }

    protected com.fedex.phuwv.common.api.models.Parameter parameterToParameter(Parameter parameter) {
        if ( parameter == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Parameter parameter1 = new com.fedex.phuwv.common.api.models.Parameter();

        parameter1.setKey( parameter.getKey() );
        parameter1.setValue( parameter.getValue() );

        return parameter1;
    }

    protected com.fedex.phuwv.common.api.models.WorkRequirement workRequirementToWorkRequirement(WorkRequirement workRequirement) {
        if ( workRequirement == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.WorkRequirement workRequirement1 = new com.fedex.phuwv.common.api.models.WorkRequirement();

        workRequirement1.setId( workRequirement.getId() );
        workRequirement1.setType( workRequirement.getType() );
        workRequirement1.setMajorVersion( workRequirement.getMajorVersion() );
        workRequirement1.setPriority( workRequirement.getPriority() );
        workRequirement1.setAttributes( attributesDtoToApiModel( workRequirement.getAttributes() ) );
        workRequirement1.setAdditionalAttributes( attributesDtoToApiModel( workRequirement.getAdditionalAttributes() ) );
        workRequirement1.setSource( workSourceDtoToApiModel( workRequirement.getSource() ) );
        workRequirement1.setStatuses( workRequirementStatusesDtoToApiModel( workRequirement.getStatuses() ) );
        workRequirement1.setAssociations( workRequirementAssociationsDtoToApiModel( workRequirement.getAssociations() ) );

        return workRequirement1;
    }

    protected com.fedex.phuwv.common.api.models.WorkAttribute workAttributeToWorkAttribute(WorkAttribute workAttribute) {
        if ( workAttribute == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.WorkAttribute workAttribute1 = new com.fedex.phuwv.common.api.models.WorkAttribute();

        workAttribute1.setKey( workAttribute.getKey() );
        List<String> list = workAttribute.getValues();
        if ( list != null ) {
            workAttribute1.setValues( new ArrayList<String>( list ) );
        }

        return workAttribute1;
    }

    protected com.fedex.phuwv.common.api.models.WorkRequirementStatus workRequirementStatusToWorkRequirementStatus(WorkRequirementStatus workRequirementStatus) {
        if ( workRequirementStatus == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.WorkRequirementStatus workRequirementStatus1 = new com.fedex.phuwv.common.api.models.WorkRequirementStatus();

        workRequirementStatus1.setType( workRequirementStatus.getType() );
        workRequirementStatus1.setValue( workRequirementStatus.getValue() );
        workRequirementStatus1.setEffectiveDateTime( workRequirementStatus.getEffectiveDateTime() );

        return workRequirementStatus1;
    }

    protected WorkRequirementAssociation workRequirementAssociationToWorkRequirementAssociation(com.fedex.phuwv.common.dto.WorkRequirementAssociation workRequirementAssociation) {
        if ( workRequirementAssociation == null ) {
            return null;
        }

        WorkRequirementAssociation workRequirementAssociation1 = new WorkRequirementAssociation();

        workRequirementAssociation1.setAssociationTarget( workRequirementAssociation.getAssociationTarget() );
        workRequirementAssociation1.setAssociationUUID( workRequirementAssociation.getAssociationUUID() );
        workRequirementAssociation1.setEffectiveDateTime( workRequirementAssociation.getEffectiveDateTime() );
        workRequirementAssociation1.setExpirationDateTime( workRequirementAssociation.getExpirationDateTime() );

        return workRequirementAssociation1;
    }

    protected com.fedex.phuwv.common.api.models.Task taskToTask(Task task) {
        if ( task == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Task task1 = new com.fedex.phuwv.common.api.models.Task();

        task1.setStatuses( taskStatusesDtoToApiModel( task.getStatuses() ) );
        task1.setWork( workAttributeToWorkAttribute( task.getWork() ) );
        task1.setTaskFacilitys( taskFacilitysDtoToApiModel( task.getTaskFacilitys() ) );

        return task1;
    }

    protected TaskStatus taskStatusToTaskStatus(com.fedex.phuwv.common.dto.TaskStatus taskStatus) {
        if ( taskStatus == null ) {
            return null;
        }

        TaskStatus taskStatus1 = new TaskStatus();

        taskStatus1.setType( taskStatus.getType() );
        taskStatus1.setValue( taskStatus.getValue() );
        taskStatus1.setEffectiveDateTime( taskStatus.getEffectiveDateTime() );

        return taskStatus1;
    }

    protected ShipmentCommodityDetail shipmentCommodityDetailToShipmentCommodityDetail(com.fedex.phuwv.common.dto.ShipmentCommodityDetail shipmentCommodityDetail) {
        if ( shipmentCommodityDetail == null ) {
            return null;
        }

        ShipmentCommodityDetail shipmentCommodityDetail1 = new ShipmentCommodityDetail();

        shipmentCommodityDetail1.setCommodityType( shipmentCommodityDetail.getCommodityType() );
        shipmentCommodityDetail1.setCommodityId( shipmentCommodityDetail.getCommodityId() );
        shipmentCommodityDetail1.setCustomsValue( customsValueDtoToApiModel( shipmentCommodityDetail.getCustomsValue() ) );

        return shipmentCommodityDetail1;
    }
}
