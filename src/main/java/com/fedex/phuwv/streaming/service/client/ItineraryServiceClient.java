package com.fedex.phuwv.streaming.service.client;

import com.fedex.phuwv.common.dto.ItineraryDTO;
import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PHUWV-ITINERARY-SERVICE" , url = "${app.rest-client.end-points.ITINERARY.base-uri}",
		configuration = FeignClientConfig.class)
public interface ItineraryServiceClient {

	@GetMapping(value = "${app.rest-client.end-points.GET_ITINERARY_BY_TRIP_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<ItineraryDTO> getLatestItineraryByTripUUID(@PathVariable("tripUUID") String tripUUID);
}
