package com.fedex.phuwv.streaming.service.service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import com.fedex.phuwv.common.jms.MessageHandler;
import com.fedex.sefs.core.trip.v1.api.TripDomainEvent;

import lombok.extern.slf4j.Slf4j;

@Service("StreamingServiceTripHandler")
@Slf4j
public class StreamingServiceTripHandler implements MessageHandler<TripDomainEvent> {

	@Autowired
	private PhuwvServiceInvoker phuwvServiceInvoker;

	private RetryTemplate tripTriggerRetryTemplate;

	@Autowired
	private StreamingPublishService streamingPublishService;
	
	@Autowired
	private StreamingServiceProperties streamingServiceProperties;
	
	 @Autowired
	 private LocationTranslationInvoker locationTranslationInvoker;

	private static final String WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT = "waiting on another SEFS Trigger event";

	private static final String HANDLINGUNIT_TYPE_CONSOLIDATION = "CONSOLIDATION";

	@Autowired
	public StreamingServiceTripHandler(@Qualifier("jmsRetryTemplate") RetryTemplate tripTriggerRetryTemplate) {
		this.tripTriggerRetryTemplate = tripTriggerRetryTemplate;

	}

	@Override
	public void handle(GenericMessage<TripDomainEvent> genericMessage) throws  Exception {
//Synchronous method call :: START

		tripTriggerRetryTemplate.execute(retryContext -> {
			try {
				TripDomainEvent message = genericMessage.getPayload();
				String tripUUID = message.getTripUUID();
				Audit.getInstance().put(AuditKey.IN, "Trip uuid received for trip trigger event: " + tripUUID);
				log.info("Trip uuid received from the incoming message for trip trigger event: {}", tripUUID);

				if (StringUtils.isNotEmpty(tripUUID)) {
					if(streamingPublishService.validatePublishingPhuwvJms(tripUUID)){
						return null;
					}

					//Call TripService(use TripUUID from the message property to get the HandlingUnitUUID)
					TripDTO futureLatestTripByHandlingUnitUUID = phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(tripUUID);

//					Call HandlingUnitService (use HandlingUnitUUID from the Waypoint data returned from step 1.2)
					HandlingUnit handlingUnit = (null != futureLatestTripByHandlingUnitUUID)?phuwvServiceInvoker.getLatestHandlingUnitByUUID(futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID()):null;

					if (Objects.isNull(handlingUnit)) {
						log.warn("No response from Handling unit service for trip uuid:" + tripUUID
								+ "," + WAITING_ON_ANOTHER_SEFS_TRIGGER_EVENT);
						return null;
					}

					//Call TaskService
					//CompletableFuture<List<Task>> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID());
					List<Task> futureTasksByHandlingUnitUUID = streamingPublishService.invokeGetTaskByHandlingUnitUUID(futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID());

//					Call ShipmentService (use ShipmentUUID from the HandlingUnit PHUWV class returned from step 2)
					//CompletableFuture<ShipmentDetails> futureLatestShipmentByUUID = streamingPublishService.invokeGetFutureLatestShipmentByUUID(handlingUnit.getShipment().getShipmentUUID());
					ShipmentDetails futureLatestShipmentByUUID = streamingPublishService.invokeGetLatestShipmentByUUID(handlingUnit.getShipment().getShipmentUUID());

					// Wait until they are all done
					//CompletableFuture.allOf(futureLatestShipmentByUUID, futureTasksByHandlingUnitUUID).join();

					//ShipmentDetails shipmentDetails = streamingPublishService.settingShipmentDetailsForHandler(futureLatestShipmentByUUID);
					ShipmentDetails shipmentDetails = futureLatestShipmentByUUID;
					if(validateShipmentLegitimacy(handlingUnit, shipmentDetails)){
						return null;
					}

					//List<Task> taskDetails = streamingPublishService.settingTaskDetails(futureTasksByHandlingUnitUUID, futureLatestTripByHandlingUnitUUID.getHandlingUnitUUID());
					List<Task> taskDetails = futureTasksByHandlingUnitUUID;

					List<Waypoint> waypointModelList = streamingPublishService.fetchItineraryAndGetWaypoints(tripUUID);

					// Merge different Models from the calls above into a new/pristine PHUWV
					// HandlingUnit class fully populated.
					streamingPublishService.mergeToPublish(handlingUnit, shipmentDetails, locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointModelList), taskDetails, streamingServiceProperties.getPhuwvSefsCoreType());
					log.info("End of trip trigger event processed with trip uuid: " + message.getTripUUID());
				}
			} catch (Exception e) {
				log.error("Exception:", e);
			}
			return null;
		}, retryContext -> {
			streamingPublishService.setRetryContext(retryContext);
			return null;
		});
//Synchronous method call :: END
	}

	private boolean validateShipmentLegitimacy(HandlingUnit handlingUnit, ShipmentDetails shipmentDetails) {
		if(streamingPublishService.validateObjectForClosure(shipmentDetails, handlingUnit.getShipment().getShipmentUUID())){
			switch (handlingUnit.getType()){
				case HANDLINGUNIT_TYPE_CONSOLIDATION:
					return false;
				default:
					return true;
			}
		}
		return false;
	}
}
