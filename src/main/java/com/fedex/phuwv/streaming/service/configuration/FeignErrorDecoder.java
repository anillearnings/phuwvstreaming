package com.fedex.phuwv.streaming.service.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.http.rest.ApiError;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

@Slf4j
public class FeignErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {

        Reader reader = null;
        try {
            response.body().asInputStream();
            reader = response.body().asReader(StandardCharsets.UTF_8);
            StringWriter writer = new StringWriter();
            IOUtils.copy(reader, writer);
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            ApiError error = mapper.readValue(writer.toString(), ApiError.class);
            reader.close();
            log.error("Exception During Feign Call: "+error.getMessage()+" for the API: "+error.getServletPath() );
        } catch (IOException e) {
            log.error("Failed to read the Error message: {}", e);
        }
        return null;
    }
}
