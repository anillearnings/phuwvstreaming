package com.fedex.phuwv.streaming.service.client;

import com.fedex.phuwv.common.dto.Waypoint;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;

import java.util.List;

@FeignClient(name = "PHUWV-WAYPOINT-SERVICE",url = "${app.rest-client.end-points.WAYPOINT.base-uri}",
configuration = FeignClientConfig.class)
public interface WaypointServiceClient {

	@GetMapping(value = "${app.rest-client.end-points.GET_WAYPOINTS_BY_WAYPOINT_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Waypoint> getLatestWaypointByUUID(@PathVariable("waypointUUID") String waypointUUID);

	@GetMapping(value = "${app.rest-client.end-points.GET_WAYPOINTS_BY_WAYPOINT_TRK_NBR.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Waypoint> getLatestWaypointByTrackNbr(@PathVariable("waypointTrkNbr") String waypointTrkNbr);

	@GetMapping(value = "${app.rest-client.end-points.GET_WAYPOINTS_BY_ENTERPRISE_TRK_NBR.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Waypoint> getWaypointByEnterpriseTrackNbr(@RequestParam("trkNbr") String waypointTrackNbr);

	@DeleteMapping(value = "${app.rest-client.end-points.DELETE_WAYPOINTS_BY_PURGE_DATE.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Boolean> purgeUntilDate(@PathVariable("purgeDt") String purgeDt);

	@PatchMapping(value = "${app.rest-client.end-points.PATCH_WAYPOINTS_BY_WAYPOINT_UUID_PURGE_DATE.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Boolean> updateWaypointWithPurgeDate(@PathVariable(value="waypointUUID") String waypointUUID, @PathVariable(value = "purgeDt") String purgeDt);

	@GetMapping(value = "${app.rest-client.end-points.GET_WAYPOINTS_BY_HANDLING_UNIT_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<List<Waypoint>> getLatestWaypointByHandlingUnitUUID(@PathVariable("huUUID") String huUUID);

	@PostMapping(value = "${app.rest-client.end-points.POST_WAYPOINTS_BY_WAYPOINT_UUIDS.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<List<Waypoint>> getLatestWaypointsByUUIDs(@RequestBody List<String> waypointUUIDs);
}
