package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.api.models.AlternateAccountId;
import com.fedex.phuwv.common.api.models.CarrierDetail;
import com.fedex.phuwv.common.api.models.ShipmentDetails;
import com.fedex.phuwv.common.dto.Address;
import com.fedex.phuwv.common.dto.Customer;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-10-12T17:08:48+0530",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-6.8.2.jar, environment: Java 1.8.0_281 (Oracle Corporation)"
)
@Component
public class ShipmentDetailsDtoToApiModelMapperImpl implements ShipmentDetailsDtoToApiModelMapper {

    @Override
    public ShipmentDetails shipmentDetailsDtoToApiModel(com.fedex.phuwv.common.dto.ShipmentDetails shipmentDetailsDto) {
        if ( shipmentDetailsDto == null ) {
            return null;
        }

        ShipmentDetails shipmentDetails = new ShipmentDetails();

        shipmentDetails.setShipmentUUID( shipmentDetailsDto.getShipmentUUID() );
        shipmentDetails.setTotalHU( shipmentDetailsDto.getTotalHU() );
        shipmentDetails.setMasterTrackingNumber( shipmentDetailsDto.getMasterTrackingNumber() );
        shipmentDetails.setCarrierDetail( carrierDetailDtoToApiModel( shipmentDetailsDto.getCarrierDetail() ) );
        shipmentDetails.setCustomerAssociations( customerAssociationsDtoToApiModel( shipmentDetailsDto.getCustomerAssociations() ) );

        return shipmentDetails;
    }

    @Override
    public CarrierDetail carrierDetailDtoToApiModel(com.fedex.phuwv.common.dto.CarrierDetail carrierDetail) {
        if ( carrierDetail == null ) {
            return null;
        }

        CarrierDetail carrierDetail1 = new CarrierDetail();

        carrierDetail1.setType( carrierDetail.getType() );
        carrierDetail1.setCode( carrierDetail.getCode() );

        return carrierDetail1;
    }

    @Override
    public List<com.fedex.phuwv.common.api.models.Customer> customerAssociationsDtoToApiModel(List<Customer> customerAssociations) {
        if ( customerAssociations == null ) {
            return null;
        }

        List<com.fedex.phuwv.common.api.models.Customer> list = new ArrayList<com.fedex.phuwv.common.api.models.Customer>( customerAssociations.size() );
        for ( Customer customer : customerAssociations ) {
            list.add( customerToCustomer( customer ) );
        }

        return list;
    }

    @Override
    public com.fedex.phuwv.common.api.models.Address customerAddressDtoToApiModel(Address customerAddress) {
        if ( customerAddress == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Address address = new com.fedex.phuwv.common.api.models.Address();

        address.setCountryCode( customerAddress.getCountryCode() );
        address.setCity( customerAddress.getCity() );
        address.setStateCode( customerAddress.getStateCode() );
        address.setPostal( customerAddress.getPostal() );
        address.setAddressLine1( customerAddress.getAddressLine1() );
        address.setAddressLine2( customerAddress.getAddressLine2() );
        address.setAddressLine3( customerAddress.getAddressLine3() );

        return address;
    }

    protected AlternateAccountId alternateAccountIdToAlternateAccountId(com.fedex.phuwv.common.dto.AlternateAccountId alternateAccountId) {
        if ( alternateAccountId == null ) {
            return null;
        }

        AlternateAccountId alternateAccountId1 = new AlternateAccountId();

        alternateAccountId1.setOwner( alternateAccountId.getOwner() );
        alternateAccountId1.setId( alternateAccountId.getId() );
        alternateAccountId1.setCountryCode( alternateAccountId.getCountryCode() );

        return alternateAccountId1;
    }

    protected com.fedex.phuwv.common.api.models.Customer customerToCustomer(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        com.fedex.phuwv.common.api.models.Customer customer1 = new com.fedex.phuwv.common.api.models.Customer();

        customer1.setCustomerType( customer.getCustomerType() );
        customer1.setCompanyName( customer.getCompanyName() );
        customer1.setContactName( customer.getContactName() );
        customer1.setPhoneNumber( customer.getPhoneNumber() );
        customer1.setCustomerAccountnumber( customer.getCustomerAccountnumber() );
        customer1.setAlternateAccount( alternateAccountIdToAlternateAccountId( customer.getAlternateAccount() ) );
        customer1.setCustomerAddress( customerAddressDtoToApiModel( customer.getCustomerAddress() ) );

        return customer1;
    }
}
