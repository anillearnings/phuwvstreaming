package com.fedex.phuwv.streaming.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.fedex.phuwv.common.dto.Task;
import com.fedex.phuwv.common.dto.TaskFacilityDetail;
import com.fedex.phuwv.common.dto.TaskStatus;
import com.fedex.phuwv.common.dto.WorkAttribute;

@Mapper(componentModel = "spring")
public interface TaskDtoToApiModelMapper {
	
	List<com.fedex.phuwv.common.api.models.Task> taskDtoToApiModel(List<Task> taskDto);

    List<com.fedex.phuwv.common.api.models.TaskStatus> taskStatusDtoToApiModel(List<TaskStatus> taskStatus);

    com.fedex.phuwv.common.api.models.WorkAttribute  workAttributeDtoToApiModel(WorkAttribute  workAttribute );

    com.fedex.phuwv.common.api.models.TaskFacilityDetail  taskFacilityDetailDtoToApiModel(TaskFacilityDetail  taskFacilityDetail );
}
