package com.fedex.phuwv.streaming.service.client;

import java.util.List;

import com.fedex.phuwv.common.dto.HandlingUnit;
import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "PHUWV-HANDLING-UNIT-SERVICE" , url = "${app.rest-client.end-points.HANDLING_UNIT.base-uri}",
		configuration = FeignClientConfig.class)
public interface HandlingUnitServiceClient {

	@GetMapping(value = "${app.rest-client.end-points.GET_HU_BY_HU_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<HandlingUnit> getLatestHandlingUnitByUUID(@PathVariable("handlingUnitUUID") String handlingUnitUUID);
	
	@GetMapping(value = "${app.rest-client.end-points.GET_HU_BY_TRK_NBR.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<HandlingUnit> getLatestHandlingUnitByTrackNbr(@RequestParam("trkNbr") String handlingUnitTrackNbr);

	@DeleteMapping(value = "${app.rest-client.end-points.DELETE_HU_BY_PURGE_DATE.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<Integer> purgeUntilDate(@PathVariable(value = "purgeDt") String purgeDt);
	
	@GetMapping(value = "${app.rest-client.end-points.GET_HU_BY_Shipment_UUID.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<List<HandlingUnit>> getListOfLatestHandlingUnitByShipmentUUID(@PathVariable("shipmentUUID") String shipmentUUID);

}
