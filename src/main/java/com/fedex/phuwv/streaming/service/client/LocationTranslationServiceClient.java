package com.fedex.phuwv.streaming.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateRequest;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateResponses;
import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;



@FeignClient(name = "PHUWV-LOCATION-TRANSLATION-SERVICE" , url = "${app.rest-client.end-points.LOCATION_TRANSLATION.base-uri}",configuration = FeignClientConfig.class)
public interface LocationTranslationServiceClient {

	@PostMapping(value = "${app.rest-client.end-points.GET_LOCATION.end-point}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<BulkTranslateResponses> getLocationTranslation(@RequestBody BulkTranslateRequest request);	
}
