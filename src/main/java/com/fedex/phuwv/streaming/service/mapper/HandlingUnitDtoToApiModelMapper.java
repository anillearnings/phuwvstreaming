package com.fedex.phuwv.streaming.service.mapper;

import com.fedex.phuwv.common.dto.*;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HandlingUnitDtoToApiModelMapper {

    @BeforeMapping
    default void beforeMapping(@MappingTarget com.fedex.phuwv.common.api.models.HandlingUnit model, HandlingUnit huDto) {
        if(null != huDto){
            huDto.setShipment(null);

            if(null != huDto.getConsolidationType()){
                model.setConsolidationType(huDto.getConsolidationType());
            }
        }
    }

    @Mappings({@Mapping(
            target = "consolidationType",
            ignore = true
    ), @Mapping(
            target = "shipment.owningOpCo",
            ignore = true
    ), @Mapping(
            target = "additionalAttributes",
            ignore = true
    )})
    com.fedex.phuwv.common.api.models.HandlingUnit huDtoToApiModel(HandlingUnit huDto);

    List<com.fedex.phuwv.common.api.models.HandlingUnitId> handlingUnitIdsDtoToApiModel(List<HandlingUnitId> handlingUnitIds);

    List<com.fedex.phuwv.common.api.models.CompositeNaturalId> naturalIdsDtoToApiModel(List<CompositeNaturalId> naturalIds);

    List<com.fedex.phuwv.common.api.models.Parameter> parameterListDtoToApiModel(List<Parameter> parameterList);

    com.fedex.phuwv.common.api.models.HandlingUnitPhysicalCharacteristic handlingUnitPhysicalCharacteristicDtoToApiModel(HandlingUnitPhysicalCharacteristic handlingUnitPhysicalCharacteristic);

    com.fedex.phuwv.common.api.models.Weight weightDtoToApiModel(Weight weight);

    com.fedex.phuwv.common.api.models.WeightUnits weightUnitsDtoToApiModel(WeightUnits weightUnits);

    com.fedex.phuwv.common.api.models.Dimensions dimensionsDtoToApiModel(Dimensions dimensions);

    com.fedex.phuwv.common.api.models.LinearUnits linearUnitsDtoToApiModel(LinearUnits linearUnits);

    com.fedex.phuwv.common.api.models.Volume volumeDtoToApiModel(Volume volume);

    com.fedex.phuwv.common.api.models.VolumeUnits volumeUnitsDtoToApiModel(VolumeUnits volumeUnits);

    List<com.fedex.phuwv.common.api.models.WorkRequirement> workRequirementsDtoToApiModel(List<WorkRequirement> workRequirements);

    List<com.fedex.phuwv.common.api.models.WorkAttribute> attributesDtoToApiModel(List<WorkAttribute> attributes);

    com.fedex.phuwv.common.api.models.WorkSource workSourceDtoToApiModel(WorkSource workSource);

    List<com.fedex.phuwv.common.api.models.WorkRequirementStatus> workRequirementStatusesDtoToApiModel(List<WorkRequirementStatus> workRequirementStatuses);

    List<com.fedex.phuwv.common.api.models.WorkRequirementAssociation> workRequirementAssociationsDtoToApiModel(List<WorkRequirementAssociation> workRequirementAssociations);

    List<com.fedex.phuwv.common.api.models.Task> tasksDtoToApiModel(List<Task> tasks);

    List<com.fedex.phuwv.common.api.models.TaskStatus> taskStatusesDtoToApiModel(List<TaskStatus> taskStatuses);

    com.fedex.phuwv.common.api.models.TaskFacilityDetail taskFacilitysDtoToApiModel(TaskFacilityDetail taskFacilitys);

    List<com.fedex.phuwv.common.api.models.ShipmentCommodityDetail> shipmentCommodityDetailsDtoToApiModel(List<ShipmentCommodityDetail> shipmentCommodityDetails);

    com.fedex.phuwv.common.api.models.Money customsValueDtoToApiModel(Money customsValue);
}
