package com.fedex.phuwv.streaming.service.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.fedex.phuwv.common.dto.Task;
import com.fedex.phuwv.streaming.service.configuration.FeignClientConfig;

@FeignClient(name = "PHUWV-TASK-SERVICE", url = "${app.rest-client.end-points.TASK.base-uri}", configuration = FeignClientConfig.class)
public interface TaskServiceClient {
	@GetMapping(value = "${app.rest-client.end-points.GET_TASK_BY_HANDLING_UNIT_UUID.end-point}", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<Task>> getTasksByHandlingUnitUUID(
			@PathVariable("handlingUnitUUID") String handlingUnitUUID);

}
