package com.fedex.phuwv.streaming.service.controller;


import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.common.logging.Audit;
import com.fedex.phuwv.common.logging.AuditKey;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.streaming.service.service.StreamingPublishService;
import com.fedex.phuwv.streaming.service.service.StreamingServiceHUHandler;
import com.fedex.phuwv.streaming.service.service.StreamingserviceService;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/api/v1/phuwv")
public class StreamingServiceController {
	
	@Autowired
	private StreamingserviceService service;

	@Autowired
	private StreamingPublishService streamingPublishService;

	@Autowired
	StreamingServiceHUHandler streamingServiceHUHandler;
	
	@Autowired
	private StreamingServiceProperties streamingServiceProperties;
	
	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;
	
	
	@Operation(summary = "Get latest handling unit by handling unit UUID or handlingUnitTrackNumber", description = "returns HandlingUnit.class")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", description = "returns HandlingUnit.class with status OK"),
			@ApiResponse(responseCode = "404", description = "returns ApiError.class with status Not found"),
			@ApiResponse(responseCode = "400", description = "returns ApiError.class with status Bad Request"),
			@ApiResponse(responseCode = "500", description = "returns ApiError.class with status Internal Server Error"),
			@ApiResponse(responseCode = "503", description = "returns ApiError.class with status Service Unavailable") })
	@GetMapping(value = "/handlingUnits/{Id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<HandlingUnit> getHU(
			@PathVariable(value = "Id") String Id) {
		log.info("Call-back service for hu uuid/tracking number:" + Id);
		Audit.getInstance().put(AuditKey.ACTION, "HandlingUnitRestControllerV1.getLatestHandlingUnitByUUID/TrackNbr");
		Audit.getInstance().put(AuditKey.IN, Id);

		HandlingUnit handlingUnitModel = service.getLatestHandlingUnit(Id);
		log.info("Returning call-back response with uuid:" + handlingUnitModel.getHuUUID());
		Audit.getInstance().put(AuditKey.OUT, "Returning call-back response with uuid:" + handlingUnitModel.getHuUUID());
		return ResponseEntity.ok(handlingUnitModel);
	}

	@GetMapping(value = "/publish/{Id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<HandlingUnit> publishHU(@PathVariable(value = "Id") String Id) throws IOException {
		HandlingUnit handlingUnitModel = service.getLatestHandlingUnit(Id);
		streamingPublishService.publishPhuwvEvent(handlingUnitModel, streamingServiceProperties.getPhuwvSefsCoreType());
		return ResponseEntity.ok(handlingUnitModel);
	}
	
	@GetMapping(value = "/testTrigger", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String> publishHU() throws Exception {
	   streamingServiceHUHandler.handle(new GenericMessage<>(JsonUtil.json2Obj(objectMapper, "data",
	         "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class)
	   ));
	   return ResponseEntity.ok("ok");
	}
}
