/*
package com.fedex.phuwv.streaming.service.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.task.v1.api.TaskDomainEvent;
import org.apache.commons.collections4.map.HashedMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.sefs.core.trip.v1.api.TripDomainEvent;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
public class StreamingServiceTripHandlerTest {

	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@InjectMocks
	private StreamingServiceTripHandler streamingServiceTriphandler;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Mock
	private StreamingPublishService streamingPublishService;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(new NeverRetryPolicy());
		ReflectionTestUtils.setField(streamingServiceTriphandler, "tripTriggerRetryTemplate", retryTemplate);

		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
	}

	@Test
	public void genericMessage_NULL_test() throws Exception {
		streamingServiceTriphandler.handle(null);
	}

	@Test
	public void ideal_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString()))
				.thenReturn(new TripDTO());

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(new ShipmentDetails());
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	public void futureLatestHandlingUnitByTripUUID_NULL_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(null);
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	public void futureLatestHandlingUnit_NULL_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(null);
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(null);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	public void futureLatestShipmentByUUID_NULL_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new HandlingUnit()));

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	public void getLatestHandlingUnitByUUID_NULL_Response_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString()))
				.thenReturn(new TripDTO());

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(null);

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_Exception_ShipmentDetails_NONNULL_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid123");
		tripDTO.setHandlingUnitUUID("huuuid123");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString()))
				.thenReturn(tripDTO);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_Exception_ShipmentDetails_NULL_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid123");
		tripDTO.setHandlingUnitUUID("huuuid123");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString()))
				.thenReturn(tripDTO);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	*/
/*@Test
	public void getFutureTaskByHandlingUnitUUID_Exception_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString()))
				.thenReturn(new TripDTO());

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(new HandlingUnit());

		Mockito.when(phuwvServiceInvoker.getFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
		taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
		Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(taskCompletableFuture);

		Mockito.when(phuwvServiceInvoker.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(new ArrayList<>());

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}*//*


	@Test
	public void type_CONS_test() throws ResourceException, Exception {

		TripDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json",
				TripDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid123");
		tripDTO.setHandlingUnitUUID("huuuid123");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString()))
				.thenReturn(tripDTO);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("CONSOLIDATION");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceTriphandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	void validatePublishingPhuwvJms_test() throws Exception {
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
		streamingServiceTriphandler.handle(new GenericMessage<>(JsonUtil.json2Obj(objectMapper, "data", "TripDomainEvent.json", TripDomainEvent.class)));
	}
}
*/
