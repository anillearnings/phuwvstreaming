package com.fedex.phuwv.streaming.service.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fedex.phuwv.common.api.models.HandlingUnit;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.streaming.service.service.StreamingPublishService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fedex.phuwv.common.http.rest.GlobalRestExceptionHandler;
import com.fedex.phuwv.streaming.service.controller.StreamingServiceController;
import com.fedex.phuwv.streaming.service.service.StreamingserviceService;

@RunWith(SpringRunner.class)
public class StreamingServiceControllerTest {
	
 	private MockMvc mockMvc;
	
 	@InjectMocks
 	private StreamingServiceController streamingServiceController;
	
 	@Mock
 	private StreamingserviceService service;

	@Mock
	private StreamingPublishService streamingPublishService;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

 	@Before
 	public void setUp()
 	{
 		mockMvc = MockMvcBuilders.standaloneSetup(streamingServiceController).setControllerAdvice(new GlobalRestExceptionHandler())
 				.build();
 	}

 	@Test
 	public void givenHandlingUnitUUID_whenGetLatestHandlingUnitByUUID_thenReturn200StatusAndHandlingUnitModel() throws Exception
 	{
 		Mockito.when(service.getLatestHandlingUnit(Mockito.anyString())).thenReturn(new HandlingUnit());
 		mockMvc.perform(get("/api/v1/phuwv/handlingUnits/Id").accept(MediaType.APPLICATION_JSON_VALUE))
 		.andExpect(status().isOk());
 	}

	@Test
	public void givenHandlingUnitUUID_whenGetLatestHandlingUnitByUUID_thenReturn200StatusAndHandlingUnitModel_After_Publish() throws Exception
	{
		Mockito.when(service.getLatestHandlingUnit(Mockito.anyString())).thenReturn(new HandlingUnit());
		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());
		mockMvc.perform(get("/api/v1/phuwv/publish/Id").accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
 }
