/*
package com.fedex.phuwv.streaming.service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
public class StreamingServiceHUHandlerTest {

	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@InjectMocks
	private StreamingServiceHUHandler streamingServiceHUHandler;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Mock
	private StreamingPublishService streamingPublishService;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(new NeverRetryPolicy());
		ReflectionTestUtils.setField(streamingServiceHUHandler, "huTriggerRetryTemplate", retryTemplate);

		Mockito.when(streamingServiceProperties.getHandlingunitAssociationLevelShipment()).thenReturn("SHIPMENT");
		Mockito.when(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull()).thenReturn("ISTRIPITINERARYSHIPMENTNULL");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeShipment()).thenReturn("SHIPMENT");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint()).thenReturn("WAYPOINT");
		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
	}

	@Test
	void non_ideal_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenThrow(NullPointerException.class);

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void ideal_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

//
	@Test
	void futureLatestHandlingUnitByUUID_NULL_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));
		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));
		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void futureLatestTripByHandlingUnitUUID_NULL_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", null);
		tripAndItineraryMap.put("ITINERARY", new ItineraryDTO());
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void futureLatestShipmentByUUID_NULL_test() throws ResourceException, Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void futureHandlingUnitByUUID_NULL_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		CompletableFuture<HandlingUnit> handlingUnitCompletableFuture = new CompletableFuture<>();
		handlingUnitCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnitCompletableFuture);

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void futureTripByHandlingUnitUUID_NULL_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
		tripDTOCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(tripDTOCompletableFuture);
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", null);
		tripAndItineraryMap.put("ITINERARY", new ItineraryDTO());
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void futureShipmentByUUID_NULL_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
		shipmentDetailsCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(shipmentDetailsCompletableFuture);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void getLatestItineraryByTripUUID_NULL_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", null);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void getLatestWaypointByUUID_Exception_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void ideal_CONS_type_test() throws Exception {

		HandlingUnitDomainEvent handlingUnitDomainEvent = JsonUtil.json2Obj(objectMapper, "data",
				"HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("CONSOLIDATION");
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnit));

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingHandlingUnit(Mockito.any()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.validateHandlingUnitForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceHUHandler.handle(new GenericMessage<>(handlingUnitDomainEvent));
	}

	@Test
	void validatePublishingPhuwvJms_test() throws Exception {
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
		streamingServiceHUHandler.handle(new GenericMessage<>(JsonUtil.json2Obj(objectMapper, "data", "HandlingUnitDomainEvent.json", HandlingUnitDomainEvent.class)));
	}
}
*/
