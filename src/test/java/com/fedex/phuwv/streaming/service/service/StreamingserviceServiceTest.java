package com.fedex.phuwv.streaming.service.service;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.exception.NoDataFoundException;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
public class StreamingserviceServiceTest {

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	@InjectMocks
	private StreamingserviceService streamingService;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingPublishService streamingPublishService;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("HANDLINGUNIT");
		Mockito.when(streamingServiceProperties.getPhuwvCheckIfUuid()).thenReturn("ISUUID");
		Mockito.when(streamingServiceProperties.getPhuwvCheckIfHandlingunitNull()).thenReturn("ISHANDLINGUNITNULL");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitUUID() {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
		streamingService.getLatestHandlingUnit("85044265-995a-3712-9604-5faa32769651");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber() {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitUUID_NULLResponse() {
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(null);
		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
		streamingService.getLatestHandlingUnit("85044265-995a-3712-9604-5faa32769651");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_NULLResponse() {
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(null);
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitWithSepData(Mockito.anyString())).thenReturn(new HandlingUnit());
		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_NULL_Shipment() {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitUUID_NULL_TripDTO_NULL_ShipmentDetails() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("79480-9491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_NULL_TripDTO_NULL_ShipmentDetails() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_NULL_Input() {
		streamingService.getLatestHandlingUnit(null);
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_ExceptionInWaypoint() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_DESTINATION() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.DESTINATION);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(Arrays.asList(waypoint));

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_DESTINATION_FACILITY() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.DESTINATION_FACILITY);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(Arrays.asList(waypoint));

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_ORIGIN() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.ORIGIN);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(Arrays.asList(waypoint));

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_OTHER() throws ExecutionException, InterruptedException {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(Arrays.asList(waypoint));

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_merge_Exception() {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenThrow(NullPointerException.class);
		streamingService.getLatestHandlingUnit("794809491762");
	}

	@Test
	public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_Task_NULL() {
		HandlingUnit handlingUnitFinal = new HandlingUnit();
		handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
		handlingUnitFinal.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr(Mockito.anyString()))
				.thenReturn(handlingUnitFinal);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("85044265-995a-3712-9604-5faa327696512521");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
		taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(taskCompletableFuture);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("85044265-995a-3712-9604-5faa32769651252121");
		itineraryItems.setWaypointUUID("85044265-995a-3712-9604-5faa32769651252111");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
		streamingService.getLatestHandlingUnit("794809491762");
	}
}