/*
 package com.fedex.phuwv.streaming.service.service;

 import static org.mockito.ArgumentMatchers.any;
 import static org.mockito.Mockito.when;

 import java.io.IOException;
 import java.util.ArrayList;
 import java.util.List;
 import java.util.concurrent.CompletableFuture;

 import com.fedex.phuwv.common.dto.*;
 import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
 import com.fedex.sefs.core.trip.v1.api.TripDomainEvent;
 import com.fedex.sefs.core.waypoint.v3.api.WaypointDomainEvent;
 import org.junit.jupiter.api.BeforeEach;
 import org.junit.jupiter.api.Test;
 import org.junit.jupiter.api.extension.ExtendWith;
 import org.mockito.InjectMocks;
 import org.mockito.Mock;
 import org.mockito.Mockito;
 import org.mockito.MockitoAnnotations;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.beans.factory.annotation.Qualifier;
 import org.springframework.boot.test.context.SpringBootTest;
 import org.springframework.messaging.support.GenericMessage;
 import org.springframework.retry.policy.NeverRetryPolicy;
 import org.springframework.retry.support.RetryTemplate;
 import org.springframework.test.context.ActiveProfiles;
 import org.springframework.test.context.junit.jupiter.SpringExtension;

 import com.fasterxml.jackson.core.JsonParseException;
 import com.fasterxml.jackson.databind.JsonMappingException;
 import com.fasterxml.jackson.databind.ObjectMapper;
 import com.fedex.phuwv.common.exception.ResourceException;
 import com.fedex.phuwv.util.JsonUtil;
 import org.springframework.test.util.ReflectionTestUtils;


 @ExtendWith(SpringExtension.class)
 @ActiveProfiles("L0")
 @SpringBootTest
 public class StreamingServiceWayPointHandlerTest {

 	@Autowired
 	@Qualifier("defaultObjectMapper")
 	private ObjectMapper objectMapper;

 	@InjectMocks
 	private StreamingServiceWayPointHandler streamingServiceWaypointHandler;

 	@Mock
 	private PhuwvServiceInvoker phuwvServiceInvoker;

 	WaypointDomainEvent waypointDomainEvent;
 	String handlingUnitUUID;

	 @Mock
	 private StreamingPublishService streamingPublishService;

	 @Mock
	 private LocationTranslationInvoker locationTranslationInvoker;

     @Mock
     private StreamingServiceProperties streamingServiceProperties;

 	@BeforeEach
 	void setup() throws JsonParseException, JsonMappingException, IOException
 	{
		MockitoAnnotations.initMocks(this);

 	 waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent1.json",
 				WaypointDomainEvent.class);
 	 handlingUnitUUID=waypointDomainEvent.getAssociations().stream().filter(s->s.getAssociationLevel().equals("HANDLINGUNIT"))
 			  .map(x->x.getAssociationUUID()).findAny().orElse(null);

        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(new NeverRetryPolicy());
        ReflectionTestUtils.setField(streamingServiceWaypointHandler, "waypointTriggerRetryTemplate", retryTemplate);

        Mockito.when(streamingServiceProperties.getWaypointAssociationLevelHandlingunit()).thenReturn("HANDLINGUNIT");
        Mockito.when(streamingServiceProperties.getWaypointAssociationLevelTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
        Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
 	}

     @Test
     public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_Overall_Exception()
             throws ResourceException, Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent2.json",
                 WaypointDomainEvent.class);

         when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID((any(String.class)))).thenThrow(NullPointerException.class);

         streamingServiceWaypointHandler.handle(new GenericMessage<>(waypointDomainEvent));

     }

     @Test
     public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_Overall_NULL()
             throws ResourceException, Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent2.json",
                 WaypointDomainEvent.class);

         when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID((any(String.class)))).thenReturn(null);

         streamingServiceWaypointHandler.handle(new GenericMessage<>(waypointDomainEvent));

     }

 	@Test
 	public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_NULL_getLatestHandlingUnitByUUID()
 			throws ResourceException, Exception {
 		waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent2.json",
 				WaypointDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setHandlingUnitUUID("huuuid123");
		when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID((any(String.class)))).thenReturn(tripDTO);

		when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(null);

 		streamingServiceWaypointHandler.handle(new GenericMessage<>(waypointDomainEvent));

 	}

	 @Test
	 public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_NULL_ShipmentDetails()
			 throws ResourceException, Exception {
		 waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent2.json",
				 WaypointDomainEvent.class);

		 TripDTO tripDTO = new TripDTO();
		 tripDTO.setHandlingUnitUUID("huuuid123");
		 when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID((any(String.class)))).thenReturn(tripDTO);

		 HandlingUnit handlingUnit = new HandlingUnit();
		 ShipmentDetails shipment = new ShipmentDetails();
		 shipment.setShipmentUUID("shipmentuuid123");
		 handlingUnit.setShipment(shipment);
         handlingUnit.setType("SHIPMENT");
		 when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

		 when(streamingPublishService.invokeGetFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(null));

         Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		 ItineraryDTO itineraryDTO = new ItineraryDTO();
		 List<ItineraryItems> items = new ArrayList<>();
		 ItineraryItems itineraryItems = new ItineraryItems();
		 itineraryItems.setWaypointUUID("waypointuuid123");
		 items.add(itineraryItems);
		 itineraryDTO.setItems(items);
		 when(streamingPublishService.fetchItineraryAndGetWaypoints((any(String.class)))).thenReturn(new ArrayList());

		 streamingServiceWaypointHandler.handle(new GenericMessage<WaypointDomainEvent>(waypointDomainEvent));

	 }

	 @Test
	 public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_NONNULL_ShipmentDetails()
			 throws ResourceException, Exception {
		 waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent2.json",
				 WaypointDomainEvent.class);

		 TripDTO tripDTO = new TripDTO();
		 tripDTO.setHandlingUnitUUID("huuuid123");
		 when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID((any(String.class)))).thenReturn(tripDTO);

		 HandlingUnit handlingUnit = new HandlingUnit();
		 ShipmentDetails shipment = new ShipmentDetails();
		 shipment.setShipmentUUID("shipmentuuid123");
		 handlingUnit.setShipment(shipment);
         handlingUnit.setType("SHIPMENT");
		 when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

		 ShipmentDetails shipmentDetails = new ShipmentDetails();
		 shipmentDetails.setShipmentUUID("shipmentuuid123");
		 when(streamingPublishService.invokeGetFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(shipmentDetails));

         Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		 ItineraryDTO itineraryDTO = new ItineraryDTO();
		 List<ItineraryItems> items = new ArrayList<>();
		 ItineraryItems itineraryItems = new ItineraryItems();
		 itineraryItems.setWaypointUUID("waypointuuid123");
		 items.add(itineraryItems);
		 itineraryDTO.setItems(items);
         when(streamingPublishService.fetchItineraryAndGetWaypoints((any(String.class)))).thenReturn(new ArrayList<>());

         Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		 when(locationTranslationInvoker.convertOSCWaypointsToLSSI((any(List.class)))).thenReturn(new ArrayList<>());

		 Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		 streamingServiceWaypointHandler.handle(new GenericMessage<WaypointDomainEvent>(waypointDomainEvent));

	 }

     @Test
     public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_NONNULL_ShipmentDetailsAndWaypoint()
             throws ResourceException, Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent2.json",
                 WaypointDomainEvent.class);

         TripDTO tripDTO = new TripDTO();
         tripDTO.setHandlingUnitUUID("huuuid123");
         when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID((any(String.class)))).thenReturn(tripDTO);

         HandlingUnit handlingUnit = new HandlingUnit();
         ShipmentDetails shipment = new ShipmentDetails();
         shipment.setShipmentUUID("shipmentuuid123");
         handlingUnit.setShipment(shipment);
         handlingUnit.setType("SHIPMENT");
         when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

         ShipmentDetails shipmentDetails = new ShipmentDetails();
         shipmentDetails.setShipmentUUID("shipmentuuid123");
         when(streamingPublishService.invokeGetFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(shipmentDetails));

         Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

         ItineraryDTO itineraryDTO = new ItineraryDTO();
         List<ItineraryItems> items = new ArrayList<>();
         ItineraryItems itineraryItems = new ItineraryItems();
         itineraryItems.setWaypointUUID("waypointuuid123");
         items.add(itineraryItems);
         itineraryDTO.setItems(items);
         when(streamingPublishService.fetchItineraryAndGetWaypoints((any(String.class)))).thenReturn(new ArrayList());

         Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

         when(locationTranslationInvoker.convertOSCWaypointsToLSSI((any(List.class)))).thenReturn(new ArrayList());

         Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

         streamingServiceWaypointHandler.handle(new GenericMessage<WaypointDomainEvent>(waypointDomainEvent));

     }

     @Test
     public void when_call_handler_using_WaypointDomainEvent_havingOnly_TripUUID_invoke_WaypointServiceTrigger_NONNULL_ShipmentDetailsAndWaypoint_WithHUInDomainEvent()
             throws ResourceException, Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent1.json",
                 WaypointDomainEvent.class);

         HandlingUnit handlingUnit = new HandlingUnit();
         ShipmentDetails shipment = new ShipmentDetails();
         shipment.setShipmentUUID("shipmentuuid123");
         handlingUnit.setShipment(shipment);
         handlingUnit.setType("SHIPMENT");
         when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

         ShipmentDetails shipmentDetails = new ShipmentDetails();
         shipmentDetails.setShipmentUUID("shipmentuuid123");
         when(streamingPublishService.invokeGetFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(shipmentDetails));

         Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

         ItineraryDTO itineraryDTO = new ItineraryDTO();
         List<ItineraryItems> items = new ArrayList<>();
         ItineraryItems itineraryItems = new ItineraryItems();
         itineraryItems.setWaypointUUID("waypointuuid123");
         items.add(itineraryItems);
         itineraryDTO.setItems(items);
         when(streamingPublishService.fetchItineraryAndGetWaypoints((any(String.class)))).thenReturn(new ArrayList());

         Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

         when(locationTranslationInvoker.convertOSCWaypointsToLSSI((any(List.class)))).thenReturn(new ArrayList());

         Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

         streamingServiceWaypointHandler.handle(new GenericMessage<WaypointDomainEvent>(waypointDomainEvent));

     }

     */
/*@Test
     public void when_call_getFutureTaskByHandlingUnitUUID_Exception_Test()
             throws ResourceException, Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent1.json",
                 WaypointDomainEvent.class);

         HandlingUnit handlingUnit = new HandlingUnit();
         ShipmentDetails shipment = new ShipmentDetails();
         shipment.setShipmentUUID("shipmentuuid123");
         handlingUnit.setShipment(shipment);
         when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

         ShipmentDetails shipmentDetails = new ShipmentDetails();
         shipmentDetails.setShipmentUUID("shipmentuuid123");
         when(phuwvServiceInvoker.getFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(shipmentDetails));

         CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
         taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
         Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(taskCompletableFuture);

         ItineraryDTO itineraryDTO = new ItineraryDTO();
         List<ItineraryItems> items = new ArrayList<>();
         ItineraryItems itineraryItems = new ItineraryItems();
         itineraryItems.setWaypointUUID("waypointuuid123");
         items.add(itineraryItems);
         itineraryDTO.setItems(items);
         when(phuwvServiceInvoker.getLatestItineraryByTripUUID((any(String.class)))).thenReturn(itineraryDTO);

         when(phuwvServiceInvoker.getLatestWaypointsByUUIDs((any(List.class)))).thenReturn(new ArrayList());

         streamingServiceWaypointHandler.handle(new GenericMessage<WaypointDomainEvent>(waypointDomainEvent));

     }*//*


     @Test
     public void when_using_domain_event_with_huuuid_test() throws Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent3.json", WaypointDomainEvent.class);

         HandlingUnit handlingUnit = new HandlingUnit();
         ShipmentDetails shipment = new ShipmentDetails();
         shipment.setShipmentUUID("shipmentuuid123");
         handlingUnit.setShipment(shipment);
         handlingUnit.setType("SHIPMENT");
         when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

         ShipmentDetails shipmentDetails = new ShipmentDetails();
         shipmentDetails.setShipmentUUID("shipmentuuid123");
         when(streamingPublishService.invokeGetFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(shipmentDetails));

         Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

         ItineraryDTO itineraryDTO = new ItineraryDTO();
         List<ItineraryItems> items = new ArrayList<>();
         ItineraryItems itineraryItems = new ItineraryItems();
         itineraryItems.setWaypointUUID("waypointuuid123");
         items.add(itineraryItems);
         itineraryDTO.setItems(items);
         when(streamingPublishService.fetchItineraryAndGetWaypoints((any(String.class)))).thenReturn(new ArrayList());

         Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

         when(locationTranslationInvoker.convertOSCWaypointsToLSSI((any(List.class)))).thenReturn(new ArrayList());

         Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

         streamingServiceWaypointHandler.handle(new GenericMessage<>(waypointDomainEvent));
     }

     @Test
     public void when_using_CONS_test() throws Exception {
         waypointDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "WaypointDomainEvent3.json", WaypointDomainEvent.class);

         HandlingUnit handlingUnit = new HandlingUnit();
         ShipmentDetails shipment = new ShipmentDetails();
         shipment.setShipmentUUID("shipmentuuid123");
         handlingUnit.setShipment(shipment);
         handlingUnit.setType("CONSOLIDATION");
         when(phuwvServiceInvoker.getLatestHandlingUnitByUUID((any(String.class)))).thenReturn(handlingUnit);

         ShipmentDetails shipmentDetails = new ShipmentDetails();
         shipmentDetails.setShipmentUUID("shipmentuuid123");
         when(streamingPublishService.invokeGetFutureLatestShipmentByUUID((any(String.class)))).thenReturn(CompletableFuture.completedFuture(null));

         Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
                 .thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

         ItineraryDTO itineraryDTO = new ItineraryDTO();
         List<ItineraryItems> items = new ArrayList<>();
         ItineraryItems itineraryItems = new ItineraryItems();
         itineraryItems.setWaypointUUID("waypointuuid123");
         items.add(itineraryItems);
         itineraryDTO.setItems(items);
         when(streamingPublishService.fetchItineraryAndGetWaypoints((any(String.class)))).thenReturn(new ArrayList());

         Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

         when(locationTranslationInvoker.convertOSCWaypointsToLSSI((any(List.class)))).thenReturn(new ArrayList());

         Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

         streamingServiceWaypointHandler.handle(new GenericMessage<>(waypointDomainEvent));
     }

     @Test
     void validatePublishingPhuwvJms_test() throws Exception {
         Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
         streamingServiceWaypointHandler.handle(new GenericMessage<>(waypointDomainEvent));
     }
 }



*/
