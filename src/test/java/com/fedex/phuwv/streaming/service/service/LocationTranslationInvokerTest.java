package com.fedex.phuwv.streaming.service.service;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateResponses;
import com.fedex.phuwv.common.dto.lssi.dto.TranslateResponseV2;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
@Slf4j
public class LocationTranslationInvokerTest {

    @InjectMocks
    private LocationTranslationInvoker locationTranslationInvoker;

    @Mock
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Mock
    private StreamingServiceProperties streamingServiceProperties;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void when_call_handlingUnitUUID_phuwvServiceInvokerSuccess()
            throws Exception {

        List<Waypoint> waypointOSCList = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setFacilityOpCo("TNT");
        waypoint.setFacilityCode("MAD");
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypointOSCList.add(waypoint);

        Mockito.when(streamingServiceProperties.getWaypointOscFacilityopcoTnt()).thenReturn("TNT");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropParty()).thenReturn("interopParty");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaValueOsc()).thenReturn("OSC");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropOperationId()).thenReturn("interopOperationId");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyBusinessContext()).thenReturn("businessContext");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetTypeKey()).thenReturn("interopParty");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetTypeValue()).thenReturn("LSSI");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetValueKey()).thenReturn("interopOperationId");

        BulkTranslateResponses bulkTranslateResponses = new BulkTranslateResponses();
        TranslateResponseV2 translateResponseV2List = new TranslateResponseV2();
        translateResponseV2List.setTargetValue("MADT8");
        bulkTranslateResponses.add(translateResponseV2List);
        Mockito.when(phuwvServiceInvoker.getFutureTranslatedLocationCode(Mockito.any())).thenReturn(bulkTranslateResponses);

        locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointOSCList);
    }

    @Test
    public void when_call_handlingUnitUUID_phuwvServiceInvokerSuccess_NULL_LocationTranslation()
            throws Exception {

        List<Waypoint> waypointOSCList = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setFacilityOpCo("TNT");
        waypoint.setFacilityCode("MAD");
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypointOSCList.add(waypoint);

        Mockito.when(streamingServiceProperties.getWaypointOscFacilityopcoTnt()).thenReturn("TNT");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropParty()).thenReturn("interopParty");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaValueOsc()).thenReturn("OSC");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropOperationId()).thenReturn("interopOperationId");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyBusinessContext()).thenReturn("businessContext");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetTypeKey()).thenReturn("interopParty");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetTypeValue()).thenReturn("LSSI");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetValueKey()).thenReturn("interopOperationId");

        BulkTranslateResponses bulkTranslateResponses = new BulkTranslateResponses();
        TranslateResponseV2 translateResponseV2List = new TranslateResponseV2();
        translateResponseV2List.setTargetValue("MADT8");
        bulkTranslateResponses.add(translateResponseV2List);
        Mockito.when(phuwvServiceInvoker.getFutureTranslatedLocationCode(Mockito.any())).thenReturn(null);

        locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointOSCList);
    }

    @Test
    public void when_call_handlingUnitUUID_phuwvServiceInvokerFailure()
            throws Exception {

        List<Waypoint> waypointOSCList = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setFacilityOpCo("TNT");
        waypoint.setFacilityCode("MAD");
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypointOSCList.add(waypoint);

        Mockito.when(streamingServiceProperties.getWaypointOscFacilityopcoTnt()).thenReturn("TNT");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropParty()).thenReturn("interopParty");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaValueOsc()).thenReturn("OSC");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyInteropOperationId()).thenReturn("interopOperationId");
        Mockito.when(streamingServiceProperties.getTranslateRequestFromCriteriaKeyBusinessContext()).thenReturn("businessContext");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetTypeKey()).thenReturn("interopParty");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetTypeValue()).thenReturn("LSSI");
        Mockito.when(streamingServiceProperties.getTranslateRequestToTargetValueKey()).thenReturn("interopOperationId");

        BulkTranslateResponses bulkTranslateResponses = new BulkTranslateResponses();
        TranslateResponseV2 translateResponseV2List = new TranslateResponseV2();
        translateResponseV2List.setTargetValue("MADT8");
        bulkTranslateResponses.add(translateResponseV2List);
        Mockito.when(phuwvServiceInvoker.getFutureTranslatedLocationCode(Mockito.any())).thenThrow(NullPointerException.class);

        locationTranslationInvoker.convertOSCWaypointsToLSSI(waypointOSCList);
    }
}
