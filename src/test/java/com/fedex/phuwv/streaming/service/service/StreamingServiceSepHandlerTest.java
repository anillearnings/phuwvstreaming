/*
package com.fedex.phuwv.streaming.service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.dto.sep.xmlparser.dto.EnhancedEvent;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.util.JsonUtil;

import com.fedex.sefs.core.itinerary.v1.api.ItineraryDomainEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
public class StreamingServiceSepHandlerTest {

	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@InjectMocks
	private StreamingServiceSepHandler streamingServiceSepHandler;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	@Mock
	private StreamingPublishService streamingPublishService;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(new NeverRetryPolicy());
		ReflectionTestUtils.setField(streamingServiceSepHandler, "sepTriggerRetryTemplate", retryTemplate);

		Mockito.when(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull()).thenReturn("ISTRIPITINERARYSHIPMENTNULL");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeShipment()).thenReturn("SHIPMENT");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint()).thenReturn("WAYPOINT");
		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.when(streamingServiceProperties.getPhuwvSepCoreType()).thenReturn("SEP");
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
	}

	@Test
	public void idealTest() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));
		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void futureLatestHandlingUnitByUUID_NULL_test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void futureLatestTripByHandlingUnitUUID_NULL_test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new HandlingUnit()));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void futureLatestShipmentByUUID_NULL_test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new HandlingUnit()));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void futureHandlingUnitByUUID_NULL_test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		CompletableFuture<HandlingUnit> handlingUnitCompletableFuture = new CompletableFuture<>();
		handlingUnitCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnitCompletableFuture);

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void futureTripByHandlingUnitUUID_NULL_test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new HandlingUnit()));

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
		tripDTOCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(tripDTOCompletableFuture);
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void futureShipmentByUUID_NULL_test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new HandlingUnit()));

		CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
		shipmentDetailsCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(shipmentDetailsCompletableFuture);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void getLatestHandlingUnitBySepTrackingNbr_Exception_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenThrow(NullPointerException.class);

		Mockito.when(phuwvServiceInvoker.getFutureHandlingUnitWithSepData(Mockito.anyString()))
				.thenReturn(new HandlingUnit());

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void getFutureShipmentByUUID_Exception_getFutureTripByHandlingUnitUUID_Exception_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", null);
		tripAndItineraryMap.put("ITINERARY", new ItineraryDTO());
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void getFutureShipmentByUUID_Exception_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void getLatestWaypointByUUID_Exception_getPhuwvReleaseVersion_Exception_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenThrow(NullPointerException.class);

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void waypoint_DESTINATION_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.DESTINATION);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void waypoint_DESTINATION_FACILITY_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.DESTINATION_FACILITY);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void waypoint_ORIGIN_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.ORIGIN);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	public void waypoint_LINEHAUL_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	*/
/*@Test
	public void getFutureTaskByHandlingUnitUUID_Exception_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(new HandlingUnit());

		Mockito.when(phuwvServiceInvoker.getFutureShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
		taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
		Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(taskCompletableFuture);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}*//*


	@Test
	public void type_CONS_Test() throws Exception {

		EnhancedEvent enhancedEvent = JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json",
				EnhancedEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("CONSOLIDATION");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingItineraryForSepTrigger(Mockito.any(), Mockito.anyString()))
				.thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip())
				.thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary())
				.thenReturn("ITINERARY");

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceSepHandler.handle(new GenericMessage<>(enhancedEvent));
	}

	@Test
	void validatePublishingPhuwvJms_test() throws Exception {
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
		streamingServiceSepHandler.handle(new GenericMessage<>(JsonUtil.json2Obj(objectMapper, "data", "SepDomainEvent.json", EnhancedEvent.class)));
	}
}
*/
