/*
package com.fedex.phuwv.streaming.service.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.dto.sep.xmlparser.dto.EnhancedEvent;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.sefs.core.shipment.v1.api.ShipmentDomainEvent;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest

public class StreamingServiceShipmentHandlerTest {

	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@InjectMocks
	private StreamingServiceShipmentHandler streamingServiceShipmentHandler;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Mock
	private StreamingPublishService streamingPublishService;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	private List<HandlingUnit> handlingUnitList = new ArrayList<HandlingUnit>();

	private HandlingUnit huModel = Mockito.mock(HandlingUnit.class);

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(new NeverRetryPolicy());
		ReflectionTestUtils.setField(streamingServiceShipmentHandler, "shipmentTriggerRetryTemplate", retryTemplate);

		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
	}

	@Test
	public void idealTest() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		huModel.setType("SHIPMENT");
		handlingUnitList.add(huModel);

		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnitList));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void futureLatestShipmentByUUID_NULL_test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		huModel.setType("SHIPMENT");
		handlingUnitList.add(huModel);
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnitList));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void futureLatestTripByHandlingUnitUUID_NULL_test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		huModel.setType("SHIPMENT");
		handlingUnitList.add(huModel);
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnitList));

		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void futureLatestHandlingUnitByUUID_NULL_test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getFutureLatestShipmentByUUID_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
		shipmentDetailsCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(shipmentDetailsCompletableFuture);

		CompletableFuture<List<HandlingUnit>> huListCompletableFuture = new CompletableFuture<>();
		huListCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(huListCompletableFuture);

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getFutureListHandlingUnitByShipmentUUID_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		CompletableFuture<List<HandlingUnit>> huListCompletableFuture = new CompletableFuture<>();
		huListCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(huListCompletableFuture);

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getFutureListHandlingUnitByShipmentUUID_EmptyList_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));


		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getFutureLatestTripByHandlingUnitUUID_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
		tripDTOCompletableFuture.completeExceptionally(new Exception("HTTP call failed!"));
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(tripDTOCompletableFuture);

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getLatestItineraryByTripUUID_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenThrow(NullPointerException.class);

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_DESTINATION_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.DESTINATION);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_DESTINATION_FACILITY_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.DESTINATION_FACILITY);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_ORIGIN_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.ORIGIN);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getLatestWaypointByUUID_LINEHAUL_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void getPhuwvReleaseVersion_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenThrow(NullPointerException.class);

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	*/
/*@Test
	public void getFutureTaskByHandlingUnitUUID_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		handlingUnitList.add(huModel);

		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnitList));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
		taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
		Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(taskCompletableFuture);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(phuwvServiceInvoker.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}*//*


	@Test
	public void publishPhuwvEvent_Exception_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doThrow(NullPointerException.class).when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void type_SHIPMENT_For_Shipment_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void type_CONS_For_Shipment_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("CONSOLIDATION");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	public void type_CONS_For_Itinerary_Test() throws Exception {

		ShipmentDomainEvent shipmentDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json",
				ShipmentDomainEvent.class);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		List<HandlingUnit> handlingUnits = new ArrayList<>();
		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("CONSOLIDATION");
		handlingUnits.add(handlingUnit);
		Mockito.when(phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(handlingUnits));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenThrow(NullPointerException.class);

		Waypoint waypoint = new Waypoint();
		waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
		waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.fetchLSSIWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.1");

		Mockito.when(streamingPublishService.buildPhuwvAPI(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList())).thenReturn(new com.fedex.phuwv.common.api.models.HandlingUnit());

		Mockito.doNothing().when(streamingPublishService).publishPhuwvEvent(Mockito.any(), Mockito.anyString());

		streamingServiceShipmentHandler.handle(new GenericMessage<>(shipmentDomainEvent));
	}

	@Test
	void validatePublishingPhuwvJms_test() throws Exception {
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
		streamingServiceShipmentHandler.handle(new GenericMessage<>(JsonUtil.json2Obj(objectMapper, "data", "ShipmentDomainEvent.json", ShipmentDomainEvent.class)));
	}
}
*/
