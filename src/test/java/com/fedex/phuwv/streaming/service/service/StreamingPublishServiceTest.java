package com.fedex.phuwv.streaming.service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.exception.NoDataFoundException;
import com.fedex.phuwv.common.exception.RetryableException;
import com.fedex.phuwv.common.exception.classifier.ExceptionClassifier;
import com.fedex.phuwv.common.jms.JMSPublisher;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.retry.RetryContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
@Slf4j
public class StreamingPublishServiceTest {

    @InjectMocks
    private StreamingPublishService streamingPublishService;

    @Mock
    private JMSPublisher domainEventPublisher;

    @Mock
    private StreamingServiceProperties streamingServiceProperties;

    @Autowired
    @Qualifier("defaultObjectMapper")
    private ObjectMapper objectMapper;

    @Mock
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Mock
    private LocationTranslationInvoker locationTranslationInvoker;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(streamingPublishService, "objectMapper", objectMapper);

        ExceptionClassifier<RetryableException> exClassifier = classifiable -> new RetryableException("exception");
        ReflectionTestUtils.setField(streamingPublishService, "exClassifier", exClassifier);
    }

    @Test
    public void test_mergeToPublish_LoopThroughWaypoints_DESTINATION() {
        HandlingUnit handlingUnitFinal = new HandlingUnit();
        handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");
        handlingUnitFinal.setOwningOpCo("owningopco");

        ShipmentDetails shipment = new ShipmentDetails();
        shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
        handlingUnitFinal.setShipment(shipment);

        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setOriginCountryCode("USA");
        shipmentDetails.setDestinationCountryCode("CAN");
        shipmentDetails.setLegacyServiceCode("legacyservicecode");
        shipmentDetails.setZipCode("zipcode");

        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.DESTINATION);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
        Mockito.when(streamingServiceProperties.getPhuwvSourceType()).thenReturn("Testv2");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("Testv3");

        Mockito.doNothing().when(domainEventPublisher).publish(Mockito.anyString(),
                Mockito.any(MessagePostProcessor.class));

        streamingPublishService.mergeToPublish(handlingUnitFinal, shipmentDetails, Arrays.asList(waypoint), Arrays.asList(new Task()), "SEFS");
    }

    @Test
    public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_DESTINATION_FACILITY() {
        HandlingUnit handlingUnitFinal = new HandlingUnit();
        handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");
        handlingUnitFinal.setOwningOpCo("owningopco");

        ShipmentDetails shipment = new ShipmentDetails();
        shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
        handlingUnitFinal.setShipment(shipment);

        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setOriginCountryCode("USA");
        shipmentDetails.setDestinationCountryCode("CAN");
        shipmentDetails.setLegacyServiceCode("legacyservicecode");
        shipmentDetails.setZipCode("zipcode");

        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.DESTINATION_FACILITY);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
        Mockito.when(streamingServiceProperties.getPhuwvSourceType()).thenReturn("Testv2");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("Testv3");

        Mockito.doNothing().when(domainEventPublisher).publish(Mockito.anyString(),
                Mockito.any(MessagePostProcessor.class));

        streamingPublishService.mergeToPublish(handlingUnitFinal, shipmentDetails, Arrays.asList(waypoint), Arrays.asList(new Task()), "SEFS");
    }

    @Test
    public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_ORIGIN() {
        HandlingUnit handlingUnitFinal = new HandlingUnit();
        handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");
        handlingUnitFinal.setOwningOpCo("owningopco");

        ShipmentDetails shipment = new ShipmentDetails();
        shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
        handlingUnitFinal.setShipment(shipment);

        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setOriginCountryCode("USA");
        shipmentDetails.setDestinationCountryCode("CAN");
        shipmentDetails.setLegacyServiceCode("legacyservicecode");
        shipmentDetails.setZipCode("zipcode");

        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.ORIGIN);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
        Mockito.when(streamingServiceProperties.getPhuwvSourceType()).thenReturn("Testv2");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("Testv3");

        Mockito.doNothing().when(domainEventPublisher).publish(Mockito.anyString(),
                Mockito.any(MessagePostProcessor.class));

        streamingPublishService.mergeToPublish(handlingUnitFinal, shipmentDetails, Arrays.asList(waypoint), Arrays.asList(new Task()), "SEFS");
    }

    @Test
    public void test_getLatestHandlingUnitBy_HandlingUnitTrackingNumber_LoopThroughWaypoints_OTHER() {
        HandlingUnit handlingUnitFinal = new HandlingUnit();
        handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");
        handlingUnitFinal.setOwningOpCo("owningopco");

        ShipmentDetails shipment = new ShipmentDetails();
        shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
        handlingUnitFinal.setShipment(shipment);

        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setOriginCountryCode("USA");
        shipmentDetails.setDestinationCountryCode("CAN");
        shipmentDetails.setLegacyServiceCode("legacyservicecode");
        shipmentDetails.setZipCode("zipcode");

        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
        Mockito.when(streamingServiceProperties.getPhuwvSourceType()).thenReturn("Testv2");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("Testv3");

        Mockito.doNothing().when(domainEventPublisher).publish(Mockito.anyString(),
                Mockito.any(MessagePostProcessor.class));

        streamingPublishService.mergeToPublish(handlingUnitFinal, shipmentDetails, Arrays.asList(waypoint), Arrays.asList(new Task()), "SEFS");
    }

    @Test
    public void test_mergeToPublish_Exception() {
        HandlingUnit handlingUnitFinal = new HandlingUnit();
        handlingUnitFinal.setHuUUID("85044265-995a-3712-9604-5faa32769651");
        handlingUnitFinal.setOwningOpCo("owningopco");

        ShipmentDetails shipment = new ShipmentDetails();
        shipment.setShipmentUUID("85044265-995a-3712-9604-5faa3276965125");
        handlingUnitFinal.setShipment(shipment);

        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setOriginCountryCode("USA");
        shipmentDetails.setDestinationCountryCode("CAN");
        shipmentDetails.setLegacyServiceCode("legacyservicecode");
        shipmentDetails.setZipCode("zipcode");

        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.DESTINATION);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenThrow(NullPointerException.class);

        streamingPublishService.mergeToPublish(handlingUnitFinal, shipmentDetails, Arrays.asList(waypoint), Arrays.asList(new Task()), "SEFS");
    }

    @Test
    public void test_postMessageProcess() throws JMSException {
        com.fedex.phuwv.common.api.models.HandlingUnit handlingUnit = new com.fedex.phuwv.common.api.models.HandlingUnit();

        List<com.fedex.phuwv.common.api.models.Waypoint> waypoints = new ArrayList<>();
        com.fedex.phuwv.common.api.models.Waypoint waypoint = new com.fedex.phuwv.common.api.models.Waypoint();
        waypoint.setSequenceNumber(new BigInteger(String.valueOf(1L)));
        waypoint.setFacilityCode("facilitycode");
        waypoint.setFacilityCountryCode("facilitycountrycode");
        waypoints.add(waypoint);
        handlingUnit.setWaypoints(waypoints);

        com.fedex.phuwv.common.api.models.ShipmentDetails shipment = new com.fedex.phuwv.common.api.models.ShipmentDetails();
        shipment.setShipmentUUID("shipmentuuid");
        handlingUnit.setShipment(shipment);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("Testv1");
        Mockito.when(streamingServiceProperties.getPhuwvSourceType()).thenReturn("Testv2");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("Testv3");

        streamingPublishService.postMessageProcess(handlingUnit, createMessageInstance(), "SEFS");
    }

    @Test
    public void test_setRetryContext() {
        try{
            streamingPublishService.setRetryContext(createRetryContextInstance());
        }catch (Exception e){
        }
    }

    @Test
    public void test_setRetryContext_exception() {
        try{
            streamingPublishService.setRetryContext(null);
        }catch (Exception e){
        }
    }

    @Test
    public void test_invokeGetFutureTripByHandlingUnitUUID_Success() {
        Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(new TripDTO()));
        streamingPublishService.invokeGetFutureTripByHandlingUnitUUID("huuuid123");
    }

    @Test
    public void test_invokeGetFutureTripByHandlingUnitUUID_Exception() {
        CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
        tripDTOCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TRIP_DETAILS_FOUND_BY_HUUUID"));
        Mockito.when(phuwvServiceInvoker.getFutureTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(tripDTOCompletableFuture);
        streamingPublishService.invokeGetFutureTripByHandlingUnitUUID("huuuid123");
    }

    @Test
    public void test_invokeGetFutureTaskByHandlingUnitUUID_Success() {
        Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(Arrays.asList(new Task())));
        streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID("huuuid123");
    }

    @Test
    public void test_invokeGetFutureTaskByHandlingUnitUUID_Exception() {
        CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
        taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
        Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString())).thenReturn(taskCompletableFuture);
        streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID("huuuid123");
    }

    @Test
    public void test_invokeGetFutureLatestShipmentByUUID_Success() {
        Mockito.when(phuwvServiceInvoker.getFutureLatestShipmentByUUID(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));
        streamingPublishService.invokeGetFutureLatestShipmentByUUID("shipmentuuid123");
    }

    @Test
    public void test_invokeGetFutureLatestShipmentByUUID_Exception() {
        CompletableFuture<ShipmentDetails> shipmentDetailsCompletableFuture = new CompletableFuture<>();
        shipmentDetailsCompletableFuture.completeExceptionally(new NoDataFoundException("NO_SHIPMENT_DETAILS_FOUND_BY_SHIPMENTUUID"));
        Mockito.when(phuwvServiceInvoker.getFutureLatestShipmentByUUID(Mockito.anyString())).thenReturn(shipmentDetailsCompletableFuture);
        streamingPublishService.invokeGetFutureLatestShipmentByUUID("shipmentuuid123");
    }

    @Test
    public void test_invokeGetFutureLatestTripByHandlingUnitUUID_Success() {
        Mockito.when(phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(new TripDTO()));
        streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID("huuuid123");
    }

    @Test
    public void test_invokeGetFutureLatestTripByHandlingUnitUUID_Exception() {
        CompletableFuture<TripDTO> tripDTOCompletableFuture = new CompletableFuture<>();
        tripDTOCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TRIP_DETAILS_FOUND_BY_HUUUID"));
        Mockito.when(phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(tripDTOCompletableFuture);
        streamingPublishService.invokeGetFutureLatestTripByHandlingUnitUUID("huuuid123");
    }

    @Test
    public void test_fetchAndSortWaypoints() {
        ItineraryDTO itineraryDTO = new ItineraryDTO();
        List<ItineraryItems> items = new ArrayList<>();
        ItineraryItems itineraryItems = new ItineraryItems();
        itineraryItems.setWaypointUUID("waypointuuid123");
        items.add(itineraryItems);
        itineraryDTO.setItems(items);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setSequenceNumber(new BigInteger("1"));
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypoint.setFacilityOpCo("opco");
        waypoint.setFacilityCode("facilitycode");
        waypoint.setDestinationCountryCode("destination");
        waypoint.setFacilityCountryCode("countrycode");
        waypoint.setOriginCountryCode("origin");
        waypoint.setUrsaSuffix("ursa");
        waypoint.setZipcode("zip");
        waypoints.add(waypoint);
        Mockito.when(phuwvServiceInvoker.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(waypoints);
        streamingPublishService.fetchAndSortWaypoints(itineraryDTO);
    }

    @Test
    public void test_fetchItineraryAndGetWaypoints() {
        ItineraryDTO itineraryDTO = new ItineraryDTO();
        List<ItineraryItems> items = new ArrayList<>();
        ItineraryItems itineraryItems = new ItineraryItems();
        itineraryItems.setWaypointUUID("waypointuuid123");
        items.add(itineraryItems);
        itineraryDTO.setItems(items);
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setSequenceNumber(new BigInteger("1"));
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypoint.setFacilityOpCo("opco");
        waypoint.setFacilityCode("facilitycode");
        waypoint.setDestinationCountryCode("destination");
        waypoint.setFacilityCountryCode("countrycode");
        waypoint.setOriginCountryCode("origin");
        waypoint.setUrsaSuffix("ursa");
        waypoint.setZipcode("zip");
        waypoints.add(waypoint);
        Mockito.when(phuwvServiceInvoker.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(waypoints);

        streamingPublishService.fetchItineraryAndGetWaypoints("tripuuid123");
    }

    @Test
    public void test_fetchLSSIWaypoints() {
        ItineraryDTO itineraryDTO = new ItineraryDTO();
        List<ItineraryItems> items = new ArrayList<>();
        ItineraryItems itineraryItems = new ItineraryItems();
        itineraryItems.setWaypointUUID("waypointuuid123");
        items.add(itineraryItems);
        itineraryDTO.setItems(items);

        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setSequenceNumber(new BigInteger("1"));
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypoint.setFacilityOpCo("opco");
        waypoint.setFacilityCode("facilitycode");
        waypoint.setDestinationCountryCode("destination");
        waypoint.setFacilityCountryCode("countrycode");
        waypoint.setOriginCountryCode("origin");
        waypoint.setUrsaSuffix("ursa");
        waypoint.setZipcode("zip");
        waypoints.add(waypoint);
        Mockito.when(phuwvServiceInvoker.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(waypoints);

        Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(waypoints);

        streamingPublishService.fetchLSSIWaypoints(itineraryDTO);
    }

    @Test
    public void test_settingTaskDetails_Success() {
        streamingPublishService.settingTaskDetails(CompletableFuture.completedFuture(Arrays.asList(new Task())), "huuuid123");
    }

    @Test
    public void test_settingTaskDetails_Exception() {
        streamingPublishService.settingTaskDetails(null, "huuuid123");
    }

    @Test
    public void test_settingHandlingUnit_Success() {
        streamingPublishService.settingHandlingUnit(CompletableFuture.completedFuture(new HandlingUnit()));
    }

    @Test
    public void test_settingHandlingUnit_Exception() {
        streamingPublishService.settingHandlingUnit(null);
    }

    @Test
    public void test_settingShipmentDetailsForHandler_Success() {
        streamingPublishService.settingShipmentDetailsForHandler(CompletableFuture.completedFuture(new ShipmentDetails()));
    }

    @Test
    public void test_settingShipmentDetailsForHandler_Failure() {
        streamingPublishService.settingShipmentDetailsForHandler(null);
    }

    @Test
    public void test_settingTripItineraryWaypointInfo_NULL_Trip() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        streamingPublishService.settingTripItineraryWaypointInfo(CompletableFuture.completedFuture(null), "huuuid123");
    }

    @Test
    public void test_settingTripItineraryWaypointInfo_NULL_Itinerary() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(null);

        streamingPublishService.settingTripItineraryWaypointInfo(CompletableFuture.completedFuture(tripDTO), "huuuid123");
    }

    @Test
    public void test_settingTripItineraryWaypointInfo_Success() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ItineraryDTO());

        streamingPublishService.settingTripItineraryWaypointInfo(CompletableFuture.completedFuture(tripDTO), "huuuid123");
    }

    @Test
    public void test_settingItineraryForSepTrigger_NULL_Trip() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        streamingPublishService.settingItineraryForSepTrigger(CompletableFuture.completedFuture(null), "huuuid123");
    }

    @Test
    public void test_settingItineraryForSepTrigger_Success() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ItineraryDTO());

        streamingPublishService.settingItineraryForSepTrigger(CompletableFuture.completedFuture(tripDTO), "huuuid123");
    }

    @Test
    public void test_validateObjectForClosure_NULL() {
        streamingPublishService.validateObjectForClosure(null, "uuid123");
    }

    @Test
    public void test_validateObjectForClosure_Success() {
        streamingPublishService.validateObjectForClosure("", "uuid123");
    }

    @Test
    public void test_validateHandlingUnitForClosure_NULL() {
        streamingPublishService.validateHandlingUnitForClosure(null, "huuuid123");
    }

    @Test
    public void test_validateHandlingUnitForClosure_Success() {
        streamingPublishService.validateHandlingUnitForClosure(new HandlingUnit(), "huuuid123");
    }

    @Test
    public void test_validateTaskDetailsForClosure_NULL() {
        streamingPublishService.validateTaskDetailsForClosure(null, "huuuid123");
    }

    @Test
    public void test_validateTaskDetailsForClosure_Success() {
        streamingPublishService.validateTaskDetailsForClosure(new ArrayList<>(), "huuuid123");
    }

    @Test
    public void test_validatePublishingPhuwvJms_TRUE() {
        Mockito.when(streamingServiceProperties.isPhuwvPublishJms()).thenReturn(Boolean.TRUE);
        streamingPublishService.validatePublishingPhuwvJms("uuid123");
    }

    @Test
    public void test_validatePublishingPhuwvJms_FALSE() {
        Mockito.when(streamingServiceProperties.isPhuwvPublishJms()).thenReturn(Boolean.FALSE);
        streamingPublishService.validatePublishingPhuwvJms("uuid123");
    }

    @Test
    public void test_invokeGetTripByHandlingUnitUUID() {
        Mockito.when(phuwvServiceInvoker.getTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new TripDTO());
        streamingPublishService.invokeGetTripByHandlingUnitUUID("uuid123");
    }

    @Test
    public void test_settingTripItineraryWaypointInfoSync_NULL_Trip() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        streamingPublishService.settingTripItineraryWaypointInfoSync(null, "huuuid123");
    }

    @Test
    public void test_settingTripItineraryWaypointInfoSync_NULL_Itinerary() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(null);

        streamingPublishService.settingTripItineraryWaypointInfoSync(tripDTO, "huuuid123");
    }

    @Test
    public void test_settingTripItineraryWaypointInfoSync_Success() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ItineraryDTO());

        streamingPublishService.settingTripItineraryWaypointInfoSync(tripDTO, "huuuid123");
    }

    @Test
    public void test_invokeGetTaskByHandlingUnitUUID() {
        Mockito.when(phuwvServiceInvoker.getTaskByHandlingUnitUUID(Mockito.anyString())).thenReturn(Arrays.asList(new Task()));
        streamingPublishService.invokeGetTaskByHandlingUnitUUID("uuid123");
    }

    @Test
    public void test_invokeGetLatestShipmentByUUID() {
        Mockito.when(phuwvServiceInvoker.getShipmentByUUID(Mockito.anyString())).thenReturn(new ShipmentDetails());
        streamingPublishService.invokeGetLatestShipmentByUUID("uuid123");
    }

    @Test
    public void test_settingItineraryForSepTriggerSync_NULL_Trip() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        streamingPublishService.settingItineraryForSepTriggerSync(null, "huuuid123");
    }

    @Test
    public void test_settingItineraryForSepTriggerSync_Success() throws ExecutionException, InterruptedException {

        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ItineraryDTO());

        streamingPublishService.settingItineraryForSepTriggerSync(tripDTO, "huuuid123");
    }

    private Message createMessageInstance() {
        return new Message() {
            @Override
            public String getJMSMessageID() {
                return null;
            }

            @Override
            public void setJMSMessageID(String s) {

            }

            @Override
            public long getJMSTimestamp() {
                return 0;
            }

            @Override
            public void setJMSTimestamp(long l) {

            }

            @Override
            public byte[] getJMSCorrelationIDAsBytes() {
                return new byte[0];
            }

            @Override
            public void setJMSCorrelationIDAsBytes(byte[] bytes) {

            }

            @Override
            public void setJMSCorrelationID(String s) {

            }

            @Override
            public String getJMSCorrelationID() {
                return null;
            }

            @Override
            public Destination getJMSReplyTo() {
                return null;
            }

            @Override
            public void setJMSReplyTo(Destination destination) {

            }

            @Override
            public Destination getJMSDestination() {
                return null;
            }

            @Override
            public void setJMSDestination(Destination destination) {

            }

            @Override
            public int getJMSDeliveryMode() {
                return 0;
            }

            @Override
            public void setJMSDeliveryMode(int i) {

            }

            @Override
            public boolean getJMSRedelivered() {
                return false;
            }

            @Override
            public void setJMSRedelivered(boolean b) {

            }

            @Override
            public String getJMSType() {
                return null;
            }

            @Override
            public void setJMSType(String s) {

            }

            @Override
            public long getJMSExpiration() {
                return 0;
            }

            @Override
            public void setJMSExpiration(long l) {

            }

            @Override
            public long getJMSDeliveryTime() {
                return 0;
            }

            @Override
            public void setJMSDeliveryTime(long l) {

            }

            @Override
            public int getJMSPriority() {
                return 0;
            }

            @Override
            public void setJMSPriority(int i) {

            }

            @Override
            public void clearProperties() {

            }

            @Override
            public boolean propertyExists(String s) {
                return false;
            }

            @Override
            public boolean getBooleanProperty(String s) {
                return false;
            }

            @Override
            public byte getByteProperty(String s) {
                return 0;
            }

            @Override
            public short getShortProperty(String s) {
                return 0;
            }

            @Override
            public int getIntProperty(String s) {
                return 0;
            }

            @Override
            public long getLongProperty(String s) {
                return 0;
            }

            @Override
            public float getFloatProperty(String s) {
                return 0;
            }

            @Override
            public double getDoubleProperty(String s) {
                return 0;
            }

            @Override
            public String getStringProperty(String s) {
                return null;
            }

            @Override
            public Object getObjectProperty(String s) {
                return null;
            }

            @Override
            public Enumeration getPropertyNames() {
                return null;
            }

            @Override
            public void setBooleanProperty(String s, boolean b) {

            }

            @Override
            public void setByteProperty(String s, byte b) {

            }

            @Override
            public void setShortProperty(String s, short i) {

            }

            @Override
            public void setIntProperty(String s, int i) {

            }

            @Override
            public void setLongProperty(String s, long l) {

            }

            @Override
            public void setFloatProperty(String s, float v) {

            }

            @Override
            public void setDoubleProperty(String s, double v) {

            }

            @Override
            public void setStringProperty(String s, String s1) {

            }

            @Override
            public void setObjectProperty(String s, Object o) {

            }

            @Override
            public void acknowledge() {

            }

            @Override
            public void clearBody() {

            }

            @Override
            public <T> T getBody(Class<T> aClass) {
                return null;
            }

            @Override
            public boolean isBodyAssignableTo(Class aClass) {
                return false;
            }
        };
    }

    private RetryContext createRetryContextInstance() {
        return new RetryContext() {
            @Override
            public void setExhaustedOnly() {

            }

            @Override
            public boolean isExhaustedOnly() {
                return false;
            }

            @Override
            public RetryContext getParent() {
                return null;
            }

            @Override
            public int getRetryCount() {
                return 0;
            }

            @Override
            public Throwable getLastThrowable() {
                return new RetryableException("exception");
            }

            @Override
            public void setAttribute(String name, Object value) {

            }

            @Override
            public Object getAttribute(String name) {
                return null;
            }

            @Override
            public Object removeAttribute(String name) {
                return null;
            }

            @Override
            public boolean hasAttribute(String name) {
                return false;
            }

            @Override
            public String[] attributeNames() {
                return new String[0];
            }
        };
    }
}
