package com.fedex.phuwv.streaming.service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.jms.JMSPublisher;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.phuwv.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import java.io.IOException;
import java.util.Enumeration;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
@Slf4j
public class PublishJMSTest {

    @InjectMocks
    private StreamingPublishService streamingPublishService;

    @Autowired
    private JMSPublisher domainEventPublisher;

    @Autowired
    @Qualifier("defaultObjectMapper")
    private ObjectMapper objectMapper;

    @Mock
    private StreamingServiceProperties streamingServiceProperties;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        domainEventPublisher = Mockito.mock(JMSPublisher.class);
        Mockito.spy(domainEventPublisher);
    }

    @Test
    public void test_publishPhuwvEvent() throws IOException, JMSException {
        com.fedex.phuwv.common.api.models.HandlingUnit handlingUnitApiModel = JsonUtil.json2Obj(objectMapper, "data",
                "PhuwvAPI.json", com.fedex.phuwv.common.api.models.HandlingUnit.class);

        Mockito.when(streamingServiceProperties.getPhuwvReleaseVersion()).thenReturn("2.1.2");
        Mockito.when(streamingServiceProperties.getPhuwvSourceType()).thenReturn("PHUWV");
        Mockito.when(streamingServiceProperties.getPhuwvDomainEventType()).thenReturn("HANDLINGUNIT");

        String pubDomEvent = JsonUtil.asCompactJson(objectMapper, handlingUnitApiModel);
        Message processedMessage = streamingPublishService.postMessageProcess(handlingUnitApiModel, createMessageInstance(), "SEFS");
        domainEventPublisher.publish(pubDomEvent, message -> processedMessage);
    }

    private Message createMessageInstance() {
        return new Message() {
            @Override
            public String getJMSMessageID() {
                return null;
            }

            @Override
            public void setJMSMessageID(String s) {

            }

            @Override
            public long getJMSTimestamp() {
                return 0;
            }

            @Override
            public void setJMSTimestamp(long l) {

            }

            @Override
            public byte[] getJMSCorrelationIDAsBytes() {
                return new byte[0];
            }

            @Override
            public void setJMSCorrelationIDAsBytes(byte[] bytes) {

            }

            @Override
            public void setJMSCorrelationID(String s) {

            }

            @Override
            public String getJMSCorrelationID() {
                return null;
            }

            @Override
            public Destination getJMSReplyTo() {
                return null;
            }

            @Override
            public void setJMSReplyTo(Destination destination) {

            }

            @Override
            public Destination getJMSDestination() {
                return null;
            }

            @Override
            public void setJMSDestination(Destination destination) {

            }

            @Override
            public int getJMSDeliveryMode() {
                return 0;
            }

            @Override
            public void setJMSDeliveryMode(int i) {

            }

            @Override
            public boolean getJMSRedelivered() {
                return false;
            }

            @Override
            public void setJMSRedelivered(boolean b) {

            }

            @Override
            public String getJMSType() {
                return null;
            }

            @Override
            public void setJMSType(String s) {

            }

            @Override
            public long getJMSExpiration() {
                return 0;
            }

            @Override
            public void setJMSExpiration(long l) {

            }

            @Override
            public long getJMSDeliveryTime() {
                return 0;
            }

            @Override
            public void setJMSDeliveryTime(long l) {

            }

            @Override
            public int getJMSPriority() {
                return 0;
            }

            @Override
            public void setJMSPriority(int i) {

            }

            @Override
            public void clearProperties() {

            }

            @Override
            public boolean propertyExists(String s) {
                return false;
            }

            @Override
            public boolean getBooleanProperty(String s) {
                return false;
            }

            @Override
            public byte getByteProperty(String s) {
                return 0;
            }

            @Override
            public short getShortProperty(String s) {
                return 0;
            }

            @Override
            public int getIntProperty(String s) {
                return 0;
            }

            @Override
            public long getLongProperty(String s) {
                return 0;
            }

            @Override
            public float getFloatProperty(String s) {
                return 0;
            }

            @Override
            public double getDoubleProperty(String s) {
                return 0;
            }

            @Override
            public String getStringProperty(String s) {
                return null;
            }

            @Override
            public Object getObjectProperty(String s) {
                return null;
            }

            @Override
            public Enumeration getPropertyNames() {
                return null;
            }

            @Override
            public void setBooleanProperty(String s, boolean b) {

            }

            @Override
            public void setByteProperty(String s, byte b) {

            }

            @Override
            public void setShortProperty(String s, short i) {

            }

            @Override
            public void setIntProperty(String s, int i) {

            }

            @Override
            public void setLongProperty(String s, long l) {

            }

            @Override
            public void setFloatProperty(String s, float v) {

            }

            @Override
            public void setDoubleProperty(String s, double v) {

            }

            @Override
            public void setStringProperty(String s, String s1) {

            }

            @Override
            public void setObjectProperty(String s, Object o) {

            }

            @Override
            public void acknowledge() {

            }

            @Override
            public void clearBody() {

            }

            @Override
            public <T> T getBody(Class<T> aClass) {
                return null;
            }

            @Override
            public boolean isBodyAssignableTo(Class aClass) {
                return false;
            }
        };
    }
}
