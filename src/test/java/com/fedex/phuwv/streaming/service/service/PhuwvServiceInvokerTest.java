package com.fedex.phuwv.streaming.service.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateRequest;
import com.fedex.phuwv.common.dto.lssi.dto.BulkTranslateResponses;
import com.fedex.phuwv.common.exception.NoDataFoundException;
import com.fedex.phuwv.streaming.service.client.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
@Slf4j
public class PhuwvServiceInvokerTest {

    private MockMvc mockMvc;

    @Autowired
    @Qualifier("defaultObjectMapper")
    private ObjectMapper objectMapper;

    @InjectMocks
    private PhuwvServiceInvoker phuwvServiceInvoker;

    @Mock
    private HandlingUnitServiceClient handlingUnitServiceClient;

    @Mock
    private ShipmentServiceClient shipmentServiceClient;

    @Mock
    private WaypointServiceClient waypointServiceClient;

    @Mock
    private TripServiceClient tripServiceClient;

    @Mock
    private ItineraryServiceClient itineraryServiceClient;

    @Mock
    private SepServiceClient sepServiceClient;

    @Mock
    private LocationTranslationServiceClient locationTranslationServiceClient;

    @Mock
    private TaskServiceClient taskServiceClient;

    private static final String handlingUnitUUID = "12345";
    private static final String handlingUnitTrackNbr = "67890";
    private static final String shipmentUUID = "567567";
    private static final String waypointUUID = "696969";
    private static final String waypointTrkNbr = "171717";
    private static final String hUUID = "898989";
    private static final String tripUUID = "909090";
    private static final String sepTrackingNbr = "237237";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void when_call_handlingUnitUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureLatestHandlingUnitByUUID(handlingUnitUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_handlingUnitUUID_phuwvServiceInvokerFailure()
            throws Exception {

        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureLatestHandlingUnitByUUID(handlingUnitUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_handlingUnitTrackNbr_phuwvServiceInvokerSuccess()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureLatestHandlingUnitByTrackNbr(handlingUnitTrackNbr);
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_handlingUnitTrackNbr_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureLatestHandlingUnitByTrackNbr(handlingUnitTrackNbr);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_FutureShipmentUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        ShipmentDetails shipmentModel = new ShipmentDetails();
        shipmentModel.setShipmentUUID("shipment123");
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(shipmentModel, HttpStatus.OK));
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureLatestShipmentByUUID(shipmentUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_FutureShipmentUUID_phuwvServiceInvokerSuccess_EmptyBody()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureLatestShipmentByUUID(shipmentUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_FutureShipmentUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureLatestShipmentByUUID(shipmentUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_FutureShipmentUUID_phuwvServiceInvoker_Exception()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureLatestShipmentByUUID(shipmentUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<Waypoint> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByUUID(waypointUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_waypointUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        CompletableFuture<Waypoint> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByUUID(waypointUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointTrkNbr_phuwvServiceInvokerSuccess()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<Waypoint> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByTrackNbr(waypointTrkNbr);
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_waypointTrkNbr_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        CompletableFuture<Waypoint> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByTrackNbr(waypointTrkNbr);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointTrackNbr_phuwvServiceInvokerSuccess()
            throws Exception {
        Mockito.when(waypointServiceClient.getWaypointByEnterpriseTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<Waypoint> completableFuture = phuwvServiceInvoker.getFutureWaypointByEnterpriseTrackNbr("090909");
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_waypointTrackNbr_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(waypointServiceClient.getWaypointByEnterpriseTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        CompletableFuture<Waypoint> completableFuture = phuwvServiceInvoker.getFutureWaypointByEnterpriseTrackNbr("090909");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointHuUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        List<Waypoint> waypoints = new ArrayList<>();
        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        waypoints.add(waypoint);
        Mockito.when(waypointServiceClient.getLatestWaypointByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(waypoints, HttpStatus.OK));
        CompletableFuture<List<Waypoint>> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByHandlingUnitUUID(hUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointHuUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<List<Waypoint>> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByHandlingUnitUUID(hUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointHuUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<List<Waypoint>> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByHandlingUnitUUID(hUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_waypointHuUUID_phuwvServiceInvoker_Exception()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByHandlingUnitUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        CompletableFuture<List<Waypoint>> completableFuture = phuwvServiceInvoker.getFutureLatestWaypointByHandlingUnitUUID(hUUID);
        assertThat(completableFuture).isNotNull();
    }

    //
    @Test
    public void when_call_LattestShipmentUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        // TODO: ASSERT STATEMENT
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        //CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getLatestShipmentByUUID("535353");
        phuwvServiceInvoker.getLatestShipmentByUUID("535353");
        //ShipmentDetails completableFuture = phuwvServiceInvoker.getLatestShipmentByUUID("535353");
        //assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_LattestShipmentUUID_phuwvServiceInvokerFailure()
            throws Exception {

        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        ShipmentDetails completableFuture = phuwvServiceInvoker.getLatestShipmentByUUID("535353");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_huUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        HandlingUnit handlingUnit = new HandlingUnit();
        handlingUnit.setPhuwvReleaseVersion("2.1.1");
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(handlingUnit, HttpStatus.OK));
        HandlingUnit completableFuture = phuwvServiceInvoker.getLatestHandlingUnitByUUID("123123");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_huUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        HandlingUnit completableFuture = phuwvServiceInvoker.getLatestHandlingUnitByUUID("123123");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_huUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        phuwvServiceInvoker.getLatestHandlingUnitByUUID("123123");
    }

    @Test
    public void when_call_tripHuUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("trip123");
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(tripDTO, HttpStatus.OK));
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_tripHuUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_tripHuUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_tripHuUUID_phuwvServiceInvokerFailure_Exception()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_ItineraryBytripUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        ItineraryDTO itineraryDTO = new ItineraryDTO();
        itineraryDTO.setItems(new ArrayList<>());
        Mockito.when(itineraryServiceClient.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(itineraryDTO, HttpStatus.OK));
        ItineraryDTO completableFuture = phuwvServiceInvoker.getLatestItineraryByTripUUID(tripUUID);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_ItineraryBytripUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(itineraryServiceClient.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        ItineraryDTO completableFuture = phuwvServiceInvoker.getLatestItineraryByTripUUID(tripUUID);
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_ItineraryBytripUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(itineraryServiceClient.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
         phuwvServiceInvoker.getLatestItineraryByTripUUID("tripuuid123");

    }

    @Test
    public void when_call_ItineraryBytripUUID_phuwvServiceInvokerFailure_Feign_Exception()
            throws Exception {
        Mockito.when(itineraryServiceClient.getLatestItineraryByTripUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        phuwvServiceInvoker.getLatestItineraryByTripUUID("tripuuid123");

    }

    @Test
    public void when_call_LattestwaypointUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        Mockito.when(waypointServiceClient.getLatestWaypointByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(waypoint, HttpStatus.OK));
        Waypoint completableFuture = phuwvServiceInvoker.getLatestWaypointByUUID("356356");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_LattestwaypointUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        Waypoint completableFuture = phuwvServiceInvoker.getLatestWaypointByUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_LattestwaypointUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        Waypoint completableFuture = phuwvServiceInvoker.getLatestWaypointByUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_HandlingUnitByShipmentUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getListOfLatestHandlingUnitByShipmentUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<List<HandlingUnit>> completableFuture = phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID("987987");
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = Exception.class)
    public void when_call_HandlingUnitByShipmentUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getListOfLatestHandlingUnitByShipmentUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        CompletableFuture<List<HandlingUnit>> completableFuture = phuwvServiceInvoker.getFutureListHandlingUnitByShipmentUUID("987987");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_HandlingUnitBySepTrackingNbr_phuwvServiceInvokerSuccess()
            throws Exception {
        HandlingUnit handlingUnit = new HandlingUnit();
        handlingUnit.setPhuwvReleaseVersion("2.1.1");
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(handlingUnit, HttpStatus.OK));
        phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr("457457");
    }

    @Test
    public void when_call_HandlingUnitBySepTrackingNbr_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr("457457");
    }

    @Test(expected = Exception.class)
    public void when_call_HandlingUnitBySepTrackingNbr_phuwvServiceInvokerFailure()
            throws Exception {

        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        //CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr("457457");
        HandlingUnit completableFuture = phuwvServiceInvoker.getLatestHandlingUnitBySepTrackingNbr("457457");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_HandlingUnitWithSepData_phuwvServiceInvokerSuccess()
            throws Exception {

        Mockito.when(sepServiceClient.getLatestHUFromSEPByTrackingNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        //CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureHandlingUnitWithSepData(sepTrackingNbr);
        HandlingUnit handlingUnit = phuwvServiceInvoker.getFutureHandlingUnitWithSepData(sepTrackingNbr);
        //HandlingUnit completableFuture = phuwvServiceInvoker.getFutureHandlingUnitWithSepData(sepTrackingNbr);
        assertThat(handlingUnit).isNull();
    }

    @Test(expected = Exception.class)
    public void when_call_HandlingUnitWithSepData_phuwvServiceInvokerFailure()
            throws Exception {

        Mockito.when(sepServiceClient.getLatestHUFromSEPByTrackingNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        //CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureHandlingUnitWithSepData(sepTrackingNbr);
        HandlingUnit completableFuture = phuwvServiceInvoker.getFutureHandlingUnitWithSepData(sepTrackingNbr);
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_TranslatedLocationCode_phuwvServiceInvokerSuccess()
            throws Exception {

        Mockito.when(locationTranslationServiceClient.getLocationTranslation(Mockito.any())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        BulkTranslateResponses response=phuwvServiceInvoker.getFutureTranslatedLocationCode(new BulkTranslateRequest());
        assertThat(response).isNull();
    }

    @Test(expected = Exception.class)
    public void when_call_TranslatedLocationCode_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(locationTranslationServiceClient.getLocationTranslation(Mockito.any())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        BulkTranslateResponses response=phuwvServiceInvoker.getFutureTranslatedLocationCode(new BulkTranslateRequest());
        assertThat(response).isNotNull();
    }

    @Test
    public void getLatestHandlingUnitUUIDByTripUUIDSuccess()
            throws Exception {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("trip123");
        Mockito.when(tripServiceClient.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(tripDTO, HttpStatus.OK));
        TripDTO completableFuture = phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getLatestHandlingUnitUUIDByTripUUIDSuccess_Empty_Body()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        TripDTO completableFuture = phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID("454545");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void getLatestHandlingUnitUUIDByTripUUIDFailure()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        TripDTO completableFuture = phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID("454545");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void getLatestHandlingUnitByTrackNbrSuccess()
            throws Exception {
        HandlingUnit handlingUnit = new HandlingUnit();
        handlingUnit.setHuUUID("hu123");
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(handlingUnit, HttpStatus.OK));
        HandlingUnit completableFuture = phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getLatestHandlingUnitByTrackNbrSuccess_Empty_Body()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        HandlingUnit completableFuture = phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr("454545");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void getLatestHandlingUnitByTrackNbrFailure()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByTrackNbr(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        HandlingUnit completableFuture = phuwvServiceInvoker.getLatestHandlingUnitByTrackNbr("454545");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void getFutureHandlingUnitByUUIDSuccess()
            throws Exception {
        HandlingUnit handlingUnit = new HandlingUnit();
        handlingUnit.setPhuwvReleaseVersion("2.1.1");
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(handlingUnit, HttpStatus.OK));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureHandlingUnitByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureHandlingUnitByUUIDSuccess_Empty_Body()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureHandlingUnitByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureHandlingUnitByUUIDFailure()
            throws Exception {
        Mockito.when(handlingUnitServiceClient.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<HandlingUnit> completableFuture = phuwvServiceInvoker.getFutureHandlingUnitByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureShipmentByUUIDSuccess()
            throws Exception {
        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setShipmentUUID("shipment123");
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(shipmentDetails, HttpStatus.OK));
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureShipmentByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureShipmentByUUIDSuccess_Empty_Body()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureShipmentByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureShipmentByUUIDFailure()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureShipmentByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureShipmentByUUIDFailure_Exception()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        CompletableFuture<ShipmentDetails> completableFuture = phuwvServiceInvoker.getFutureShipmentByUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTripByHandlingUnitUUIDSuccess()
            throws Exception {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("trip123");
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(tripDTO, HttpStatus.OK));
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTripByHandlingUnitUUIDSuccess_Empty_Body()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTripByHandlingUnitUUIDFailure()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTripByHandlingUnitUUIDFailure_Exception()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        CompletableFuture<TripDTO> completableFuture = phuwvServiceInvoker.getFutureTripByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTaskByHandlingUnitUUIDSuccess()
            throws Exception {
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task());
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(tasks, HttpStatus.OK));
        CompletableFuture<List<Task>> completableFuture = phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTaskByHandlingUnitUUIDSuccess_Empty_Body()
            throws Exception {
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        CompletableFuture<List<Task>> completableFuture = phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTaskByHandlingUnitUUIDFailure()
            throws Exception {
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        CompletableFuture<List<Task>> completableFuture = phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void getFutureTaskByHandlingUnitUUIDFailure_Exception()
            throws Exception {
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        CompletableFuture<List<Task>> completableFuture = phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID("454545");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getLatestWaypointsByUUIDs_phuwvServiceInvokerSuccess()
            throws Exception {
        Waypoint waypoint = new Waypoint();
        waypoint.setType(Waypoint.TypeEnum.LINEHAUL);
        Mockito.when(waypointServiceClient.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(new ResponseEntity<>(Arrays.asList(waypoint), HttpStatus.OK));
        List<Waypoint> completableFuture = phuwvServiceInvoker.getLatestWaypointsByUUIDs(Arrays.asList("356356"));
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getLatestWaypointsByUUIDs_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        List<Waypoint> completableFuture = phuwvServiceInvoker.getLatestWaypointsByUUIDs(Arrays.asList("356356"));
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getLatestWaypointsByUUIDs_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        List<Waypoint> completableFuture = phuwvServiceInvoker.getLatestWaypointsByUUIDs(Arrays.asList("356356"));
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getLatestWaypointsByUUIDs_phuwvServiceInvokerFailure_Exception()
            throws Exception {
        Mockito.when(waypointServiceClient.getLatestWaypointsByUUIDs(Mockito.anyList())).thenThrow(NullPointerException.class);
        List<Waypoint> completableFuture = phuwvServiceInvoker.getLatestWaypointsByUUIDs(Arrays.asList("356356"));
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getShipmentByUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        ShipmentDetails shipmentDetails = new ShipmentDetails();
        shipmentDetails.setShipmentUUID("shipmentuuid123");
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(shipmentDetails, HttpStatus.OK));
        ShipmentDetails completableFuture = phuwvServiceInvoker.getShipmentByUUID("356356");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getShipmentByUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        ShipmentDetails completableFuture = phuwvServiceInvoker.getShipmentByUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getShipmentByUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        ShipmentDetails completableFuture = phuwvServiceInvoker.getShipmentByUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getShipmentByUUID_phuwvServiceInvokerFailure_Exception()
            throws Exception {
        Mockito.when(shipmentServiceClient.getLatestShipmentByUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        ShipmentDetails completableFuture = phuwvServiceInvoker.getShipmentByUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getTripByHandlingUnitUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripUUID("tripuuid123");
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(tripDTO, HttpStatus.OK));
        TripDTO completableFuture = phuwvServiceInvoker.getTripByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getTripByHandlingUnitUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        TripDTO completableFuture = phuwvServiceInvoker.getTripByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getTripByHandlingUnitUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        TripDTO completableFuture = phuwvServiceInvoker.getTripByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getTripByHandlingUnitUUID_phuwvServiceInvokerFailure_Exception()
            throws Exception {
        Mockito.when(tripServiceClient.getLatestTripByHandlingUnitUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        TripDTO completableFuture = phuwvServiceInvoker.getTripByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getTaskByHandlingUnitUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        Task task = new Task();
        WorkAttribute work = new WorkAttribute();
        work.setKey("key");
        task.setWork(work);
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(Arrays.asList(task), HttpStatus.OK));
        List<Task> completableFuture = phuwvServiceInvoker.getTaskByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNotNull();
    }

    @Test
    public void when_call_getTaskByHandlingUnitUUID_phuwvServiceInvokerSuccess_Empty_Body()
            throws Exception {
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.OK));
        List<Task> completableFuture = phuwvServiceInvoker.getTaskByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getTaskByHandlingUnitUUID_phuwvServiceInvokerFailure()
            throws Exception {
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
        List<Task> completableFuture = phuwvServiceInvoker.getTaskByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getTaskByHandlingUnitUUID_phuwvServiceInvokerFailure_Exception()
            throws Exception {
        Mockito.when(taskServiceClient.getTasksByHandlingUnitUUID(Mockito.anyString())).thenThrow(NullPointerException.class);
        List<Task> completableFuture = phuwvServiceInvoker.getTaskByHandlingUnitUUID("356356");
        assertThat(completableFuture).isNull();
    }

    @Test
    public void when_call_getListHandlingUnitByShipmentUUID_phuwvServiceInvokerSuccess()
            throws Exception {
        HandlingUnit handlingUnit = new HandlingUnit();
        handlingUnit.setHuUUID("huuuid123");
        Mockito.when(handlingUnitServiceClient.getListOfLatestHandlingUnitByShipmentUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(Arrays.asList(handlingUnit), HttpStatus.OK));
        List<HandlingUnit> completableFuture = phuwvServiceInvoker.getListHandlingUnitByShipmentUUID("356356");
        assertThat(completableFuture).isNotNull();
    }

    @Test(expected = NoDataFoundException.class)
    public void when_call_getListHandlingUnitByShipmentUUID_phuwvServiceInvokerFailure() {
        Mockito.when(handlingUnitServiceClient.getListOfLatestHandlingUnitByShipmentUUID(Mockito.anyString())).thenReturn(new ResponseEntity<>(HttpStatus.NO_CONTENT));
        phuwvServiceInvoker.getListHandlingUnitByShipmentUUID("356356");
    }
}
