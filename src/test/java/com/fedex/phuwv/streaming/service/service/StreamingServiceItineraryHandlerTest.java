/*
package com.fedex.phuwv.streaming.service.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import com.fedex.phuwv.common.dto.*;
import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.handlingunit.v1.api.HandlingUnitDomainEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.sefs.core.itinerary.v1.api.ItineraryDomainEvent;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
public class StreamingServiceItineraryHandlerTest {

	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@InjectMocks
	private StreamingServiceItineraryHandler streamingServiceItineraryHandler;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	@Mock
	private TripDTO tripDTO;
	@Mock
	private ShipmentDetails shipmentDetails;

	@Mock
	private StreamingPublishService streamingPublishService;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);

		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(new NeverRetryPolicy());
		ReflectionTestUtils.setField(streamingServiceItineraryHandler, "itineraryTriggerRetryTemplate", retryTemplate);

		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
	}

	@Test
	void ideal_test() throws ResourceException, Exception {
		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);
		tripDTO.setTripUUID("tripuuid");
		tripDTO.setHandlingUnitUUID("HandlingUnitUUID");
		shipmentDetails.setShipmentUUID("ShipmentUUID");

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(tripDTO);
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(tripDTO.getHandlingUnitUUID()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(new ShipmentDetails());
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));

	}

	@Test
	void futureLatestHandlingUnitByUUID_NULL_test() throws ResourceException, Exception {

		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(null);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	void futureLatestTripByHandlingUnitUUID_NULL_test() throws ResourceException, Exception {

		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		Mockito.when(phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	void futureLatestShipmentByUUID_NULL_test() throws ResourceException, Exception {

		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		Mockito.when(phuwvServiceInvoker.getFutureLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new HandlingUnit()));

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(phuwvServiceInvoker.getFutureLatestTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));
		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));
	}

	@Test
	void getLatestWaypointByUUID_Exception_getLatestHandlingUnitByUUID_NULL_test() throws ResourceException, Exception {
		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		tripDTO.setHandlingUnitUUID("HandlingUnitUUID");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(tripDTO);

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(null);

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));

	}

	@Test
	void getLatestWaypointByUUID_Exception_ShipmentDetails_NONNULL_test() throws ResourceException, Exception {
		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		tripDTO.setHandlingUnitUUID("HandlingUnitUUID");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(tripDTO);

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));

	}

	@Test
	void getLatestWaypointByUUID_Exception_ShipmentDetails_NULL_test() throws ResourceException, Exception {
		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		tripDTO.setHandlingUnitUUID("HandlingUnitUUID");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(tripDTO);

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
			.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("SHIPMENT");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));

	}

	*/
/*@Test
	void getFutureTaskByHandlingUnitUUID_Exception_test() throws ResourceException, Exception {
		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);
		tripDTO.setTripUUID("tripuuid");
		tripDTO.setHandlingUnitUUID("HandlingUnitUUID");
		shipmentDetails.setShipmentUUID("ShipmentUUID");

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(tripDTO);

		CompletableFuture<List<Task>> taskCompletableFuture = new CompletableFuture<>();
		taskCompletableFuture.completeExceptionally(new NoDataFoundException("NO_TASK_DETAILS_FOUND_BY_HUUUID"));
		Mockito.when(phuwvServiceInvoker.getFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(taskCompletableFuture);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(tripDTO.getHandlingUnitUUID()))
				.thenReturn(new HandlingUnit());

		Mockito.when(phuwvServiceInvoker.getFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);

		Mockito.when(phuwvServiceInvoker.getLatestWaypointsByUUIDs(Mockito.anyList())).thenReturn(new ArrayList<>());

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));

	}*//*


	@Test
	void CONS_type_test() throws ResourceException, Exception {
		ItineraryDomainEvent tripDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json",
				ItineraryDomainEvent.class);

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		tripDTO.setHandlingUnitUUID("HandlingUnitUUID");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitUUIDByTripUUID(Mockito.anyString())).thenReturn(tripDTO);

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		HandlingUnit handlingUnit = new HandlingUnit();
		handlingUnit.setHuUUID("huuuid123");
		handlingUnit.setType("CONSOLIDATION");

		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setShipmentUUID("shipmentuuid123");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Collections.singletonList(itineraryItems));
		Mockito.when(streamingPublishService.fetchItineraryAndGetWaypoints(Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString())).thenReturn(new ArrayList<>());

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceItineraryHandler.handle(new GenericMessage<>(tripDomainEvent));

	}

	@Test
	void validatePublishingPhuwvJms_test() throws Exception {
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
		streamingServiceItineraryHandler.handle(new GenericMessage<>(JsonUtil.json2Obj(objectMapper, "data", "ItineraryDomainEvent.json", ItineraryDomainEvent.class)));
	}
}
*/
