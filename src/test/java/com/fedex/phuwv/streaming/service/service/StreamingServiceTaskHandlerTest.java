/*
package com.fedex.phuwv.streaming.service.service;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import com.fedex.phuwv.common.dto.*;

import com.fedex.phuwv.streaming.service.configuration.StreamingServiceProperties;
import com.fedex.sefs.core.shipment.v1.api.ShipmentDomainEvent;
import org.apache.commons.collections4.map.HashedMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fedex.phuwv.common.exception.ResourceException;
import com.fedex.phuwv.util.JsonUtil;
import com.fedex.sefs.core.task.v1.api.TaskDomainEvent;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("L0")
@SpringBootTest
public class StreamingServiceTaskHandlerTest {

	@Autowired
	@Qualifier("defaultObjectMapper")
	private ObjectMapper objectMapper;

	@InjectMocks
	private StreamingServiceTaskHandler streamingServiceTaskhandler;

	@Mock
	private PhuwvServiceInvoker phuwvServiceInvoker;

	private static final String handlingUnitUUID = "36d83f00-76c8-38c4-a904-c9368f855d5a";

	@Mock
	private StreamingPublishService streamingPublishService;

	@Mock
	private LocationTranslationInvoker locationTranslationInvoker;

	@Mock
	private StreamingServiceProperties streamingServiceProperties;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		RetryTemplate retryTemplate = new RetryTemplate();
		retryTemplate.setRetryPolicy(new NeverRetryPolicy());
		ReflectionTestUtils.setField(streamingServiceTaskhandler, "taskTriggerRetryTemplate", retryTemplate);

		Mockito.when(streamingServiceProperties.getPhuwvCheckIfTripItineraryShipmentNull()).thenReturn("ISTRIPITINERARYSHIPMENTNULL");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeShipment()).thenReturn("SHIPMENT");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeWaypoint()).thenReturn("WAYPOINT");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTask()).thenReturn("TASK");
		Mockito.when(streamingServiceProperties.getPhuwvSefsCoreType()).thenReturn("SEFS");
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.FALSE);
	}

	@Test
	public void ideal_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);

		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);

		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString()))
				.thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.validateTaskDetailsForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void futureTripByHandlingUnitUUID_NULL_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);
		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);
		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(null));

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));

		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", null);
		tripAndItineraryMap.put("ITINERARY", new ItineraryDTO());
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void latestHandlingUnitByUUID_NULL_test() throws ResourceException, Exception {
		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);
		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);
		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(new TripDTO()));
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(null);
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ShipmentDetails()));
		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void futureLatestShipmentByUUID_NULL_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);
		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);
		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Mockito.when(phuwvServiceInvoker.getLatestItineraryByTripUUID(Mockito.anyString())).thenReturn(itineraryDTO);
		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void trip_feign_calls_NULL_results_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);

		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);

		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", null);
		tripAndItineraryMap.put("ITINERARY", new ItineraryDTO());
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void itinerary_feign_calls_NULL_results_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);

		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);

		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", null);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void shipment_feign_calls_NULL_results_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);

		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);

		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void task_feign_calls_NULL_results_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);

		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);

		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("SHIPMENT");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(null));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString()))
				.thenReturn(null);
		Mockito.when(streamingPublishService.validateTaskDetailsForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.TRUE);

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void getLatestHandlingUnitByUUID_NULL_test() throws ResourceException, Exception {
		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);
		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);
		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString())).thenReturn(null);
		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	public void type_CONS_test() throws ResourceException, Exception {

		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		MessageHeaders messageHeaders = new MessageHeaders(headers);

		TaskDomainEvent taskDomainEvent = JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json",
				TaskDomainEvent.class);

		GenericMessage<TaskDomainEvent> genericMessage = new GenericMessage<TaskDomainEvent>(taskDomainEvent,
				messageHeaders);

		HandlingUnit handlingUnit = new HandlingUnit();
		ShipmentDetails shipment = new ShipmentDetails();
		shipment.setShipmentUUID("shipmentuuid123");
		handlingUnit.setShipment(shipment);
		handlingUnit.setType("CONSOLIDATION");
		Mockito.when(phuwvServiceInvoker.getLatestHandlingUnitByUUID(Mockito.anyString()))
				.thenReturn(handlingUnit);

		ShipmentDetails shipmentDetails = new ShipmentDetails();
		shipmentDetails.setOriginCountryCode("USA");
		shipmentDetails.setDestinationCountryCode("CAN");
		Mockito.when(streamingPublishService.invokeGetFutureLatestShipmentByUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(shipmentDetails));

		TripDTO tripDTO = new TripDTO();
		tripDTO.setTripUUID("tripuuid");
		Mockito.when(streamingPublishService.invokeGetFutureTripByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(tripDTO));

		Mockito.when(streamingPublishService.invokeGetFutureTaskByHandlingUnitUUID(Mockito.anyString()))
				.thenReturn(CompletableFuture.completedFuture(new ArrayList<>()));

		ItineraryDTO itineraryDTO = new ItineraryDTO();
		ItineraryItems itineraryItems = new ItineraryItems();
		itineraryItems.setSequenceId("seqid");
		itineraryItems.setWaypointUUID("waypointuuid");
		itineraryDTO.setItems(Arrays.asList(itineraryItems));
		Map<String,Object> tripAndItineraryMap = new HashMap<>();
		tripAndItineraryMap.put("TRIP", tripDTO);
		tripAndItineraryMap.put("ITINERARY", itineraryDTO);
		Mockito.when(streamingPublishService.settingTripItineraryWaypointInfo(Mockito.any(), Mockito.anyString())).thenReturn(tripAndItineraryMap);
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeTrip()).thenReturn("TRIP");
		Mockito.when(streamingServiceProperties.getPhuwvDomainEventTypeItinerary()).thenReturn("ITINERARY");

		Mockito.when(streamingPublishService.fetchAndSortWaypoints(Mockito.any())).thenReturn(new ArrayList<>());

		Mockito.when(streamingPublishService.settingShipmentDetailsForHandler(Mockito.any()))
				.thenReturn(shipmentDetails);
		Mockito.when(streamingPublishService.validateObjectForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(streamingPublishService.settingTaskDetails(Mockito.any(), Mockito.anyString()))
				.thenReturn(new ArrayList<>());
		Mockito.when(streamingPublishService.validateTaskDetailsForClosure(Mockito.any(), Mockito.anyString()))
				.thenReturn(Boolean.FALSE);

		Mockito.when(locationTranslationInvoker.convertOSCWaypointsToLSSI(Mockito.anyList())).thenReturn(new ArrayList<>());

		Mockito.doNothing().when(streamingPublishService).mergeToPublish(Mockito.any(), Mockito.any(), Mockito.anyList(), Mockito.anyList(), Mockito.anyString());

		streamingServiceTaskhandler.handle(genericMessage);
	}

	@Test
	void validatePublishingPhuwvJms_test() throws Exception {
		Map<String, Object> headers = new HashedMap<String, Object>();
		headers.put("HandlingUnitUUID", handlingUnitUUID);
		Mockito.when(streamingPublishService.validatePublishingPhuwvJms(Mockito.anyString())).thenReturn(Boolean.TRUE);
		streamingServiceTaskhandler.handle(new GenericMessage<TaskDomainEvent>(JsonUtil.json2Obj(objectMapper, "data", "TaskDomainEvent.json", TaskDomainEvent.class), new MessageHeaders(headers)));
	}
}*/
